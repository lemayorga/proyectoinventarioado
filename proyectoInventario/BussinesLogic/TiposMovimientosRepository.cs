﻿using System.Data;
using DataAccess;
using Entity;
using BussinesLogic.Extensions;
using System.ComponentModel;
using System;
using System.Linq;
using System.Collections.Generic;

namespace BussinesLogic
{
    public class TiposMovimientosRepository : BaseRepository<TiposMovimientos>
    {
        #region Variables

        public enum DataTiposMov
        {
            [Description("")]
            todos = 0,
            [Description("COMPRAS")]
            compras = 1,
            [Description("VENTAS")]
            ventas = 2
        }

        #endregion

        protected sealed override string KeyName
        {
            get
            {
                return "tipoId";

            }
        }

        protected sealed override string TableName
        {
            get
            {
                return "dbo.TiposMovimientos";
            }
        }

        protected sealed override TiposMovimientos DataRowToModel(DataRow dr)
        {
            return dr == null ? null : dr.ToModel<TiposMovimientos>();
        }


        /// <summary>
        /// Insertar los datos si no existen
        /// </summary>
        public void InsertDataInit()
        {
           List<DataTiposMov> dataClasisf = Enum.GetValues(typeof(DataTiposMov)).Cast<DataTiposMov>().Select(x => x).ToList();
           List<string> dataClasisfDescript = dataClasisf.Select(x => x.DescriptionAttr()).ToList();


           List<TiposMovimientos> dataExistente = this.GetAll(where: $" codigo IN({string.Join(",", dataClasisf.Select(p => "\'" + p.DescriptionAttr() + "\'"))})").ToList();

            var dataNoExistente = dataClasisf.Where(x => !dataExistente.Select(y => y.codigo).ToList().Contains(x.DescriptionAttr()));

            var dataInsert = new List<TiposMovimientos>();
            foreach (var clasif in dataNoExistente)
                dataInsert.Add(new TiposMovimientos() { tipo = clasif.ToString(), codigo = clasif.DescriptionAttr() });

            this.Insert(dataInsert);
        }

        /// <summary>
        /// Obtener el id en base a un codigo de clasificacion
        /// </summary>
        /// <param name="dataClasif"></param>
        /// <returns></returns>
        public int GetIdDataEnum(DataTiposMov dataClasif)
        {
            var clasisf = this.FirsOrDefault(where: $"codigo = '{dataClasif.DescriptionAttr()}'");
            return clasisf == null ? 0 : clasisf.tipoId;
           
        }
    }
}
