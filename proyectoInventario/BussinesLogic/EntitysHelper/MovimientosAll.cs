﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;

namespace BussinesLogic
{
    /// <summary>
    /// Listar todos los movimientos(data maestro) con los datos principales
    /// </summary>
    public class MovimientosAll : Movimientos 
    {
        /// <summary>
        /// Nombre de la Persona 
        /// </summary>
        public string perNombre { get; set; }
        /// <summary>
        /// Apellido de la persoan
        /// </summary>
        public string perApellido { get; set; }
        /// <summary>
        /// Datos de la persona
        /// </summary>
        public string persona { get { return $"{perNombre} {perApellido}";  } }
        /// <summary>
        /// Tipo de movimiento
        /// </summary>
        public string tipoMov { get; set; }
    }
}
