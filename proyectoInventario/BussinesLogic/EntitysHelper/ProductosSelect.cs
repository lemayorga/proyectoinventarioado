﻿using BussinesLogic.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinesLogic
{
    /// <summary>
    /// Listar los Productos a ser seleccionados para un movimiento
    /// </summary>
    public class ProductosSelect : IProdSelect
    {
        /// <summary>
        /// Movimiento detalle Id
        /// </summary>
        public int movDetId { get; set; }
        /// <summary>
        /// Producto Id
        /// </summary>
        public int productoId { get; set; }
        /// <summary>
        /// Producto
        /// </summary>
        public string producto { get; set; }
        /// <summary>
        /// Marca
        /// </summary>
        public string marca { get; set; }
        /// <summary>
        /// Modelo
        /// </summary>
        public string modelo { get; set; }
        /// <summary>
        /// Costo
        /// </summary>
        public double costo { get; set; }
        /// <summary>
        /// Precio
        /// </summary>
        public double precio { get; set; }
        /// <summary>
        ///Existencias
        /// </summary>
        public int existencia { get; set; }
        /// <summary>
        /// Cantidad a ser ingresada desde la UI
        /// </summary>
        public int cantidad { get; set; }
    }
}
