﻿using BussinesLogic.Extensions;
using DataAccess;
using Entity.Security;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinesLogic.Security
{
    class RolesControls : BaseRepository<AppRolesControls>
    {
        protected sealed override string KeyName
        {
            get
            {
                return "appRolesControlId";

            }
        }

        protected sealed override string TableName
        {
            get
            {
                return "dbo.AppRolesControls";
            }
        }

        protected sealed override AppRolesControls DataRowToModel(DataRow dr)
        {
            return dr == null ? null : dr.ToModel<AppRolesControls>();
        }
    }
}
