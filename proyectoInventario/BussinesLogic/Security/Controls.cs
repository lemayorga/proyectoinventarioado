﻿using BussinesLogic.Extensions;
using DataAccess;
using Entity.Security;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinesLogic.Security
{
    public class Controls : BaseRepository<AppControls>
    {
        protected sealed override string KeyName
        {
            get
            {
                return "appControlId";

            }
        }

        protected sealed override string TableName
        {
            get
            {
                return "dbo.AppControls";
            }
        }

        protected sealed override AppControls DataRowToModel(DataRow dr)
        {
            return dr == null ? null : dr.ToModel<AppControls>();
        }

        /// <summary>
        /// Obtener los controles relacionados a un usuario por su rol  
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public List<FormContolsRol> GetControlesRolUsuario(AppRoles rol)
        {
            this.ClearParameters();
            this.AddParameter("@rolId", rol.appRolId);
            var datos = this.ExecProcedure("ListControlsUser");
            return datos == null ? null
                : datos.Tables.Count < 0 ? null
                : datos.Tables[0].ToList<FormContolsRol>();
       }

    }
}
