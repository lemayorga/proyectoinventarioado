﻿using BussinesLogic.Extensions;
using DataAccess;
using Entity.Security;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinesLogic.Security
{
    public class Roles : BaseRepository<AppRoles>
    {
        #region Variables

        /// <summary>
        /// Constante de rol sa
        /// </summary>
        public const string ROL_SA = "sadministrador";

        #endregion

        protected sealed override string KeyName
        {
            get
            {
                return "appRolId";

            }
        }

        protected sealed override string TableName
        {
            get
            {
                return "dbo.AppRoles";
            }
        }

        protected sealed override AppRoles DataRowToModel(DataRow dr)
        {
            return dr == null ? null : dr.ToModel<AppRoles>();
        }

        /// <summary>
        /// Insertar los datos si no existen
        /// </summary>
        public AppRoles InsertDataInit()
        {
            var rol = this.FirsOrDefault(where: $"appRolName in('{ROL_SA}')");
            if (rol == null)
            {
                rol = new AppRoles() { appRolName = ROL_SA, descripcion = "Rol Sa" };
                this.Insert(rol);
            }
            return rol;
        }


        /// <summary>
        /// Listar los datos en formato Datatable
        /// </summary>
        /// <returns></returns>
        public DataTable Get_DT(string where= "")
        {
            var appRolSA = $"appRolName <> '{ROL_SA}'";
            where = where + (string.IsNullOrEmpty(where) ? appRolSA : $"AND {appRolSA}");
            return this.Select(table: $"{TableName}", fieldsSelect: "*", where: where, innerJoin: "");

        }
        /// <summary>
        /// Obtener el primer registro del rol para un usuario 
        /// </summary>
        /// <param name="appUser"></param>
        /// <returns></returns>
        public AppRoles GetFirstRolUser(AppUsers appUser)
        {
            var joins = new Dictionary<string, string>();
            joins.Add("AppRolesUser ru  ", "r.appRolId = ru.appRolId");

            var data = this.Select(table: $"{TableName} r", fieldsSelect: " TOP 1  r.* ", innerJoin: joins, where: $"ru.appUserId = {appUser.appUserId.ToString()}");

            return data == null ? null : data.ToList<AppRoles>().FirstOrDefault();
        }
    }

}