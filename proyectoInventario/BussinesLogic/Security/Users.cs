﻿using DataAccess;
using Entity.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using BussinesLogic.Extensions;

namespace BussinesLogic.Security
{
    public class Users : BaseRepository<AppUsers>
    {
        #region Variables

        /// <summary>
        /// Constante de usuario sa
        /// </summary>
        public const string USER_SA = "sadministrador";
        /// <summary>
        /// Constante de contrase;a de usuario sa
        /// </summary>
        public const string USER_SA_PASS = "managua0102";
        /// <summary>
        /// Minimo de caracteres para la contrase;a
        /// </summary>
        public const int PASS_LENGTH_MIN = 6;
        /// <summary>
        /// Saber los caracteres invalidos
        /// </summary>
        public char[] caratersInvalidos = new char[] { '-', '*','/','\\' };
        /// <summary>
        /// Repositorio de roles usuarios
        /// </summary>
        private RolesUser rolesUser = new RolesUser();
        /// <summary>
        /// Repositorio de roles
        /// </summary>
        private Roles roles = new Roles();
        /// <summary>
        /// Mensajes de validacion de login
        /// </summary>
        public enum MgsLogin
        {
            usuarioNoExiste,
            usuarioBloqueado,
            exitoso,
        }
        #endregion

        protected sealed override string KeyName
        {
            get
            {
                return "appUserId";

            }
        }

        protected sealed override string TableName
        {
            get
            {
                return "dbo.AppUsers";
            }
        }

        protected sealed override AppUsers DataRowToModel(DataRow dr)
        {
            return dr == null ? null : dr.ToModel<AppUsers>();
        }


        /// <summary>
        /// Insertar los datos si no existen
        /// </summary>
        public AppUsers InsertDataInit()
        {
            var rol = new Roles().InsertDataInit();
            var user = this.FirsOrDefault(where: $"appUserName in('{USER_SA}')");
            if (rol == null)
            {
                user = new AppUsers() { appUserName = USER_SA, password = USER_SA_PASS, bloqueado = false, firstName = USER_SA, lastName = USER_SA };
                this.Insert(user);
            }
            return user;
        }

        /// <summary>
        /// Listar los datos en formato Datatable
        /// </summary>
        /// <returns></returns>ss
        public DataTable Get_DT(string where = "")
        {
            var appUserSA =  $"appUserName <> '{USER_SA}'";
            where = where  + (string.IsNullOrEmpty(where) ? appUserSA : $"AND {appUserSA}");
            return this.Select(table: $"{TableName}", fieldsSelect: "*",where: where, innerJoin: "");
        }

        /// <summary>
        ///Guardar registro de usuario con rol, se ajunta logica segun negocio
        /// </summary>
        /// <param name="user"></param>
        /// <param name="rolId"></param>
        public AppUsers SaveUser(AppUsers user, int rolId)
        {
            bool esActualizacion = user.appUserId > 0;
            if (!esActualizacion) this.Insert(user);
            else this.Update(user);

            var rol = roles.GetById(rolId);
            if (rol != null)
            {
                if (esActualizacion)
                {
                    var userInRoles = rolesUser.GetUserInRoles(user.appUserId);
                    if (userInRoles.Any())
                    {
                        userInRoles.ForEach(x => { rolesUser.Delete(x.appRolUserId); });
                    }
                }
                rolesUser.Insert(new AppRolesUser() { appUserId = user.appUserId, appRolId  = rol.appRolId });
            }
            return user;
        }

        /// <summary>
        /// Obtener el primero registro de rol 
        /// </summary>
        /// <param name="appUserId"></param>
        /// <returns></returns>
        public AppRolesUser GetFirtItemRol(int appUserId)
        {
            var rolesUser_ = rolesUser.FirsOrDefault($"appUserId IN({appUserId})");
            return rolesUser_;
        }


        /// <summary>
        /// Verificar su ya existe el nombre de usuario
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public bool ExisteUserName(string userName)
        {
            var usuarios = this.Get(where: $"appUserName = '{userName.Trim()}'");
            return usuarios.Any();
        }

        /// <summary>
        /// Validar el login de usuarios
        /// </summary>
        /// <param name="usuarioName"></param>
        /// <param name="contrasena"></param>
        /// <returns></returns>
        public  AppUsers IniciarSesion(string usuarioName, string contrasena, out MgsLogin resultado)
        {
            var usuario = this.FirsOrDefault(where: $"appUserName = '{RemoverCaracteres(usuarioName)}' and password = '{RemoverCaracteres(contrasena)}'");
            if (usuario == null)
                resultado = MgsLogin.usuarioNoExiste;
            else if (usuario.bloqueado)
                resultado =  MgsLogin.usuarioBloqueado;
            else
                resultado =  MgsLogin.exitoso;

            return usuario;
        }

        /// <summary>
        /// Remover caracteres especiales
        /// </summary>
        /// <param name="texto"></param>
        private string RemoverCaracteres(string texto)
        {
           
            caratersInvalidos.ToList().ForEach(x =>
            {
                if (texto.Contains(x))
                    texto.Replace(x.ToString(), "");
            });
            return texto.Trim();
        }
    }
}
