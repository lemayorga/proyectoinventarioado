﻿using BussinesLogic.Extensions;
using DataAccess;
using Entity.Security;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinesLogic.Security
{
    public class Forms : BaseRepository<AppForms>
    {
        protected sealed override string KeyName
        {
            get
            {
                return "appFormId";

            }
        }

        protected sealed override string TableName
        {
            get
            {
                return "dbo.AppForms";
            }
        }

        protected sealed override AppForms DataRowToModel(DataRow dr)
        {
            return dr == null ? null : dr.ToModel<AppForms>();
        }
    }
}
