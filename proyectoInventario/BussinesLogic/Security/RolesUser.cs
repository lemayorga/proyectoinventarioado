﻿using BussinesLogic.Extensions;
using DataAccess;
using Entity.Security;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinesLogic.Security
{
    class RolesUser : BaseRepository<AppRolesUser>
    {
        protected sealed override string KeyName
        {
            get
            {
                return "appRolUserId";

            }
        }

        protected sealed override string TableName
        {
            get
            {
                return "dbo.AppRolesUser";
            }
        }

        protected sealed override AppRolesUser DataRowToModel(DataRow dr)
        {
            return dr == null ? null : dr.ToModel<AppRolesUser>();
        }

        /// <summary>
        /// Obtener un Usuario en Roles
        /// </summary>
        /// <param name="appUserId"></param>
        /// <returns></returns>
        public List<AppRolesUser> GetUserInRoles(int appUserId)
        {
            return this.Get(where: $"appUserId = {appUserId}").ToList<AppRolesUser>();
        }
    }
}
