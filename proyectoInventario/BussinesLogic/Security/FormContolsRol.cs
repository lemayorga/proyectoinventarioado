﻿using Entity.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinesLogic.Security
{
    public class FormContolsRol 
    {
        public int appFormId { get; set; }
        public string formName { get; set; }
        public int? formFatherId { get; set; }
        public bool fVisible { get; set; }
        public int appControlId { get; set; }
        public string controlName { get; set; }
        public bool cVisible { get; set; }
    }
}
