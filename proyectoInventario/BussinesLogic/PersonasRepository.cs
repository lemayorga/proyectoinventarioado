﻿using System.Data;
using DataAccess;
using Entity;
using BussinesLogic.Extensions;
using static BussinesLogic.ClasficacionesRepository;
using System.Collections.Generic;

namespace BussinesLogic
{
    public class PersonasRepository : BaseRepository<Personas>
    {
        #region Variables

        /// <summary>
        /// Repositorio de la tabla puente Clasificaciones-Personas
        /// </summary>
        private ClasifPersonasRepository clasficacionesPerRepository = new ClasifPersonasRepository();
        /// <summary>
        /// Repositorio de tabla de Clasificaciones de personas
        /// </summary>
        private ClasficacionesRepository clasficacionesRepository = new ClasficacionesRepository();
        /// <summary>
        ///  Repositorio de tabla de Movimientos
        /// </summary>
        private MovimientosRepository movimientosRepository = new MovimientosRepository();

        #endregion

        protected sealed override string KeyName
        {
            get
            {
                return "personaId";

            }
        }

        protected sealed override string TableName
        {
            get
            {
                return "dbo.Personas";
            }
        }

        protected sealed override Personas DataRowToModel(DataRow dr)
        {
            return dr == null ? null : dr.ToModel<Personas>();
        }

        /// <summary>
        /// Obtener data de personas segun un tipo de clasiificacion
        /// </summary>
        /// <param name="clasisficacion"></param>
        /// <returns></returns>
        public ICollection<Personas> GetAllClasificados(DataClasif clasisficacion)
        {
            var joins = new Dictionary<string, string>();
            joins.Add("ClasifPersonas cp", $"p.{KeyName} = cp.personaId");
            joins.Add("Clasificaciones c", $"c.clasificacionId = cp.clasificacionId");

            return this.Get(tableNameAlias: "p", innerJoin: joins,where: $"c.codigo = '{clasisficacion.DescriptionAttr()}'");
        }


        /// <summary>
        /// Obtener data de personas segun un tipo de clasiificacion
        /// </summary>
        /// <param name="clasisficacion"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        public DataTable GetAllClasificados_DT(DataClasif clasisficacion)
        {
            var joins = new Dictionary<string, string>();
            joins.Add("ClasifPersonas cp", $"p.{KeyName} = cp.personaId");
            joins.Add("Clasificaciones c", $"c.clasificacionId = cp.clasificacionId");

            return this.Select(table: $"{TableName} p",fieldsSelect: "p.* ", innerJoin: joins, where: $"c.codigo = '{clasisficacion.DescriptionAttr()}'");
        }

        /// <summary>
        /// Insertar clasificacion de la persona
        /// </summary>
        /// <param name="persona"></param>
        /// <param name="clasisficacion"></param>
        /// <returns></returns>
        public ClasifPersonas AddClasificacion(Personas persona, DataClasif clasisficacion)
        {
            var idClasisf = clasficacionesRepository.GetIdDataEnum(clasisficacion);
            var clasisf = new ClasifPersonas() { clasificacionId = idClasisf, personaId = persona.personaId };
            clasficacionesPerRepository.Insert(clasisf);
            return clasisf;
        }    

        /// <summary>
        /// Eliminar persona con sus clasificacion de persona
        /// </summary>
        /// <param name="key"></param>
        /// <param name="clasisficacion"></param>
        /// <returns></returns>
        public bool Delete(int key, DataClasif clasisficacion)
        {
            var idClasisf = clasficacionesRepository.GetIdDataEnum(clasisficacion);
            var clasisf = clasficacionesPerRepository.FirsOrDefault($"personaId = {key} and clasificacionId = {idClasisf}");

            if (clasisf != null)
                clasficacionesPerRepository.Delete(clasisf.clasifPersId);
        
            return this.Delete(key);
        }

        /// <summary>
        /// Verificar si la persona tiene Movimmientos
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool TieneMovimientos(int key)
        {
            return movimientosRepository.ExisteRegMov(MovimientosRepository.EntidadMovs.master, this.KeyName, key);
        }
    }
}
