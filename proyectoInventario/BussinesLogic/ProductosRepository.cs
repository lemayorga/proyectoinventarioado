﻿using System.Data;
using DataAccess;
using Entity;
using BussinesLogic.Extensions;
using System.Collections.Generic;
using System.Linq;
using static BussinesLogic.TiposMovimientosRepository;

namespace BussinesLogic
{
    public class ProductosRepository : BaseRepository<Productos>
    {
        #region Variables

        /// <summary>
        ///  Repositorio de tabla de Movimientos
        /// </summary>
        private MovimientosRepository movimientosRepository = new MovimientosRepository();

        #endregion
        protected sealed override string KeyName
        {
            get
            {
                return "productoId";

            }
        }

        protected sealed override string TableName
        {
            get
            {
                return "dbo.Productos";
            }
        }

        protected sealed override Productos DataRowToModel(DataRow dr)
        {
            return dr == null ? null : dr.ToModel<Productos>();
        }

        /// <summary>
        /// Obtener data de personas segun un tipo de clasiificacion
        /// </summary>
        /// <param name="clasisficacion"></param>
        /// <returns></returns>
        public DataTable GetAll_DT()
        {
            return this.Select(table: $"vwProductos", fieldsSelect: "*",innerJoin: "");
        }

        /// <summary>
        /// Obtener los productos ordernados por un parametro
        /// </summary>
        /// <param name="order">Valor por default es por el nombre</param>
        /// <returns></returns>
        public List<Productos> GetOrderByName(string order = "producto")
        {
            return GetAll(orderBy: order).ToList();
        }

        /// <summary>
        /// Obtener los productos para su seleccion para los movimientos Detalle
        /// </summary>
        /// <param name="tipoMov"></param>
        /// <returns></returns>
        public DataTable GetProductosSelect_DT(DataTiposMov tipoMov)
        {
            this.ClearParameters();
            this.AddParameter("@tipoMov", tipoMov.GetHashCode());
            var data = this.ExecProcedure("ListProductosSelect");
            if (data.Tables.Count <= 0) return null;

            return data.Tables[0];
        }

        /// <summary>
        /// Verificar si la persona tiene Movimmientos
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool TieneMovimientos(int key)
        {
            return movimientosRepository.ExisteRegMov(MovimientosRepository.EntidadMovs.detalle, this.KeyName, key);
        }
    }
}
