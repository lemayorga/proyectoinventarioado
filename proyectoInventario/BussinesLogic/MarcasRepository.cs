﻿using System.Data;
using DataAccess;
using Entity;
using BussinesLogic.Extensions;

namespace BussinesLogic
{
    public class MarcasRepository : BaseRepository<Marcas>
    {
        protected sealed override string KeyName
        {
            get
            {
                return "marcaId";

            }
        }

        protected sealed override string TableName
        {
            get
            {
                return "dbo.Marcas";
            }
        }

        protected sealed override Marcas DataRowToModel(DataRow dr)
        {
            return dr == null ? null : dr.ToModel<Marcas>();
        }
    }
}
