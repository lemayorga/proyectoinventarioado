﻿using System.Data;
using DataAccess;
using Entity;
using BussinesLogic.Extensions;
using System.Collections.Generic;
using static BussinesLogic.TiposMovimientosRepository;
using System.Linq;
using static BussinesLogic.EstadosRepository;
using System;

namespace BussinesLogic
{
    public class MovimientosRepository : BaseRepository<Movimientos>
    {
        #region Variables
        /// <summary>
        /// Repositorio de Movimientos Detalle
        /// </summary>
        private MovimientoDetRepository movDetRepo = new MovimientoDetRepository();
        /// <summary>
        /// Reepositorio de tipos de movimientos
        /// </summary>
        private TiposMovimientosRepository tipoMovRepo = new TiposMovimientosRepository();
        /// <summary>
        /// Reepositorio de estados
        /// </summary>
        private EstadosRepository estadoRepo = new EstadosRepository();
        /// <summary>
        /// Reepositorio de estados mov
        /// </summary>
        private MovEstadosRepository estadoMovRepo = new MovEstadosRepository();
        /// <summary>
        /// Clasificacion de las entidades de los movimientos
        /// </summary>
        public enum EntidadMovs
        {
            master,
            detalle
        }

        #endregion

        protected sealed override string KeyName
        {
            get
            {
                return "movimientoId";

            }
        }

        protected sealed override string TableName
        {
            get
            {
                return "dbo.Movimientos";
            }
        }

        protected sealed override Movimientos DataRowToModel(DataRow dr)
        {
            return dr == null ? null : dr.ToModel<Movimientos>();
        }

        /// <summary>
        /// Obtener el detalle del movimiento
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ICollection<MovimientoDet> GetDetalle(int id)
        {
            return movDetRepo.GetAll(where: $"movimientoId = {id}");
        }

        /// <summary>
        /// Obtener el detalle del movimiento
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<ProductosSelect> GetDetalleProductosSelect(int id)
        {
            this.ClearParameters();
            this.AddParameter("@idMov", id);
            var dt = this.ExecProcedure("ListProductosSelectMovDet");

            return dt == null ? null
                 : dt.Tables[0].ToList<ProductosSelect>();
        }

        /// <summary>
        /// Agregar una lista del detalle
        /// </summary>
        /// <param name="detalle"></param>
        public void AddSaveDetalle(int movId, List<MovimientoDet> detalle)
        {
            detalle.ForEach(x =>{  x.movimientoId = movId; });
            movDetRepo.Insert(detalle);
        }

        /// <summary>
        /// Insertar un estado de movimientos
        /// </summary>
        /// <param name="movId"></param>
        /// <param name="estado"></param>
        /// <param name="usuario"></param>
        /// <param name="fecha"></param>
        public void  InsertEstadoMov(int movId, DataEstados estado, string usuario ,DateTime fecha)
        {
            var estadoId = estadoRepo.GetIdDataEnum(estado);
            estadoMovRepo.Insert(new MovEstados() {  movimientoId = movId, estadoId = estadoId, usuario = usuario, fecha = fecha });
        }

        /// <summary>
        /// Obtener los datos de un movimiento
        /// </summary>
        /// <param name="movId"></param>
        /// <returns></returns>
        public List<MovimientosAll> GetMovimientosAllData()
        {
           return  this.Select(table: "vwMovimientosAll", fieldsSelect: "*", where: null, innerJoin: "").ToList<MovimientosAll>();
        }

        /// <summary>
        /// Obtener los datos de un movimiento
        /// </summary>
        /// <returns></returns>
        public DataTable GetMovimientosAllData_DT(DataTiposMov tipoMov)
        {
            var tipoMovId = tipoMovRepo.GetIdDataEnum(tipoMov);
            return this.Select(table: "vwMovimientosAll", fieldsSelect: "*", where: $"tipoId={tipoMovId}", innerJoin: "");
        }

        /// <summary>
        /// Verificar la existencia de un registro de entidad en las entidades de movimientos
        /// </summary>
        /// <param name="entMovs"></param>
        /// <param name="foreignName"></param>
        /// <param name="foreignValue"></param>
        /// <returns></returns>
        public bool ExisteRegMov(EntidadMovs entMovs,string foreignName, int foreignValue)
        {
            var existe = 0;
            switch (entMovs)
            {
                case EntidadMovs.master:
                    existe =  this.Count(where: $"{foreignName} = {foreignValue}");
                    break;
                case EntidadMovs.detalle:
                    existe = movDetRepo.Count(where: $"{foreignName} = {foreignValue}");
                    break;
            }

            return existe > 0 ? true : false;
        }

        /// <summary>
        /// Anular movimiento
        /// </summary>
        /// <param name="movimientoId"></param>
        /// <param name="usuario"></param>
        /// <param name="fecha"></param>
        public bool AnularMovimiento(int movimientoId, string usuario, DateTime fecha)
        {
            var estadoId = estadoRepo.GetIdDataEnum(DataEstados.anulado);
            estadoMovRepo.Insert(new MovEstados() { movimientoId = movimientoId, estadoId = estadoId, usuario = usuario, fecha = fecha });
            return true;
        }

        /// <summary>
        /// Obtener el ultimo estado de un movimiento
        /// </summary>
        /// <param name="movId"></param>
        /// <returns></returns>
        public Estados GetEstado(int movId)
        {
            this.ClearParameters();
            this.AddParameter("@movId", movId);
            var dt = this.ExecProcedure("GetEstadoMov");

            var result = dt == null ? null : dt.Tables[0].ToList<Estados>();

            return result.FirstOrDefault();
        }
    }
}
