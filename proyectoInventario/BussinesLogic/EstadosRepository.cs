﻿using System.Data;
using DataAccess;
using Entity;
using BussinesLogic.Extensions;
using System.ComponentModel;
using System;
using System.Linq;
using System.Collections.Generic;

namespace BussinesLogic
{
    public class EstadosRepository : BaseRepository<Estados>
    {
        #region Variables

        public enum DataEstados
        {
            [Description("ACTIVO")]
            activo,
            [Description("ANULADO")]
            anulado
        }

        #endregion

        protected sealed override string KeyName
        {
            get
            {
                return "estadoId";

            }
        }

        protected sealed override string TableName
        {
            get
            {
                return "dbo.Estados";
            }
        }

        protected sealed override Estados DataRowToModel(DataRow dr)
        {
            return dr == null ? null : dr.ToModel<Estados>();
        }


        /// <summary>
        /// Insertar los datos si no existen
        /// </summary>
        public void InsertDataInit()
        {
           List<DataEstados> dataClasisf = Enum.GetValues(typeof(DataEstados)).Cast<DataEstados>().Select(x => x).ToList();
           List<string> dataClasisfDescript = dataClasisf.Select(x => x.DescriptionAttr()).ToList();


           List<Estados> dataExistente = this.GetAll(where: $" codigo IN({string.Join(",", dataClasisf.Select(p => "\'" + p.DescriptionAttr() + "\'"))})").ToList();

            var dataNoExistente = dataClasisf.Where(x => !dataExistente.Select(y => y.codigo).ToList().Contains(x.DescriptionAttr()));

            var dataInsert = new List<Estados>();
            foreach (var clasif in dataNoExistente)
                dataInsert.Add(new Estados() { estado = clasif.ToString(), codigo = clasif.DescriptionAttr() });

            this.Insert(dataInsert);
        }

        /// <summary>
        /// Obtener el id en base a un codigo de clasificacion
        /// </summary>
        /// <param name="dataEst"></param>
        /// <returns></returns>
        public int GetIdDataEnum(DataEstados dataEst)
        {
            var dataEst_ = this.FirsOrDefault(where: $"codigo = '{dataEst.DescriptionAttr()}'");
            return dataEst_ == null ? 0 : dataEst_.estadoId;
           
        }
    }
}
