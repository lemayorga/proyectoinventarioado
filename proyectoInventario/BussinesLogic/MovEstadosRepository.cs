﻿using System.Data;
using DataAccess;
using Entity;
using BussinesLogic.Extensions;
using System.ComponentModel;
using System;
using System.Linq;
using System.Collections.Generic;

namespace BussinesLogic
{
    public class MovEstadosRepository : BaseRepository<MovEstados>
    {

        protected sealed override string KeyName
        {
            get
            {
                return "movEstadoId";

            }
        }

        protected sealed override string TableName
        {
            get
            {
                return "dbo.MovEstados";
            }
        }

        protected sealed override MovEstados DataRowToModel(DataRow dr)
        {
            return dr == null ? null : dr.ToModel<MovEstados>();
        }
    }
}
