﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

//https://stackoverflow.com/questions/10727910/implement-a-generic-repository-pattern-using-old-ado-net
namespace BussinesLogic.Extensions
{
    public static class DataTableExtensions
    {
        /// <summary>
        /// Verificar si existe la columna
        /// </summary>
        /// <param name="r"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public static bool HasColumn(this IDataRecord r, string columnName)
        {
            try
            {
                return r.GetOrdinal(columnName) >= 0;
            }
            catch (IndexOutOfRangeException)
            {
                return false;
            }
        }

        /// <summary>
        /// Data table a Lista de  un modelo
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="table"></param>
        /// <returns></returns>
        public static List<T> ToList<T>(this DataTable table) where T : new()
        {
            IList<PropertyInfo> properties = typeof(T).GetProperties().ToList();
            List<T> result = new List<T>();

            foreach (var row in table.Rows)
            {
                var item = CreateItemFromRow<T>((DataRow)row, properties);
                result.Add(item);
            }

            return result;
        }

        /// <summary>
        /// Convertir un dataRow a un objeto modelo
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="row"></param>
        /// <param name="properties"></param>
        /// <returns></returns>
        private static T CreateItemFromRow<T>(DataRow row, IList<PropertyInfo> properties) where T : new()
        {
            Type classType = typeof(T);
            IList<PropertyInfo> propertyList = classType.GetProperties();

            T classObject = new T();
            foreach (PropertyInfo property in propertyList)
            {
                if (property != null && property.CanWrite)
                {
                    if (row[property.Name] != DBNull.Value)
                    {
                        if (string.IsNullOrEmpty(row[property.Name].ToString()) && property.IsNullableProperty())
                            continue;

                        Type typeNull = null;
                        if (property.IsNullableProperty())
                        {
                            typeNull = property.PropertyType.GenericTypeArguments[0];
                        }
                        object propertyValue = property.IsNullableProperty() 
                            ? Convert.ChangeType(row[property.Name], typeNull)
                            : Convert.ChangeType(row[property.Name],property.PropertyType);

                        property.SetValue(classObject, propertyValue, null);
                    }
                }
            }
            return classObject;
        }

        /// <summary>
        /// Saber si una propiedad es Nunable
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <returns></returns>
        public static bool IsNullableProperty(this PropertyInfo propertyInfo)
        => propertyInfo.PropertyType.Name.IndexOf("Nullable`", StringComparison.Ordinal) > -1;

        /// <summary>
        /// Crear el Item al modelo
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="row"></param>
        /// <returns></returns>
        public static T ToModel<T>(this DataRow row) where T : new()
        {
            IList<PropertyInfo> properties = typeof(T).GetProperties().ToList();
            return CreateItemFromRow<T>((DataRow)row, properties);
        }

        /// <summary>
        /// Obtener el Table de un Dataset
        /// </summary>
        /// <param name="ds"></param>
        /// <returns></returns>
        public static DataTable GetTableInDataSet(this DataSet ds)
        {
            if (ds == null) return null;

            return ds.Tables.Count <= 0 ? null : ds.Tables[0];
        }

    }
}
