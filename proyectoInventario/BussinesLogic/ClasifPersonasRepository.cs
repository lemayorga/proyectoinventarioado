﻿using System.Data;
using DataAccess;
using Entity;
using BussinesLogic.Extensions;

namespace BussinesLogic
{
    public class ClasifPersonasRepository : BaseRepository<ClasifPersonas>
    {
        protected sealed override string KeyName
        {
            get
            {
                return "clasifPersId";

            }
        }

        protected sealed override string TableName
        {
            get
            {
                return "dbo.ClasifPersonas";
            }
        }

        protected sealed override ClasifPersonas DataRowToModel(DataRow dr)
        {
            return dr == null ? null : dr.ToModel<ClasifPersonas>();
        }
    }
}
