﻿using System.Data;
using DataAccess;
using Entity;
using BussinesLogic.Extensions;

namespace BussinesLogic
{
    public class MovimientoDetRepository : BaseRepository<MovimientoDet>
    {
        protected sealed override string KeyName
        {
            get
            {
                return "movDetId";

            }
        }

        protected sealed override string TableName
        {
            get
            {
                return "dbo.MovimientoDet";
            }
        }

        protected sealed override MovimientoDet DataRowToModel(DataRow dr)
        {
            return dr == null ? null : dr.ToModel<MovimientoDet>();
        }

    }
}
