﻿using System.Data;
using DataAccess;
using Entity;
using BussinesLogic.Extensions;
using System.ComponentModel;
using System;
using System.Linq;
using System.Collections.Generic;

namespace BussinesLogic
{
    public class ClasficacionesRepository : BaseRepository<Clasificaciones>
    {
        #region Variables

        public enum DataClasif
        {
            [Description("CLIENTE")]
            clientes,
            [Description("PROVEEDOR")]
            proveedores
        }

        #endregion

        protected sealed override string KeyName
        {
            get
            {
                return "clasificacionId";

            }
        }

        protected sealed override string TableName
        {
            get
            {
                return "dbo.Clasificaciones";
            }
        }

        protected sealed override Clasificaciones DataRowToModel(DataRow dr)
        {
            return dr == null ? null : dr.ToModel<Clasificaciones>();
        }


        /// <summary>
        /// Insertar los datos si no existen
        /// </summary>
        public void InsertDataInit()
        {
           List<DataClasif> dataClasisf = Enum.GetValues(typeof(DataClasif)).Cast<DataClasif>().Select(x => x).ToList();
           List<string> dataClasisfDescript = dataClasisf.Select(x => x.DescriptionAttr()).ToList();


           List<Clasificaciones> dataExistente = this.GetAll(where: $" codigo IN({string.Join(",", dataClasisf.Select(p => "\'" + p.DescriptionAttr() + "\'"))})").ToList();

            var dataNoExistente = dataClasisf.Where(x => !dataExistente.Select(y => y.codigo).ToList().Contains(x.DescriptionAttr()));

            var dataInsert = new List<Clasificaciones>();
            foreach (var clasif in dataNoExistente)
                dataInsert.Add(new Clasificaciones() { clasificacion = clasif.ToString(), codigo = clasif.DescriptionAttr() });

            this.Insert(dataInsert);
        }

        /// <summary>
        /// Obtener el id en base a un codigo de clasificacion
        /// </summary>
        /// <param name="dataClasif"></param>
        /// <returns></returns>
        public int GetIdDataEnum(DataClasif dataClasif)
        {
            var clasisf = this.FirsOrDefault(where: $"codigo = '{dataClasif.DescriptionAttr()}'");
            return clasisf == null ? 0 : clasisf.clasificacionId;
           
        }
    }
}
