﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.SqlExtensions;
using BussinesLogic.Extensions;
using static BussinesLogic.TiposMovimientosRepository;
using System.Data;

namespace BussinesLogic.Reports
{
    public class ReportData :  SqlStoredProcedure
    {
        /// <summary>
        /// Obtener los movimientos detalle
        /// </summary>
        /// <param name="tipoMov"></param>
        /// <param name="fechaInicial"></param>
        /// <param name="fechaFinal"></param>
        /// <returns></returns>
        public DataTable GetMovimientosDetalle(DataTiposMov tipoMov, DateTime fechaInicial, DateTime fechaFinal)
        {
            this.ClearParameters();
            this.AddParameter("@tipoMov", tipoMov.DescriptionAttr());
            this.AddParameter("@fecha1", fechaInicial);
            this.AddParameter("@fecha2", fechaFinal);
            return this.ExecProcedure("rptMovimientosDetalle").GetTableInDataSet();            
        }

        /// <summary>
        /// Obtener los movimientos (master)
        /// </summary>
        /// <param name="tipoMov"></param>
        /// <param name="fechaInicial"></param>
        /// <param name="fechaFinal"></param>
        /// <returns></returns>
        public DataTable GetMovimientos(DataTiposMov tipoMov, DateTime fechaInicial, DateTime fechaFinal)
        {
            this.ClearParameters();
            this.AddParameter("@tipoMov", tipoMov.DescriptionAttr());
            this.AddParameter("@fecha1", fechaInicial);
            this.AddParameter("@fecha2", fechaFinal);
            return this.ExecProcedure("rptMovimientos").GetTableInDataSet();
        }

        /// <summary>
        /// Obtener las existencias de los productos
        /// </summary>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public DataTable GetExistencias(string nombre)
        {
            this.ClearParameters();
            this.AddParameter("@nombre", nombre);
            return this.ExecProcedure("rptExistencias").GetTableInDataSet();
        }
    }
}
