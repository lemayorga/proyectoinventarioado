﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.SqlExtensions
{
    /// <summary>
    ///  Clase que maneja el llamado a los procedimientos almacenados
    /// </summary>
    /// <remarks></remarks>
    public class SqlStoredProcedure
    {
        /// <summary>
        /// Sql Conexion
        /// </summary>
        private readonly SqlConexion objConex;

        /// <summary>
        ///  Declaracion de variables para el nombre de procedimiento y los parametros de los   procedimientos
        /// </summary>
        /// <remarks></remarks>
        private string _name;

        /// <summary>
        /// Coleccion de parametros
        /// </summary>
        private Collection _parameters;

        /// <summary>
        ///   Propiedad de la clase para asignar y obtener el nombre del procedimiento
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// Propiedad para obtener y asignar los parametros de los procediemientos almacenados
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public Collection Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <remarks></remarks>
        public SqlStoredProcedure()
        {
            Name = string.Empty;
            Parameters = new Collection();
            objConex = new SqlConexion();
        }

        /// <summary>
        /// Constructor que solo recibe el nombre del procedimiento e inicializa la colección.
        /// </summary>
        /// <param name="nNombre">Nombre del procedimiento almacenado</param>
        /// <remarks></remarks>
        public SqlStoredProcedure(string nNombre)
        {
            try
            {
                Name = nNombre;
                Parameters = new Collection();
                objConex = new SqlConexion();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nNombre">Nombre del procedimiento almacenado</param>
        /// <remarks></remarks>
        public SqlStoredProcedure(string nNombre, SqlConexion sqlConexion)
        {
            try
            {
                Name = nNombre;
                Parameters = new Collection();
                objConex = sqlConexion;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Limpiar parametros
        /// </summary>
        public void ClearParameters()
        {
            this.Parameters.Clear();
        }

        /// <summary>
        ///  Agrega los parametros del procedimiento y su respectivo valor.
        /// </summary>
        /// <param name="pName"></param>
        /// <param name="pValue"></param>
        /// <remarks></remarks>
        public void AddParameter(string pName, object pValue)
        {
            try
            {
                SqlStoredProcedureParameter iParametro = new SqlStoredProcedureParameter((pName[0].ToString().Trim() == "@" ? pName : "@" + pName), pValue);
                this.Parameters.Add(iParametro);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  Ejecuta el procedimiento almacenado.
        /// </summary>
        /// <param name="nombre">Modificacion del nombre a ejecutar, default el nombre inicializado en la instancia</param>>
        /// <returns></returns>
        /// <remarks></remarks>
        public DataSet ExecProcedure(string nombre = "")
        {
            try
            {
                string _nombre = (string.IsNullOrEmpty(nombre.Trim()) ? this.Name : nombre);
                SqlConexion Conn = new SqlConexion();

                SqlCommand sqlCmd = new SqlCommand(_nombre, objConex.getSqlConnection());
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.CommandTimeout = 120;

                SqlStoredProcedureParameter mParametro = null;

                foreach (SqlStoredProcedureParameter mParametro_loopVariable in this.Parameters)
                {
                    mParametro = mParametro_loopVariable;
                    SqlParameter pParam = new SqlParameter(mParametro.Variable, mParametro.GetTypeProperty);
                    pParam.Direction = ParameterDirection.Input;
                    pParam.Value = mParametro.Valor;
                    sqlCmd.Parameters.Add(pParam);
                }

                SqlDataAdapter sda = new SqlDataAdapter(sqlCmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);

                objConex.getSqlConnection().Close();
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Select de consulta
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="fieldsSelect"></param>
        /// <param name="innerJoin"></param>
        /// <param name="where"></param>
        /// <param name="groupby"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        public DataTable Select(string table, string fieldsSelect = "*", string innerJoin = "", string where = "", string groupby = "", string orderBy = "")
        {
            try
            {
                string sql = "SELECT " + fieldsSelect + " FROM " + table
                            + (!string.IsNullOrEmpty(innerJoin) ? " inner join " + innerJoin : "")
                            + (!string.IsNullOrEmpty(where) ? " WHERE " + where : "")
                            + (!string.IsNullOrEmpty(groupby) ? " group by " + groupby : "")
                            + (!string.IsNullOrEmpty(orderBy) ? " order by " + orderBy : "");
                SqlCommand sqlCmd = new SqlCommand(sql, objConex.getSqlConnection());
                SqlDataAdapter sda = new SqlDataAdapter(sqlCmd);
                DataTable ds = new DataTable();
                sda.Fill(ds);

                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objConex.getSqlConnection().Close();
            }
        }

        /// <summary>
        /// Select de consulta
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="fieldsSelect"></param>
        /// <param name="innerJoin"></param>
        /// <param name="where"></param>
        /// <param name="groupby"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        public DataTable Select(string table, string fieldsSelect = null,IDictionary<string, string> innerJoin = null, string where = "", IList<string> groupby = null, IList<string> orderBy = null)
        {
            try
            {
                string joinSQl = string.Empty;
                if (innerJoin !=  null)
                {
                    foreach (var item in innerJoin)
                        joinSQl += $" inner join {item.Key} ON {item.Value} ";
                }

                string sql = string.IsNullOrEmpty(fieldsSelect)  ? "*" : fieldsSelect;
                sql =   $"SELECT {sql} FROM {table} ";
                sql += joinSQl;
                sql += (!string.IsNullOrEmpty(where) ? " WHERE " + where : " ");
                           sql += (groupby == null ? " " : string.Join(",", groupby));
                           sql += (orderBy == null ? " " : string.Join(",", orderBy));
                SqlCommand sqlCmd = new SqlCommand(sql, objConex.getSqlConnection());
                SqlDataAdapter sda = new SqlDataAdapter(sqlCmd);
                DataTable ds = new DataTable();
                sda.Fill(ds);

                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objConex.getSqlConnection().Close();
            }
        }

        /// <summary>
        /// Ejecutar Query
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public DataTable ExecQuery(string query)
        {
            try
            {
                SqlCommand sqlCmd = new SqlCommand(query, objConex.getSqlConnection());
                SqlDataAdapter sda = new SqlDataAdapter(sqlCmd);
                DataTable ds = new DataTable();
                sda.Fill(ds);

                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objConex.getSqlConnection().Close();
            }
        }

        /// <summary>
        /// Retornar una cadena segun valor de variable
        /// </summary>
        /// <param name="DataType"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public string GetDataTypeStringOperator(PropertyInfo DataType, object value)
        {
            switch (DataType.PropertyType.Name)
            {
                case "Boolean": return value.ToString().ToUpper() == "FALSE" ? "0" : "1";
                case "Char": return $"'{value}'";
                case "SByte": return value.ToString();
                case "Int16": return value.ToString();
                case "Int32": return value.ToString();
                case "Int64": return value.ToString();
                case "Byte": return value.ToString();
                case "UInt16": return value.ToString();
                case "UInt32": return value.ToString();
                case "UInt64": return value.ToString();
                case "Single": return $"'{value}'";
                case "Double": return $"'{value}'";
                case "Decimal": return $"'{value}'";
                case "DateTime": return $"'{DateTime.Parse(value.ToString()).ToString("yyyy-MM-dd hh:mm:ss")}'";
                case "Guid": return $"'{value}'";
                case "String": return $"'{value}'";
                case "Object": return $"'{value}'";
                default:
                    if (DataType.PropertyType.IsGenericType && DataType.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                        return $"NULL";

                    return $"'{value}'";
            }
        }

        /// <summary>
        /// Retornar una cadena segun valor de variable
        /// </summary>
        /// <param name="DataType"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public string GetDataTypeStringOperat2(TypeCode typeCode, object value)
        {
            switch (typeCode)
            {
                case TypeCode.Boolean: return bool.Parse(value.ToString()) ? "1" : "0";
                case TypeCode.Char: return $"'{value}'";
                case TypeCode.SByte: return value.ToString();
                case TypeCode.Int16: return value.ToString();
                case TypeCode.Int32: return value.ToString();
                case TypeCode.Int64: return value.ToString();
                case TypeCode.Byte: return value.ToString();
                case TypeCode.UInt16: return value.ToString();
                case TypeCode.UInt32: return value.ToString();
                case TypeCode.UInt64: return value.ToString();
                case TypeCode.Single: return $"'{value}'";
                case TypeCode.Double: return $"'{value}'";
                case TypeCode.Decimal: return $"'{value}'";
                case TypeCode.DateTime: return $"'{DateTime.Parse(value.ToString()).ToString("yyyy-MM-dd hh:mm:ss")}'";
                case TypeCode.Empty: return $"'{value}'";
                case TypeCode.String: return $"'{value}'";
                case TypeCode.Object: return $"'{value}'";
                default: return $"'{value}'";
            }
        }
    }
}
