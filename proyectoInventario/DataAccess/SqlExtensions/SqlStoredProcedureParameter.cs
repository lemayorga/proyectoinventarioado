﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.SqlExtensions
{
    /// <summary>
    /// Clase que maneja las variables y su respectivo valor
    /// </summary>
    /// <remarks></remarks>
    public class SqlStoredProcedureParameter
    {
        private string mVariable;

        private object mValor;
        /// <summary>
        ///  Nombre de la variable, debe ser igual a la declarada en el procedimiento almacenado
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public string Variable
        {
            get { return mVariable; }
            set { mVariable = value; }
        }

        /// <summary>
        /// Valor de la variable, puede ser de cualquier tipo de dato. preferible que  coincida con las variables declaradas en GetTypeProperty
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public object Valor
        {
            get { return mValor; }
            set { mValor = value; }
        }

        /// <summary>
        ///  Se definen los posibles tipos de datos que se le van a enviar al procedimiento almacenado
        /// Esta lista podria aumentar conforme se usen otro tipo de variable.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public SqlDbType GetTypeProperty
        {
            get
            {
                if (mValor.GetType().FullName == "System.String;")
                {
                    return SqlDbType.NVarChar;
                }
                else if (mValor.GetType().FullName == "System.Int16;")
                {
                    return SqlDbType.Int;
                }
                else if (mValor.GetType().FullName == "System.Int32;")
                {
                    return SqlDbType.Int;
                }
                else if (mValor.GetType().FullName == "System.Int64;")
                {
                    return SqlDbType.BigInt;
                }
                else if (mValor.GetType().FullName == "System.Integer;")
                {
                    return SqlDbType.Int;
                }
                else if (mValor.GetType().FullName == "System.Decimal;")
                {
                    return SqlDbType.Decimal;
                }
                else if (mValor.GetType().FullName == "System.Double;")
                {
                    return SqlDbType.Float;
                }
                else if (mValor.GetType().FullName == "System.DateTime;")
                {
                    return SqlDbType.DateTime;
                }
                else if (mValor.GetType().FullName == "System.Byte;")
                {
                    return SqlDbType.Image;
                }

                return SqlDbType.NVarChar;
            }
        }

        /// <summary>
        ///   Constructor para la inicializacion de la variable.
        /// </summary>
        /// <param name="pVariable"></param>
        /// <param name="pValor"></param>
        /// <remarks></remarks>
        public SqlStoredProcedureParameter(string pVariable, object pValor)
        {
            try
            {
                this.Variable = pVariable;
                this.Valor = pValor;
            }
            catch (Exception ex)
            {
                throw new Exception("Error en la creacion del Parametro \n " + ex.Message);
            }
        }
    }
}
