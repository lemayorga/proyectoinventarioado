﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.SqlExtensions
{
    /// <summary>
    /// Clase que maneja la conexion con sql server
    /// </summary>
    /// <remarks></remarks>
    public class SqlConexion
    {
        /// <summary>
        /// Variable de almacenamiento de cadena de conexion
        /// </summary>
        private readonly string conexion;

        /// <summary>
        /// Constructor Vacio
        /// </summary>
        public SqlConexion()
        {
            conexion = ConfigurationManager.ConnectionStrings["SqlConexion"].ConnectionString;
        }

        /// <summary>
        /// Constructor con parametro de conexion string
        /// </summary>
        /// <param name="conectionString"></param>
        public SqlConexion(string conectionString)
        {
            conexion = conectionString;
        }

        /// <summary>
        /// Obtener la un SqlConection con la cadena de conexion
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public SqlConnection getSqlConnection()
        {
            return new SqlConnection(conexion);
        }

        /// <summary>
        /// Obtener la Cadena de Conexion
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public string conex_string()
        {
            return conexion;
        }
    }
}
