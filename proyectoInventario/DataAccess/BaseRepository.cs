﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using DataAccess.SqlExtensions;

//https://stackoverflow.com/questions/10727910/implement-a-generic-repository-pattern-using-old-ado-net
//https://codereview.stackexchange.com/questions/77514/class-to-datatable-or-datatable-to-class-mapper

namespace DataAccess
{
    public abstract class BaseRepository<T> : SqlStoredProcedure, IRepository<T> where T : class
    {
        /// <summary>
        /// Nombre de la Tabla
        /// </summary>
        protected abstract string TableName { get; }
        /// <summary>
        /// Nombre de la llave primaria
        /// </summary>
        protected abstract string KeyName { get; }

        /// <summary>
        /// Convercion de un DataRow a un registro de la entidad
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        protected abstract T DataRowToModel(DataRow dr);

        /// <summary>
        /// Convertir un DataTable a una lista de la Entidad
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        protected ICollection<T> DataTableToCollection(DataTable dt)
        {
            if (dt == null) return null;

            return dt.AsEnumerable().Select(x => DataRowToModel(x)).ToList();
        }

        /// <inheritdoc />
        public virtual bool Delete(IList<int> collectionId)
        {
            if (collectionId == null)
                return false;

            var query = $"delete from {TableName} where {KeyName ?? "Id"} in ({string.Join(",", collectionId)})";
            this.ExecQuery(query);
            return true;
        }

        /// <inheritdoc />
        public virtual bool Delete(int keyValue)
        {
            if (string.IsNullOrEmpty(KeyName) || keyValue == 0)
                return false;

            var query = $"delete from {TableName} where {KeyName ?? "Id"} = {keyValue}";
            this.ExecQuery(query);
            return true;
        }

        /// <inheritdoc />
        public virtual ICollection<T> GetAll(string tableNameAlias = "", string innerJoin = "", string where = "", string groupby = "", string orderBy = "")
        {
            var dt = this.Select($"{TableName} {tableNameAlias}", "*", innerJoin, where, groupby, orderBy);
            return DataTableToCollection(dt);
        }

        /// <inheritdoc />
        public virtual ICollection<T> Get(string tableNameAlias = "", IDictionary<string, string> innerJoin = null, string where = "", IList<string> groupby = null, IList<string> orderBy = null)
        {
            var dt = this.Select($"{TableName} {tableNameAlias}", null, innerJoin, where, groupby, orderBy);
            return DataTableToCollection(dt);
        }

        /// <inheritdoc />
        public virtual T FirsOrDefault(string where = "", string innerJoin = "")
        {
            var dt = this.Select(TableName, "TOP 1 *", innerJoin, where, string.Empty, string.Empty);
            var data =  DataTableToCollection(dt);
            return data == null ? null : data.FirstOrDefault<T>();
        }

        /// <inheritdoc />
        public T GetById(int id)
        {
            var query = $"select * from {TableName} where {KeyName ?? "Id"} = {id}";
            var dt = this.ExecQuery(query);

            if (dt == null) return null;
            if (dt.Rows.Count <= 0) return null;

            DataRow dr = dt.Select()[0];
            return DataRowToModel(dr);
        }

        /// <inheritdoc />
        public virtual void Insert(T entity)
        {
            var properties = this.GetPropertiesModel().Where(x => x.Name != KeyName);
            var propertiesJoin = string.Join(",", properties.Select(x => $"[{x.Name}]").ToArray());
            var propertiesValues = string.Join(",", properties.Select(x => GetDataTypeStringOperator(x, x.GetValue(entity))));

            var query = $"INSERT INTO {TableName}  ({propertiesJoin}) VALUES ({propertiesValues}) SELECT SCOPE_IDENTITY()";
            var dt = this.ExecQuery(query);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    var propKey = this.GetPropertiesModel().FirstOrDefault(x => x.Name == KeyName);
                    if (propKey != null)
                    {
                        var keyValue = dt.Rows[0][0];
                        propKey.SetValue(entity, Convert.ChangeType(keyValue, Type.GetType(propKey.PropertyType.FullName)));
                    }
                }
            }
        }

        /// <inheritdoc />
        public virtual void Insert(IList<T> entity)
        {
            foreach (T item in entity)
                Insert(item);
        }

        /// <inheritdoc />
        public virtual void Update(T entity)
        {
            var propertiesUpdate = new List<string>();
            var properties = this.GetPropertiesModel();

            PropertyInfo valueKeyName = properties != null ? properties.FirstOrDefault(x => x.Name == KeyName) : null;
            foreach (PropertyInfo pr in properties.Where(x => x.Name != KeyName))
                propertiesUpdate.Add($"[{pr.Name}] = {GetDataTypeStringOperator(pr, pr.GetValue(entity))}");

            var query = $"UPDATE {TableName} SET {string.Join(",", propertiesUpdate)} ";
                query += $"WHERE {KeyName} = {valueKeyName.GetValue(entity)} ";
            var dt = this.ExecQuery(query);
        }


        /// <inheritdoc />
        public virtual int Count(string fieldCount = "", string where = "", string innerJoin = "")
        {
            fieldCount = string.IsNullOrEmpty(fieldCount) ? this.KeyName : fieldCount;
            var dt = this.Select(TableName, fieldsSelect: $"COUNT({fieldCount})", innerJoin: innerJoin, where: where);
            var cont = dt == null ? 0 
                       : dt.Rows.Count != 0 ? int.Parse(dt.Rows[0][0].ToString()) : 0 ;
            return cont;
        }


        /// <inheritdoc />
        public virtual int Sum(string fieldCount = "", string where = "", string innerJoin = "")
        {
            fieldCount = string.IsNullOrEmpty(fieldCount) ? this.KeyName : fieldCount;
            var dt = this.Select(TableName, fieldsSelect: $"SUM({fieldCount})", innerJoin: innerJoin, where: where);
            var cont = dt == null ? 0
                       : dt.Rows.Count != 0 ? int.Parse(dt.Rows[0][0].ToString()) : 0;
            return cont;
        }

        /// <inheritdoc />
        public IList<PropertyInfo> GetPropertiesModel()
        {
            return  typeof(T).GetProperties(BindingFlags.Public |
                                                  BindingFlags.Instance |
                                                  BindingFlags.DeclaredOnly).ToList();
        }

       /// <summary>
       /// Obtener el nombre de la llave primaria establecida 
       /// </summary>
       /// <returns></returns>
        public string GetKeyName()
        {
            return KeyName;
        }

        /// <summary>
        /// Obtener el nombre de la Tabla de la Base de datos
        /// </summary>
        /// <returns></returns>
        public string GetTableName()
        {
            return TableName;
        }

        /// <inheritdoc />
        public DataTable GetDataOnDatatable(string tableNameAlias = "", string innerJoin = "", string where = "", string groupby = "", string orderBy = "")
        {
           return Select($"{TableName} {tableNameAlias}", "*", innerJoin, where, groupby, orderBy);
        }
    }
}
