﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public interface IRepository<T> where T : class
    {
        /// <summary>
        /// Obtener una entidad por ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T GetById(int id);
        /// <summary>
        /// Obtener el primer registro
        /// </summary>
        /// <param name="where">Condicion</param>
        /// <param name="innerJoin">JJoin de tablas</param>
        /// <returns></returns>
        T FirsOrDefault(string where = "", string innerJoin = "");
        /// <summary>
        /// Listar todos los registros
        /// </summary>
        /// <param name="tableNameAlias">Alias de la tabla</param>
        /// <param name="innerJoin">Join con otras entidades de tablas</param>
        /// <param name="where">Condicion</param>
        /// <param name="groupby">Columnas para agrupar consultar</param>
        /// <param name="orderBy">Coluumnas para ordernar consulta</param>
        /// <returns></returns>
        ICollection<T> GetAll(string tableNameAlias = "", string innerJoin = "", string where = "", string groupby = "", string orderBy = "");
        /// <summary>
        /// Obtener los registros
        /// </summary>
        /// <param name="tableNameAlias">Alias de la tabla</param>
        /// <param name="innerJoin">Join con otras entidades de tablas</param>
        /// <param name="where">Condicion</param>
        /// <param name="groupby">Columnas para agrupar consultar</param>
        /// <param name="orderBy">Coluumnas para ordernar consulta</param>
        /// <returns></returns>
        ICollection<T> Get(string tableNameAlias = "", IDictionary<string, string> innerJoin = null, string where = "", IList<string> groupby = null, IList<string> orderBy = null);
        /// <summary>
        /// Obtener los registros en un DataTale
        /// </summary>
        /// <param name="tableNameAlias"></param>
        /// <param name="innerJoin"></param>
        /// <param name="where"></param>
        /// <param name="groupby"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        DataTable GetDataOnDatatable(string tableNameAlias = "", string innerJoin = "", string where = "", string groupby = "", string orderBy = "");
        /// <summary>
        /// Ejecutar Update a entidad
        /// </summary>
        /// <param name="entity"></param>
        void Update(T entity);
        /// <summary>
        /// Ejecutar Insert a la entidad
        /// </summary>
        /// <param name="entity"></param>
        void Insert(T entity);
        /// <summary>
        /// Ejecutar Insert de una Coleccion de registro
        /// </summary>
        /// <param name="entity"></param>
        void Insert(IList<T> entity);
        /// <summary>
        /// Ejecutar Delete a la entidad
        /// </summary>
        /// <param name="keyValue">LLave Primaria</param>
        /// <returns></returns>
        bool Delete(int keyValue);
        /// <summary>
        ///  Sumar la cantidad de regitros existentes segun consulta
        /// </summary>
        /// <param name="fieldCount"></param>
        /// <param name="where"></param>
        /// <param name="innerJoin"></param>
        /// <returns></returns>
        int Sum(string fieldCount = "", string where = "", string innerJoin = "");
        /// <summary>
        /// Contar la cantidad de regitros existentes segun consulta
        /// </summary>
        /// <param name="fieldCount"></param>
        /// <param name="where"></param>
        /// <param name="innerJoin"></param>
        /// <returns></returns>
        int Count(string fieldCount = "", string where = "", string innerJoin = "");
        /// <summary>
        /// Ejecutar Delete a una coleccion de registro
        /// </summary>
        /// <param name="collectionId"></param>
        /// <returns></returns>
        bool Delete(IList<int> collectionId);
        /// <summary>
        /// Obtener las propiedades del modelo
        /// </summary>
        /// <returns></returns>
        IList<PropertyInfo> GetPropertiesModel();
    }
}
