﻿using InventarioForms.Code.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventarioForms.reportes.Existencias
{
    public class ExistenciasParameter : IReportParameters
    {
        public string nombre { get; set; }
    }
}
