﻿using InventarioForms.Code.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventarioForms.reportes.Existencias
{
    public class Existencias : IReportData
    {
        public int productoId { get; set; }
        public string producto { get; set; }
        public double costo { get; set; }
        public double precio { get; set; }
        public int existencia { get; set; }

    }
}
