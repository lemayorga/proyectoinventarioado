﻿using BussinesLogic.Extensions;
using InventarioForms.Code.Interface;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventarioForms.reportes.Existencias
{
    public class ExistenciasBase : BaseReport
    {
        /// <inheritdoc/>
        public ExistenciasBase(IReportParameters parametros_) : base(parametros_)
        {

        }

        /// <inheritdoc/>
        protected override string nameReport
        {
            get
            {
                return "Existencias.rptExistencias";
            }
        }
        /// <inheritdoc/>
        public override ReportParameter[] SetParameters()
        {
            return parametersArray ?? new ReportParameter[] { };
        }

        /// <inheritdoc/>
        public override ReportParameter[] GetParameterUser()
        {
            return parametersArray ?? new ReportParameter[] { };
        }

        /// <inheritdoc/>
        public override void GetData()
        {
            var p = (ExistenciasParameter)parameterUser;
            var result = reportQuerys.GetExistencias(p.nombre).ToList<Existencias>().Select(x => (IReportData)x);

            reportData.Add("dsBody", result.ToList());

        }
    }
}
