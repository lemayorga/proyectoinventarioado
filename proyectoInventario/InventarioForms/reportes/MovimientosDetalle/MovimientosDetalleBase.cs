﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InventarioForms.Code.Interface;
using BussinesLogic;
using BussinesLogic.Reports;
using BussinesLogic.Extensions;
using InventarioForms.Code;

namespace InventarioForms.reportes.MovimientosDetalle
{
    public class rptDataMovimientosDetale : BaseReport
    {
        /// <inheritdoc/>
        public rptDataMovimientosDetale(IReportParameters parametros_) : base(parametros_)
        {            
        
        }

        /// <inheritdoc/>
        protected override string nameReport
        {
            get
            {
                return "MovimientosDetalle.rptMovimientosDetalle";
            }
        }

        /// <inheritdoc/>
        public override ReportParameter[] SetParameters()
        {
            parametersArray = new ReportParameter[]
            {
                new ReportParameter("Decimales", Constantes.CANT_DECIMALES.ToString())
            };
            return parametersArray;
        }

        /// <inheritdoc/>
        public override ReportParameter[] GetParameterUser()
        {
            var par = (MovimientosDetalleParameter)parameterUser;
            parametersArray = parametersArray ?? new ReportParameter[] { };
            var p = parametersArray.ToList();
            p.Add(new ReportParameter("fInicio", par.fechaInicial.ToString()));
            p.Add(new ReportParameter("fFinal", par.fechaFinal.ToString()));
            parametersArray = p.ToArray();
            return parametersArray;
        }

        /// <inheritdoc/>
        public override void GetData()
        {
            var p = (MovimientosDetalleParameter)parameterUser;
            var result = reportQuerys.GetMovimientosDetalle(p.codigo, p.fechaInicial, p.fechaFinal).ToList<MovimientosDetalle>().Select(x => (IReportData) x);

            reportData.Add("dsBoody", result.ToList());

        }
    }
}
