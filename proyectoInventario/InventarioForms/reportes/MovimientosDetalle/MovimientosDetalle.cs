﻿using InventarioForms.Code.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventarioForms.reportes.MovimientosDetalle
{
    public class MovimientosDetalle  : IReportData
    {
        public int movimientoId { get; set; }
        public string tipo { get; set; }
        public string referencia { get; set; }
        public DateTime fecha { get; set; }
        public string tipoPersona { get; set; }
        public string persona { get; set; }
        public int productoId { get; set; }
        public string producto { get; set; }
        public string marca { get; set; }
        public string modelo { get; set; }
        public double costo { get; set; }
        public double precio { get; set; }
        public int cantidad { get; set; }
    }
}
