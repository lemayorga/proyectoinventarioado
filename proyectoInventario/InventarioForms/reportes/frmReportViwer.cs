﻿using InventarioForms.Code;
using InventarioForms.Code.Interface;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InventarioForms.reportes
{
    public partial class frmReportViwer : Form
    {
        public frmReportViwer()
        {
            InitializeComponent();
        }

        private void frmReportViwer_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();

        }

        /// <summary>
        /// Generar el reporte
        /// </summary>
        /// <param name="report"></param>
        public void GenerateReport(IReport report)
        {
            try
            {
                report.GetData();

                this.reportViewer1.LocalReport.ReportEmbeddedResource = report.fullNameReport;
                report.SetParameters();
                report.GetParameterUser();

                this.reportViewer1.LocalReport.SetParameters(report.parametersArray);


                report.GetReport(this.reportViewer1.LocalReport.DataSources);
                this.reportViewer1.RefreshReport();

                if (!report.CountData())
                {
                    MsgAlert.Information("No hay datos a mostrar con los parametros proporcionados. \n Favor intenten con otros datos.");
                    return;
                }

                this.Show();
            }
            catch (Exception ex)
            {
                MsgAlert.Error(ex);
            }

        }
    }
}
