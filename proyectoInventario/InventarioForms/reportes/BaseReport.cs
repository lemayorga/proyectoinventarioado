﻿using BussinesLogic.Reports;
using InventarioForms.Code;
using InventarioForms.Code.Interface;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventarioForms.reportes
{
    public abstract class BaseReport : IReport
    {
        /// <summary>
        /// Variable para llamado de las consultas
        /// </summary>
        protected ReportData reportQuerys;
        /// <summary>
        /// Nombre del reporte
        /// </summary>
        protected abstract string nameReport { get; }
        /// <summary>
        /// Parametros por parte del usuario
        /// </summary>
        protected IReportParameters parameterUser;
        /// <summary>
        /// Nombre de la ruta del reporte
        /// </summary>
        private string pathsReports = "InventarioForms.reportes.";
        /// <summary>
        /// Ruta completa del reporte
        /// </summary>
        public string fullNameReport { get { return pathsReports + nameReport + ".rdlc"; } }
        /// <summary>
        /// Diccionarion con los datos de los Ds de las consultas
        /// </summary>
        public Dictionary<string, List<IReportData>> reportData = null;

        /// <summary>
        /// Parametros
        /// </summary>
        private ReportParameter[] parametrosArray_ { get; set; }
        /// <summary>
        /// Parametros
        /// </summary>
        public ReportParameter[] parametersArray
        {
            get
            {
                return parametrosArray_ ?? new ReportParameter[] { };
            }

            set
            {
                parametrosArray_ = value;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="parametros_"></param>
        public BaseReport(IReportParameters parametros_)
        {
            reportQuerys = new ReportData();
            parameterUser = parametros_;
            reportData =  new Dictionary<string, List<IReportData>>();
       }

        /// <summary>
        /// Obtener la data
        /// </summary>
        public virtual void GetData()
        {

        }


        /// <summary>
        /// Obtener los parametros
        /// </summary>
        public virtual ReportParameter[] GetParameterUser()
        {
            return parametersArray;
        }

        /// <summary>
        /// Generar data al reporte
        /// </summary>
        /// <param name="dataSourceCollection"></param>
        /// <returns></returns>
        public virtual ReportDataSourceCollection GetReport(ReportDataSourceCollection dataSourceCollection)
        {
            dataSourceCollection.Clear();

            foreach (var item in reportData)
            {
                dataSourceCollection.Add(new ReportDataSource(item.Key, item.Value));
            }

            return dataSourceCollection;
        }

        /// <summary>
        /// Establecer parametros
        /// </summary>
        /// <returns></returns>
        public virtual ReportParameter[] SetParameters()
        {
            return parametersArray ?? new ReportParameter[] { };
        }

        /// <summary>
        /// Saber si hay datos 
        /// </summary>
        /// <returns></returns>
        public bool CountData()
        {
            return this.reportData.Count > 0;
        }

        public void VerificarParameter(ref ReportViewer rptv)
        {
            var parameters = rptv.LocalReport.GetParameters();
        }
    }
}
