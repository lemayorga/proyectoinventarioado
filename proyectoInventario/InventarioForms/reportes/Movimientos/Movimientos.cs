﻿using InventarioForms.Code.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventarioForms.reportes.Movimientos
{
    public class Movimientos : IReportData
    {
        public int movimientoId { get; set; }
        public string tipo { get; set; }
        public string referencia { get; set; }
        public DateTime fecha { get; set; }
        public string tipoPersona { get; set; }
        public string persona { get; set; }
        public double subtotal { get; set; }
        public double descuento { get; set; }
        public double impuesto { get; set; }
        public double total { get; set; }
    }
}
