﻿using BussinesLogic.Extensions;
using InventarioForms.Code;
using InventarioForms.Code.Interface;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventarioForms.reportes.Movimientos
{
    public class MovimientosBase : BaseReport
    {
        /// <inheritdoc/>
        public MovimientosBase(IReportParameters parametros_) : base(parametros_)
        {

        }

        /// <inheritdoc/>
        protected override string nameReport
        {
            get
            {
                return "Movimientos.rptMovimientos";
            }
        }

        /// <inheritdoc/>
        public override ReportParameter[] SetParameters()
        {
            parametersArray = new ReportParameter[]
            {
                new ReportParameter("Decimales", Constantes.CANT_DECIMALES.ToString())
            };
            return parametersArray;
        }

        /// <inheritdoc/>
        public override ReportParameter[] GetParameterUser()
        {
            var par = (MovimientosParameter)parameterUser;
            parametersArray = parametersArray ?? new ReportParameter[] { };
            var p = parametersArray.ToList();
            p.Add(new ReportParameter("fInicio", par.fechaInicial.ToString()));
            p.Add(new ReportParameter("fFinal", par.fechaFinal.ToString()));
            parametersArray = p.ToArray();
            return parametersArray;
        }

        /// <inheritdoc/>
        public override void GetData()
        {
            var p = (MovimientosParameter)parameterUser;
            var result = reportQuerys.GetMovimientos(p.codigo, p.fechaInicial, p.fechaFinal).ToList<Movimientos>().Select(x => (IReportData)x);

            reportData.Add("dsBody", result.ToList());

        }
    }
}
