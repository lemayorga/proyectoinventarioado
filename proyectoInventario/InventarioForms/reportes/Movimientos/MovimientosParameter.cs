﻿using InventarioForms.Code.Interface;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BussinesLogic.TiposMovimientosRepository;

namespace InventarioForms.reportes.Movimientos
{
    public class MovimientosParameter : IReportParameters
    {
        public DataTiposMov codigo { get; set; }
        public DateTime fechaInicial { get; set; }
        public DateTime fechaFinal { get; set; }
    }
}
