﻿using Entity.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventarioForms.Code
{
    ///// <summary>
    ///// Usuario Actual
    ///// </summary>
    //public string USUARIO = "ANONIMO";
    //public string ROLES = "";

    public static class Constantes
    {
        #region Constantes
        /// <summary>
        /// Valor del impuesto
        /// </summary>
        public const int IMPUESTO = 15;
        /// <summary>
        /// Vaor maximo de descuento
        /// </summary>
        public const int MAYOR_DESCUENTO = 100;
        /// <summary>
        /// Cantidad de decimales a utilizar en el sistema
        /// </summary>
        public const int CANT_DECIMALES = 2;
        /// <summary>
        /// Cantidad sin decimales a utilizar en el sistema
        /// </summary>
        public const int CANT_SIN_DECIMALES = 0;
        /// <summary>
        /// Formato de fechas
        /// </summary>
        public const string FORMAT_DATE = "dd/MM/yyyy";
        /// <summary>
        /// Usuario Actual
        /// </summary>
        public static string USER_NAME = "ANONIMO";
        /// <summary>
        /// Roles del usuario Actual
        /// </summary>
        public static string ROLES = "";
        /// <summary>
        /// ROl de super usuario
        /// </summary>
        public static string ROL_SA = "sadministrador";
        /// <summary>
        /// Nombre de la app
        /// </summary>
        public const string APP_NAME = "SISTEMA DE INVENTARIO Y VENTA";
        /// <summary>
        /// Usuario actual
        /// </summary>
        public static AppUsers USUARIO_ACTUAL = null;
        /// <summary>
        /// Rol actual
        /// </summary>
        public static AppRoles ROL_ACTUAL = null;
        /// <summary>
        /// Base de datos actual
        /// </summary>
        public static string MY_BASE_DATOS = null;
        /// <summary>
        /// Servido actual
        /// </summary>
        public static string MY_SERVIDOR = null;
        /// <summary>
        /// Variable app de sistema
        /// </summary>
        public static App APP = new App();
        #endregion
    }
}
