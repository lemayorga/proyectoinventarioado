﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InventarioForms.Code;
using Entity.Security;
using BussinesLogic.Security;

namespace InventarioForms.Code
{
    public static class MyControls
    {
        #region Variables

        /// <summary>
        /// Datos cargado de listado de controles por el rol
        /// </summary>
        //public static List<FormContolsRol> listaControles = null;
        /// <summary>
        /// Repositorio de controles
        /// </summary>
        private static Controls controls = new Controls();

        #endregion 

        #region Metodos

        /// <summary>
        /// Recursos para formularios de Visual Studio
        /// </summary>
        /// <param name="myFormulario"></param>
        public static void UsuarioRecursos(Form myFormulario)
        {
            try
            {
                if (Constantes.ROLES  == Constantes.ROL_SA)
                {
                    foreach (Control myControl in myFormulario.Controls)
                    {
                        if (myControl is Button)
                        {
                            myControl.Enabled = true;
                        }

                        if (myControl is GroupBox)
                        {
                            UsuarioRecursos((GroupBox)myControl, true, true);
                        }
                    }
                    return;
                }

                //if (listaControles == null)
                //{
                   var listaControles = controls.GetControlesRolUsuario(Constantes.ROL_ACTUAL);
                //}

                var listaTemp = listaControles.Where(x => x.formName.Trim() == myFormulario.Name.Trim());
                foreach (var myNameControl in listaTemp)
                {
                    foreach (Control myControl in myFormulario.Controls)
                    {
                        if (myControl is Button)
                        {
                            if (myControl.Name.Trim() == myNameControl.controlName.Trim())
                                myControl.Enabled = true;
                        }

                        if (myControl is GroupBox)
                        {
                            UsuarioRecursos((GroupBox)myControl, true, false);
                        }
                    }
                }
            }
            catch (Exception ex) { Console.Write(ex.Message); }
        }

        /// <summary>
        /// Recursos para GroupControl de Devexpress
        /// </summary>
        /// <param name="myGroupControl"></param>
        /// <param name="value"></param>
        /// <param name="usuarioSA"></param>
        public static void UsuarioRecursos(GroupBox myGroupControl, bool value, bool usuarioSA = false)
        {
            try
            {
                if (usuarioSA)
                {
                    foreach (Control myControl in myGroupControl.Controls)
                    {
                        if (myControl is Button)
                        {
                            myControl.Enabled = value;
                        }
                    }
                }
                else
                {
                    //if (listaControles == null)
                    //{
                       var listaControles = controls.GetControlesRolUsuario(Constantes.ROL_ACTUAL);
                    //}

                    foreach (var myNameControl in listaControles)
                    {
                        foreach (Control myControl in myGroupControl.Controls)
                        {
                            if (myControl is System.Windows.Forms.Button)
                            {
                                if (((Button)myControl).Name.Trim() == myNameControl.controlName.Trim())
                                    ((Button)myControl).Enabled = value;
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { Console.Write(ex.Message); }
        }

        #endregion
    }
}
