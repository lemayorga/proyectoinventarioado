﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace InventarioForms
{
    public static class FilterCondition
    {
        private enum Condiciones
        {
            Igual,
            Parecido    
        }

        /// <summary>
        /// Popularisar combobox con datos de condiciones
        /// </summary>
        /// <param name="cb"></param>
        public static void ComboxCondition(this ComboBox cb)
        {
            try
            {
                cb.DataSource = Enum.GetValues(typeof(Condiciones)).Cast<Condiciones>().Select(x => x.ToString()).ToList();
            }
            catch(Exception ex)
            {
                 MsgAlert.Error(ex);
            }
        }

        /// <summary>
        /// Popularisar combobox con las columnas de un gridview
        /// </summary>
        /// <param name="cb"></param>
        /// <param name="gv"></param>
        public static void ComboxProperties(this ComboBox cb, DataGridView gv)
        {
            try
            {
                var properties = new List<string>();
                foreach (DataGridViewColumn column in gv.Columns)
                    properties.Add(column.Name);

                cb.DataSource = properties;
            }
            catch (Exception ex)
            {
                 MsgAlert.Error(ex);
            }
        }

        /// <summary>
        /// Crear condicion segun paremtro y condicion
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="_operator"></param>
        /// <param name="parameter"></param>
        /// <param name="gr"></param>
        /// <returns></returns>
        public static string CreateConditionFilter(this DataGridView gv,string propertyName, string _operator, object parameter)
        {
            if (string.IsNullOrEmpty(propertyName)) return string.Empty;
            var column = gv.Columns[propertyName];

            if (column == null) return string.Empty;
            var stringSql = FilterCondition.GetDataTypeStringOperator(column.ValueType, parameter);

            var names = Enum.GetNames(typeof(Condiciones));
            switch ((Condiciones)Enum.Parse(typeof(Condiciones), _operator))
            {
                case Condiciones.Igual:
                    stringSql = $"{propertyName} = {stringSql}";

                    break;
                case Condiciones.Parecido:
                    stringSql = $"{propertyName} like '%{stringSql.Replace("\'", "")}%'";
                    break;
            }
            return stringSql;
        }

        /// <summary>
        /// Returns the SQL data type equivalent, as a string for use in SQL script generation methods.
        /// </summary>
        private static string GetDataTypeStringOperator(Type DataType, object value)
        {
            switch (DataType.Name)
            {
                case "Boolean": return value.ToString();
                case "Char": return $"'{value}'";
                case "SByte": return value.ToString();
                case "Int16": return value.ToString();
                case "Int32": return value.ToString();
                case "Int64": return value.ToString();
                case "Byte": return value.ToString();
                case "UInt16": return value.ToString();
                case "UInt32": return value.ToString();
                case "UInt64": return value.ToString();
                case "Single": return $"'{value}'";
                case "Double": return $"'{value}'";
                case "Decimal": return $"'{value}'";
                case "DateTime": return $"'{DateTime.Parse(value.ToString()).ToString("yyyy-MM-dd hh:mm:ss")}'";
                case "Guid": return $"'{value}'";
                case "String": return $"'{value}'";
                case "Object": return $"'{value}'";
                default: return $"'{value}'";
            }
        }

    }
}
