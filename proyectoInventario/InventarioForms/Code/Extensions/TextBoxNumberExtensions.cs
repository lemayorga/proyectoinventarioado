﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InventarioForms.Code.Extensions
{
    public static class TextBoxNumberExtensions
    {
        /// <summary>
        /// Validar numeros decimales en el textbox decimal
        /// </summary>
        /// <param name="textBox"></param>
        /// <param name="e"></param>
        public static void KeyPressDecimal(this TextBox textBox, KeyPressEventArgs e)
        {
            // Verify that the pressed key isn't CTRL or any non-numeric digit
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // If you want, you can allow decimal (float) numbers
            if ((e.KeyChar == '.') && (textBox.Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }
        /// <summary>
        /// Evitar copiar y pegar en textbox decimal
        /// </summary>
        /// <param name="textBox"></param>
        /// <param name="e"></param>
        public static void KeyDownDecimal(this TextBox textBox, KeyEventArgs e)
        {
            if (e.Control == true) MsgAlert.Information("Opción de Copiar y Pegar esta deshabilitada para este componente");
        }

        /// <summary>
        /// Para celldas de grid 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void Column_KeyPressDecimal(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar)  && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if (e.KeyChar == '.'
                && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Para celldas de grid 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void Column_KeyPressNumber(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Texto solo en mayuscula
        /// </summary>
        /// <param name="text"></param>
        public static void OnlyUpper(this TextBox text)
        {
            text.CharacterCasing = CharacterCasing.Upper;
        }
    }
}
