﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InventarioForms.Code.Extensions
{
    public static class GridViewExtension
    {
        /// <summary>
        /// Extencion del gridViewData para dar estilo personalizado para la App
        /// </summary>
        /// <param name="grid"></param>
        public static void FormaStyle(this DataGridView grid, DataGridViewEditMode editMode = DataGridViewEditMode.EditProgrammatically)
        {
            grid.RowsDefaultCellStyle.BackColor = Color.Azure;
            grid.AlternatingRowsDefaultCellStyle.BackColor = Color.LightSkyBlue;
            grid.CellBorderStyle = DataGridViewCellBorderStyle.SunkenVertical;

            grid.DefaultCellStyle.SelectionBackColor = Color.GreenYellow;
            grid.DefaultCellStyle.SelectionForeColor = Color.Black;

            grid.DefaultCellStyle.WrapMode = DataGridViewTriState.False;
            grid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            grid.AllowUserToResizeColumns = true;

            foreach (DataGridViewColumn column in grid.Columns)
                column.HeaderText = column.HeaderText.ToUpper();

            grid.EditMode = editMode;
            grid.AllowUserToAddRows = false;

            grid.DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;
            grid.ColumnHeadersDefaultCellStyle.Font = new Font(grid.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
            grid.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            grid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            grid.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
        }

        /// <summary>
        /// Filtro 
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="filtro"></param>
        public static void Filter(this DataGridView grid, string filtro)
        {
            BindingSource bsTemp = (BindingSource)grid.DataSource;
            DataTable dt = (DataTable)bsTemp.DataSource;
            dt.DefaultView.RowFilter = filtro;
            grid.DataSource = bsTemp;
        }

        /// <summary>
        /// Filtracion de un solo valor
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="valor"></param>
        /// <param name="argumentos"></param>
        public static void FilterOneValue(this DataGridView grid, string valor, string[] argumentos)
        {
            string filtro = string.Empty;
            for (int i = 0; i < argumentos.Length; i++)
                filtro +=  (i == 0 ? string.Empty : "OR")  + $" {argumentos[i]} like '%{valor}%' ";
     

            BindingSource bsTemp = (BindingSource)grid.DataSource;
            DataTable dt = (DataTable)bsTemp.DataSource;
            dt.DefaultView.RowFilter = filtro;
            grid.DataSource = bsTemp;
        }
    }
}
