﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InventarioForms.Code.Extensions
{
    public static class ComboBoxExtensions
    {
        /// <summary>
        /// Populizar un comboBox
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="combo"></param>
        /// <param name="data"></param>
        /// <param name="valueMember"></param>
        /// <param name="displayMember"></param>
        public static void Populate(this ComboBox combo, DataTable data, string valueMember, string displayMember)
        {
            combo.DataSource = data;
            combo.ValueMember = valueMember;
            combo.DisplayMember = displayMember;
            combo.StyleCustomer();
        }

        /// <summary>
        /// Populizar un comboBox
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="combo"></param>
        /// <param name="data"></param>
        /// <param name="valueMember"></param>
        /// <param name="displayMember"></param>
        public static void Populate<T>(this ComboBox combo,IList<T> data,string valueMember,string displayMember)
        {
            combo.DataSource = data;
            combo.ValueMember = valueMember;
            combo.DisplayMember = displayMember;
            combo.StyleCustomer();
        }

        /// <summary>
        /// Populizar un comboBox
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="combo"></param>
        /// <param name="data"></param>
        /// <param name="valueMember"></param>
        /// <param name="displayMember"></param>
        public static void Populate<T>(this ComboBox combo, IEnumerable<T> data, string valueMember, string displayMember)
        {
            combo.Populate(data.ToList(), valueMember, displayMember);
        }

        /// <summary>
        /// Estilo personalizado al combox
        /// </summary>
        /// <param name="combo"></param>
        private static void StyleCustomer(this ComboBox combo)
        {
            combo.DropDownStyle = ComboBoxStyle.DropDownList;
            combo.SelectedValue = -1;
        }

        /// <summary>
        /// Populizar un comboBox
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="combo"></param>
        /// <param name="data"></param>
        /// <param name="valueMember"></param>
        /// <param name="displayMember"></param>
        public static void Populate<T>(this DataGridViewComboBoxColumn combo, IList<T> data, string valueMember, string displayMember)
        {
            combo.DataSource = data;
            combo.ValueMember = valueMember;
            combo.DisplayMember = displayMember;
        }
        
    }
}
