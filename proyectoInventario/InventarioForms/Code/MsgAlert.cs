﻿using System;
using System.Windows.Forms;

namespace InventarioForms
{
    /// <summary>
    /// LLamado de las ventanas de MessageBox en metodos predefinidos 
    /// </summary>
    public static class MsgAlert
    {
        /// <summary>
        /// Warning
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="title"></param>
        /// <param name="msgButtons"></param>
        /// <returns></returns>
        public static DialogResult Warning(string msg,string title = "Advertencia", MessageBoxButtons msgButtons = MessageBoxButtons.OK)
        {
            return MessageBox.Show(msg, title, msgButtons, MessageBoxIcon.Warning);
        }

        /// <summary>
        /// Error
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="title"></param>
        /// <param name="msgButtons"></param>
        /// <returns></returns>
        public static DialogResult Error(string msg, string title = "Error", MessageBoxButtons msgButtons = MessageBoxButtons.OK)
        {
            return MessageBox.Show(msg, title, msgButtons, MessageBoxIcon.Error);
        }

        /// <summary>
        /// Error
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="title"></param>
        /// <param name="msgButtons"></param>
        /// <returns></returns>
        public static DialogResult Error(Exception ex, string title = "Error", MessageBoxButtons msgButtons = MessageBoxButtons.OK)
        {
            var sms = ex.Message + (ex.InnerException == null ? string.Empty : $"\n{ex.InnerException.Message}");
            return MessageBox.Show(sms, title, msgButtons, MessageBoxIcon.Error);
        }

        /// <summary>
        /// Question
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="title"></param>
        /// <param name="msgButtons"></param>
        /// <returns></returns>
        public static DialogResult Question(string msg, string title = "", MessageBoxButtons msgButtons = MessageBoxButtons.OKCancel)
        {
            return MessageBox.Show(msg, title, msgButtons, MessageBoxIcon.Question);
        }

        /// <summary>
        /// Information
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="title"></param>
        /// <param name="msgButtons"></param>
        /// <returns></returns>
        public static DialogResult Information(string msg, string title = "", MessageBoxButtons msgButtons = MessageBoxButtons.OK)
        {
            return MessageBox.Show(msg, title, msgButtons, MessageBoxIcon.Information);
        }


        /// <summary>
        /// Stop
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="title"></param>
        /// <param name="msgButtons"></param>
        /// <returns></returns>
        public static DialogResult Stop(string msg, string title = "", MessageBoxButtons msgButtons = MessageBoxButtons.OK)
        {
            return MessageBox.Show(msg, title, msgButtons, MessageBoxIcon.Stop);
        }

        /// <summary>
        /// Exclamation
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="title"></param>
        /// <param name="msgButtons"></param>
        /// <returns></returns>
        public static DialogResult Exclamation(string msg, string title = "", MessageBoxButtons msgButtons = MessageBoxButtons.OK)
        {
            return MessageBox.Show(msg, title, msgButtons, MessageBoxIcon.Exclamation);
        }

        /// <summary>
        /// Simple alert
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        public static DialogResult None(string msg, string title = "")
        {
            return MessageBox.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.None);
        }
    }
}
