﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinesLogic;
using Entity;
using System.Configuration;
using BussinesLogic.Security;

namespace InventarioForms
{
    public static class AppInit
    {
        private static bool AppInitData { get { return bool.Parse(ConfigurationManager.AppSettings["AppInitData"]); } }

        /// <summary>
        /// Insertar la data Inicial del aplicativo
        /// </summary>
        public static void GenerateDataInit()
        {
            if (!AppInitData) return;

            var clasisPers = new ClasficacionesRepository();
            var tipoMov = new TiposMovimientosRepository();
            var estados = new EstadosRepository();
            var user = new Users();

            clasisPers.InsertDataInit();
            tipoMov.InsertDataInit();
            estados.InsertDataInit();
            user.InsertDataInit();
            InsertMarcas();
        }

        /// <summary>
        /// Insertar motos
        /// </summary>
        private static void InsertMarcas()
        {
            var marcasRepo = new MarcasRepository();
            var marcas = new List<string>() { "YAMAHA", "DAYUN", "SUZUKI", "SERPENTO", "HONDA","PULSAR","RAYBAR" , "GENESIS" ,"HERO","KAWASAKI","BAJAJ"};
            marcas.ForEach(x => {

                var marca = marcasRepo.GetAll(where: $"marca = '{x}'");
                if (marca == null  || !marca.Any())
                    marcasRepo.Insert(new Marcas() { marca = x.ToUpper().Trim() });
            });
        }
    }
}
