﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventarioForms.Code
{
    public class MyConection
    {
        private string myProviderName = "SqlConexion";
        public string myServer { get; set; }
        public string myBaseDate { get; set; }
        public string myUser { get; set; }
        public string myPassword { get; set; }

        public MyConection(string myProviderName_ = "")
        {
            if (!string.IsNullOrEmpty(myProviderName_))
            {
                myProviderName = myProviderName_;
            }
            GetData();
        }

        /// <summary>
        /// Obtener la cadena de conexion
        /// </summary>
        /// <param name="providerName">Provider de conexion</param>
        /// <returns></returns>
        public string GetConexionString(string providerName = "")
        {
            providerName = (!string.IsNullOrEmpty(providerName) ? providerName : this.myProviderName);
            string cadena = ConfigurationManager.ConnectionStrings[providerName].ToString();
            return cadena;
        }


        /// <summary>
        /// Llenar propiedades de la clase segun datos de la cadena de conexion
        /// </summary>
        /// <param name="providerName"></param>
        public void GetData(string providerName = "")
        {
            providerName = (!string.IsNullOrEmpty(providerName) ? providerName : this.myProviderName);
            ConnectionStringSettingsCollection connections = ConfigurationManager.ConnectionStrings;

            //si existe por lo menos una conexión continuamos
            if (connections.Count != 0)
            {
                //Recorremos las conexiones existentes
                foreach (ConnectionStringSettings connection in connections)
                {
                    //asignamos el nombre
                    string name = connection.Name;
                    //obtenemos el proveedor, solo por demostración, no lo utilizaremos ni nada.
                    string provider = connection.ProviderName;
                    //obtenemos la cadena
                    string connectionString = connection.ConnectionString;

                    //comparamos el nombre al de nuestro atributo de la clase para verificar si es la cadena
                    //de conexión que modificaremos
                    if (name.Equals(providerName))
                    {

                        //separamos la conexión en un arreglo tomando ; como separador
                        string[] sC = connectionString.Split(';');
                        foreach (String s in sC)
                        {

                            //separamos por el simbolo = para obtener el campo y el valor
                            string[] spliter = s.Split('=');
                            switch (spliter[0].ToUpper())
                            {

                                case "DATA SOURCE":
                                    this.myServer = spliter[1];
                                    break;
                                case "USER ID":
                                    this.myUser = spliter[1];
                                    break;
                                case "PASSWORD":
                                    this.myPassword = spliter[1];
                                    break;
                                case "INITIAL CATALOG":
                                    this.myBaseDate = spliter[1];
                                    break;

                            }
                        }

                    }
                }
            }
        }
    }
}
