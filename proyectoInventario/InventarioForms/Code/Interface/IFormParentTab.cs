﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InventarioForms.Code.Interface
{
    public interface IFormParentTab
    {
        void fnShowAsChildrenTabPage(Form form, ref TabControl tab_Control);

        void fnCloseAsChildrenTabPage();
    }
}
