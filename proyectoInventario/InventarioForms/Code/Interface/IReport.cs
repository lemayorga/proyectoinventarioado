﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventarioForms.Code.Interface
{
    public interface IReport
    {
        string fullNameReport { get; }
        ReportParameter[] parametersArray { get; set; }
        ReportParameter[] SetParameters();
        ReportDataSourceCollection GetReport(ReportDataSourceCollection dataSourceCollection);
        void GetData();
        bool CountData();
        ReportParameter[] GetParameterUser();
    }
}
