﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventarioForms.Code.Interface
{
    public interface IFormAdd
    {
        /// <summary>
        /// Funcion paea guardar los datos
        /// </summary>
        void fnGuardar();
        /// <summary>
        /// Funcion para Validar los datos antes de guardar
        /// </summary>
        /// <returns></returns>
        bool fnValidar();
        /// <summary>
        /// Funcion para cargar los datos al momento de abrir el formulario
        /// </summary>
        void fnLoadData();
        /// <summary>
        /// Establecer valores para las variables de incio del formulario, abrir el formulario segun parametro
        /// </summary>
        /// <param name="formList"></param>
        /// <param name="key"></param>
        void fnSetInitVariablesShow(IFormListar formList, int key = 0, bool showForm = true);
    }
}
