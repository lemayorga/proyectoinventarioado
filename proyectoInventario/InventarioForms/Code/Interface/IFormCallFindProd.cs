﻿using BussinesLogic;
using BussinesLogic.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventarioForms.Code.Interface
{
    public  interface IFormCallFindProd
    {
        /// <summary>
        /// Variable que captura los registros seleccionados de productos
        /// </summary>
        List<ProductosSelect> productosSelect { get;  set;  }
        /// <summary>
        /// Metodo para recargar el grid
        /// </summary>
        void ReloadDataProd();
    }
}
