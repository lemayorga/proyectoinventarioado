﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventarioForms
{
    public interface IFormListar
    {
        /// <summary>
        /// Metodo para Listar los datos
        /// </summary>
        void fnListData();
        /// <summary>
        /// Metodo para abrir el formulario de nuevo registro
        /// </summary>
        void fnOpenNew();
        /// <summary>
        /// Metodo para abrir el formulario de edicion de registro
        /// </summary>
        /// <param name="key"></param>
        void fnOpenUpdate(object key);
    }
}
