﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using InventarioForms.formularios;

namespace InventarioForms
{
    public class App
    {
        Thread thrMasterForm, thrLogin;


        public void SetFormMaster(Form formulario)
        {
            try
            {
                formulario.Dispose();
                thrMasterForm = new Thread(() => SetForm(formulario, new Master(), thrMasterForm));
                thrMasterForm.Start();
            }
            catch (Exception ex)
            {
                MsgAlert.Error(ex);
            }
        }

        public void SetFormLogin(Form formulario)
        {
            try
            {
                formulario.Dispose();
                thrLogin = new Thread(() => SetForm(formulario, new frmLogin(), thrLogin));
                thrLogin.Start();
            }
            catch (Exception ex)
            {
                MsgAlert.Error(ex);
            }
        }

        private void SetForm(Form formFather, Form formSon, Thread hilo)
        {
            Application.Run(formSon);
            hilo.Abort();
        }

        /// <summary>
        /// brir formulario
        /// </summary>
        /// <param name="form"></param>
        public static Form OpenForm(Form form)
        {
            Form existe = Application.OpenForms.OfType<Form>().Where(pre => pre.Name == form.Name).SingleOrDefault<Form>();
            if (existe != null)
                existe.Dispose();

            form.Show();
            return form;
        }
    }
}
