﻿using InventarioForms.Code.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InventarioForms.Code
{
    public class FormParentTab : IFormParentTab
    {
        /// <summary>
        /// TabPage
        /// </summary>
        private TabPage tabPage;
        /// <summary>
        /// TabControl
        /// </summary>
        private TabControl tabControl;
        /// <summary>
        /// Formulario
        /// </summary>
        private Form form;

        /// <summary>
        /// Obtener el TabPage que contiene el formulario
        /// </summary>
        /// <returns></returns>
        public TabPage GetTabPage()
        {
            return tabPage;
        }

        /// <summary>
        ///  Obtener el TabControl que contiene el formulario
        /// </summary>
        /// <returns></returns>
        public TabControl GetTabControl()
        {
            return tabControl;
        }

        /// <summary>
        /// Presentar el formulario dentro del TabPage del TabControl
        /// </summary>
        /// <param name="form_"></param>
        /// <param name="tab_Control"></param>
        public void fnShowAsChildrenTabPage(Form form_, ref TabControl tab_Control)
        {
            Form existe = Application.OpenForms.OfType<Form>().Where(pre => pre.Name == form_.Name).SingleOrDefault<Form>();

            if (existe != null) return;

            form = form_;
            if (tabPage == null && tabControl == null)
            {
                tabControl = tab_Control;
                tabPage = new TabPage();
                tabPage.Text = form.Text;
                tabPage.Height = tabPage.Height * 2;
                form.TopLevel = false;
                form.Dock = DockStyle.Fill;
                form.FormBorderStyle = FormBorderStyle.None;
                tabPage.Controls.Add(form);
                tabControl.TabPages.Add(tabPage);
                tabControl.SelectedTab = tabPage;
                form.Show();
            }
        }

        /// <summary>
        /// Cerrar formulario y borrar el tabPage del TabControl
        /// </summary>
        public void fnCloseAsChildrenTabPage()
        {
            if(tabPage != null && tabControl!= null)  tabControl.TabPages.Remove(tabPage);
            if(form != null) form.Dispose();
        }
    }
}
