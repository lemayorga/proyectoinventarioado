﻿using BussinesLogic;
using Entity;
using InventarioForms.Code.Extensions;
using InventarioForms.Code.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InventarioForms.Code;

namespace InventarioForms.formularios
{
    public partial class frmAddProductos : Form, IFormAdd
    {
        #region Variables

        /// <summary>
        /// Valor de la llave primaria del formulario
        /// </summary>
        public int KeyForm = 0;
        /// <summary>
        /// Modelo a usar
        /// </summary>
        private Productos model;
        /// <summary>
        /// Repositorio del modelo
        /// </summary>
        private ProductosRepository repo;
        /// <summary>
        /// Formulario de recarga lista de datos
        /// </summary>
        public IFormListar formList;

        #endregion

        public frmAddProductos()
        {
            InitializeComponent();
            this.MaximumSize = new Size(this.Size.Width, this.Size.Height);
            this.Text = string.Empty;
            this.txtNombre.MaxLength  = 50;
            MyControls.UsuarioRecursos(this);
        }

        private void frmAddPersona_Load(object sender, EventArgs e)
        {
            repo = new ProductosRepository();
            this.lblKey.Visible = KeyForm == 0 ? false : true;
            this.lblKey.Text = $"Id: {KeyForm.ToString()}";
            fnLoadData();
        }

        /// <inheritdoc/>
        public void fnShow() { this.Show(); }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            fnGuardar();
        }

        public void fnSetInitVariablesShow(IFormListar formList, int key = 0, bool showForm = true)
        {
            this.KeyForm = key;
            this.formList = formList;
            if (showForm) App.OpenForm(this);
        }

        /// <inheritdoc/>
        public void fnLoadData()
        {
            try
            {
                this.txtNombre.OnlyUpper();
                this.txtModelo.OnlyUpper();

                //var marcas = new MarcasRepository().GetAll(orderBy: "marca");
                //this.cbMarca.Populate(marcas, "marcaId", "marca");

                if (KeyForm != 0)
                {
                    var dataModel = repo.GetById(this.KeyForm);
                    if (dataModel != null)
                    {
                        this.txtNombre.Text = dataModel.producto;
                        this.txtCosto.Text = dataModel.costo.ToString();
                        this.txtPrecio.Text = dataModel.precio.ToString();
                        //this.cbMarca.SelectedValue = dataModel.marcaId;
                        //this.txtModelo.Text = dataModel.modelo;
                    }
                }
            }
            catch (Exception ex)
            {
                 MsgAlert.Error(ex);
            }
        }

        /// <inheritdoc/>
        public void fnGuardar()
        {
            try
            {
                if (!fnValidar()) return;

                model = new Productos()
                {
                    productoId = this.KeyForm,
                    producto = this.txtNombre.Text.Trim(),
                    costo = double.Parse(this.txtCosto.Text.Trim()),
                    precio =  double.Parse(this.txtPrecio.Text.Trim()),
                    //marcaId = int.Parse(this.cbMarca.SelectedValue.ToString()),
                    //modelo = this.txtModelo.Text,
                };

                if (this.KeyForm == 0)
                    repo.Insert(model);
                else
                    repo.Update(model);

                formList.fnListData();
                this.Dispose();
             
            }
            catch (Exception ex)
            {
                 MsgAlert.Error(ex);
            }
        }

        /// <inheritdoc/>
        public bool fnValidar()
        {
            var msg = string.Empty;
            msg += string.IsNullOrEmpty(this.txtNombre.Text) ? "*Campo nombre vacio \n" : string.Empty;

            msg += string.IsNullOrEmpty(this.txtCosto.Text) ?  "*Campo costo vacio \n"
                 :double.Parse(this.txtCosto.Text) <= 0 ?  "*Campo costo no puede ser valor cero \n" : string.Empty;

            msg += string.IsNullOrEmpty(this.txtPrecio.Text) ? "*Campo precio vacio \n"
                 : double.Parse(this.txtPrecio.Text) <= 0 ? "*Campo precio no puede ser valor cero \n" : string.Empty;

            //msg +=  this.cbMarca.SelectedValue == null ? "*Seleccione una marca\n"  : string.Empty;
            //msg += string.IsNullOrEmpty(this.txtModelo.Text) ? "*Campo modelo vacio \n" : string.Empty;

            if (!string.IsNullOrEmpty(msg))
            {
                MsgAlert.Error(msg);
                return false;
            }
            return true;
        }

        private void txtCosto_KeyPress(object sender, KeyPressEventArgs e)
        {
            (sender as TextBox).KeyPressDecimal(e);
        }

        private void txtCosto_KeyDown(object sender, KeyEventArgs e)
        {
            (sender as TextBox).KeyDownDecimal(e);
        }

        private void txtPrecio_KeyPress(object sender, KeyPressEventArgs e)
        {
            (sender as TextBox).KeyPressDecimal(e);
        }

        private void txtPrecio_KeyDown(object sender, KeyEventArgs e)
        {
            (sender as TextBox).KeyDownDecimal(e);
        }
    }
}
