﻿using BussinesLogic;
using BussinesLogic.Security;
using Entity;
using InventarioForms.Code.Extensions;
using InventarioForms.Code.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InventarioForms.Code;
using Entity.Security;

namespace InventarioForms.formularios
{
    public partial class frmAddUsuario : Form, IFormAdd
    {
        #region Variables

        /// <summary>
        /// Valor de la llave primaria del formulario
        /// </summary>
        public int KeyForm = 0;
        /// <summary>
        /// Modelo a usar
        /// </summary>
        private AppUsers model;
        /// <summary>
        /// Repositorio del modelo
        /// </summary>
        private Users repo;
        /// <summary>
        /// Formulario de recarga lista de datos
        /// </summary>
        public IFormListar formList;

        #endregion

        public frmAddUsuario()
        {
            InitializeComponent();
            this.MaximumSize = new Size(this.Size.Width, this.Size.Height);
            this.Text = string.Empty;
            this.txtNombre.MaxLength = this.txtApellido.MaxLength = 50;
            this.txtUsreName.MaxLength = 10;
            MyControls.UsuarioRecursos(this);
        }

        private void frmAddPersona_Load(object sender, EventArgs e)
        {
            repo = new Users();
            this.lblKey.Visible = KeyForm == 0 ? false : true;
            this.lblKey.Text = $"Id: {KeyForm.ToString()}";
            fnLoadData();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            fnGuardar();
        }

        /// <inheritdoc/>
        public void fnSetInitVariablesShow(IFormListar formList, int key = 0, bool showForm = true)
        {
            this.KeyForm = key;
            this.formList = formList;
            if (showForm) App.OpenForm(this);
        }

        /// <inheritdoc/>
        public void fnLoadData()
        {
            try
            {
                this.txtNombre.OnlyUpper();
                this.txtApellido.OnlyUpper();
                this.cbRol.Populate(new Roles().Get_DT(), "appRolId", "appRolName");

                if (KeyForm != 0)
                {
                    var dataModel = repo.GetById(this.KeyForm);
                    if (dataModel != null)
                    {
                        this.txtNombre.Text = dataModel.firstName;
                        this.txtApellido.Text = dataModel.lastName;
                        this.txtUsreName.Text = dataModel.appUserName;
                        this.txtPass1.Text = dataModel.password;
                        this.txtPass2.Text = dataModel.password;
                        this.ckBloqueo.Checked = dataModel.bloqueado;

                    }
                   var itemRol = repo.GetFirtItemRol(dataModel.appUserId);
                   if (itemRol != null) this.cbRol.SelectedValue = itemRol.appRolId.ToString();

                    this.txtUsreName.Enabled = false;
                    this.ckBloqueo.Visible = true;
                }
            }
            catch (Exception ex)
            {
                 MsgAlert.Error(ex);
            }
        }

        /// <inheritdoc/>
        public void fnGuardar()
        {
            try
            {
                if (!fnValidar()) return;

                model = new AppUsers()
                {
                    appUserId = this.KeyForm,
                    firstName = this.txtNombre.Text.Trim(),
                    lastName = this.txtApellido.Text.Trim(),
                    appUserName = this.txtUsreName.Text.Trim(),
                    password = this.txtPass1.Text.Trim(),
                    bloqueado = this.ckBloqueo.Checked
                };


                repo.SaveUser(model,int.Parse(this.cbRol.SelectedValue.ToString()));

                MsgAlert.Information("Usuario guardado exitosamente!!");
                formList.fnListData();
                this.Dispose();
             
            }
            catch (Exception ex)
            {
                 MsgAlert.Error(ex);
            }
        }

        /// <inheritdoc/>
        public bool fnValidar()
        {
            var msg = string.Empty;
            msg += string.IsNullOrEmpty(this.txtUsreName.Text) ? "*Campo usuario vacio \n" : string.Empty;
            msg += this.cbRol.SelectedValue == null ? "*Seleccione un rol \n" : string.Empty;
            msg += string.IsNullOrEmpty(this.txtNombre.Text) ? "*Campo nombre vacio \n" : string.Empty;
            msg += string.IsNullOrEmpty(this.txtApellido.Text) ? "*Campo apellido vacio \n" : string.Empty;

            msg += string.IsNullOrEmpty(this.txtPass1.Text) ? "*Campo contraseña vacio \n" : string.Empty;
            msg += string.IsNullOrEmpty(this.txtPass2.Text) ? "*Campo confirmación contraseña vacio \n" : string.Empty;


            if (this.KeyForm == 0)
            {
                var existeNombre = repo.ExisteUserName(this.txtUsreName.Text);
                msg += existeNombre ? "*Nombre de usuario ya existe, intente otro nombre \n" : string.Empty;
            }

            if (!string.IsNullOrEmpty(msg))
            {
                MsgAlert.Error(msg);
                return false;
            }
            
            if (this.txtPass1.Text.Trim().Length < Users.PASS_LENGTH_MIN)
            {
                MsgAlert.Error($"Contraseña de ser minimo {Users.PASS_LENGTH_MIN} caracteres");
                return false;
            }

            if (this.txtPass1.Text.Trim() != this.txtPass2.Text.Trim())
            {
                MsgAlert.Error("Las contrasñas escritas son diferentes, intente nuevamente");
                return false;
            }
          

            bool caracterInval = false;
            repo.caratersInvalidos.ToList().ForEach(x => 
            {

                if (this.txtPass1.Text.Contains(x))
                {
                    caracterInval = true;
                }
            });
            if (caracterInval)
            {
                MsgAlert.Error($"No se puede usar los caracteres: {string.Join(",", repo.caratersInvalidos)} \n como caracter de contraseña");
                return false;
            }
            return true;
        }
    }
}
