﻿using BussinesLogic;
using Entity;
using InventarioForms.Code.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InventarioForms.Code.Extensions;
using InventarioForms.Code;
using BussinesLogic.Interface;
using static BussinesLogic.TiposMovimientosRepository;
using static BussinesLogic.EstadosRepository;

namespace InventarioForms.formularios
{
    public partial class frmAddCompra : Form, IFormAdd, IFormCallFindProd
    {
        #region Variables

        /// <summary>
        /// Valor de la llave primaria del formulario
        /// </summary>
        public int KeyForm = 0;
        /// <summary>
        /// Modelo Movimiento Maestro a usar
        /// </summary>
        private Movimientos movimiento = null;
        /// <summary>
        /// Repositorio de personas
        /// </summary>
        private PersonasRepository personasRepo;
        /// <summary>
        /// Repositorio de productos
        /// </summary>
        private ProductosRepository prodRepo;
        /// <summary>
        /// Repositorio del modelo
        /// </summary>
        private MovimientosRepository movRepo;
        /// <summary>
        /// Tipos de movimientos repositorio
        /// </summary>
        private TiposMovimientosRepository tipoMovRepo;
        /// <summary>
        /// Formulario de recarga lista de datos
        /// </summary>
        public IFormListar formList;
        /// <summary>
        /// Saber el tipo de Movimiento
        /// </summary>
        private DataTiposMov tipoMov = DataTiposMov.compras;
        /// <summary>
        /// Variable que captura los registros seleccionados de productos
        /// </summary>
        private List<ProductosSelect> productosSelect_;
        /// <inheritdoc/>
        public List<ProductosSelect> productosSelect
        {
            get
            {
                return productosSelect_ ?? new List<ProductosSelect>();
            }

            set
            {
                productosSelect_ = value;
            }
        }

        #endregion

        public frmAddCompra()
        {
            InitializeComponent();
            this.MaximumSize = new Size(this.Size.Width, this.Size.Height);
            this.Text = string.Empty;
            this.txtReferencia.MaxLength  = 50;
            MyControls.UsuarioRecursos(this);
        }

        private void frmAddPersona_Load(object sender, EventArgs e)
        {
            movRepo = new MovimientosRepository();
            personasRepo = new PersonasRepository();
            prodRepo = new ProductosRepository();
            tipoMovRepo = new TiposMovimientosRepository();

            this.lblKey.Visible = KeyForm == 0 ? false : true;
            this.lblKey.Text = $"Id registro: {KeyForm.ToString()}";
            fnLoadData();
        }

        /// <inheritdoc/>
        public void fnShow() { this.Show(); }


        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            fnGuardar();
        }

        public void fnSetInitVariablesShow(IFormListar formList, int key = 0, bool showForm = true)
        {
            this.KeyForm = key;
            this.formList = formList;
            if (showForm) App.OpenForm(this);
        }

        /// <inheritdoc/>
        public void fnLoadData()
        {
            try
            {
                this.txtImp.Text = Constantes.IMPUESTO.ToString("#,##0.00");
                this.txtReferencia.OnlyUpper();

                var proveedores = new PersonasRepository().GetAllClasificados(ClasficacionesRepository.DataClasif.proveedores).Select(x => new { x.personaId, nombre = $"{x.nombres} {x.apellidos}" });
                this.cbProveedor.Populate(proveedores, "personaId", "nombre");


                if (KeyForm != 0)
                {
                    var dataModel = movRepo.GetById(this.KeyForm);
                    if (dataModel != null)
                    {
                        this.cbProveedor.SelectedValue = int.Parse(dataModel.personaId.ToString());
                        this.txtReferencia.Text = dataModel.numero.ToString();
                        this.dpFecha.Value = dataModel.fecha;
                        this.txtObserv.Text = dataModel.observaciones;
                        this.txtSubtotal.Text = dataModel.subtotal.ToString("#,##0.00");
                        this.txtDescuento.Text = dataModel.descuento.ToString("#,##0.00");
                        this.txtImp.Text = dataModel.impuesto.ToString("#,##0.00");
                        this.txtTotal.Text = dataModel.total.ToString("#,##0.00");
                    }

                    productosSelect =   movRepo.GetDetalleProductosSelect(this.KeyForm);

                    this.cbProveedor.Enabled = false;
                    this.txtReferencia.Enabled = false;
                    this.dpFecha.Enabled = false;
                    this.txtObserv.Enabled = false;
                    this.btnSelecccionar.Enabled = false;
                    this.btnGuardar.Enabled = false;
                    this.txtDescuento.ReadOnly = true;
                    this.txtImp.ReadOnly = true;
                }
             
                ReloadDataProd();
                this.grMovDet.FormaStyle();
             
            }
            catch (Exception ex)
            {
                 MsgAlert.Error(ex);
            }
        }

        /// <inheritdoc/>
        public void ReloadDataProd()
        {
            this.grMovDet.DataSource = new BindingList<ProductosSelect>(this.productosSelect);
            this.grMovDet.FormaStyle();         
            GenerarMontos();
            StyleGrid();
        }

        /// <inheritdoc/>
        public void fnGuardar()
        {
            try
            {
                if (!fnValidar()) return;

                movimiento = new Movimientos()
                {
                    movimientoId = this.KeyForm,
                    numero = this.txtReferencia.Text.Trim(),
                    fecha = this.dpFecha.Value,
                    personaId = int.Parse(this.cbProveedor.SelectedValue.ToString()),
                    observaciones = this.txtObserv.Text,
                    subtotal = double.Parse(this.txtSubtotal.Text),
                    descuento = double.Parse(this.txtDescuento.Text),
                    impuesto = double.Parse(this.txtImp.Text),
                    total = double.Parse(this.txtTotal.Text),
                    tipoId = tipoMovRepo.GetIdDataEnum(tipoMov)
                };

                if (this.KeyForm == 0)
                {
                    var movDetelle = productosSelect.Select(x => new MovimientoDet
                    {
                        movDetId = x.movDetId,
                        productoId = x.productoId,
                        cantidad = x.cantidad,
                        valor = x.costo,
                    }).ToList();
                   
                    movRepo.Insert(movimiento);
                    movRepo.AddSaveDetalle(movimiento.movimientoId, movDetelle);
                    movRepo.InsertEstadoMov(movimiento.movimientoId, DataEstados.activo, Constantes.USER_NAME, DateTime.Now);
                }
                  

                formList.fnListData();
                this.Dispose();
             
            }
            catch (Exception ex)
            {
                 MsgAlert.Error(ex);
            }
        }

        /// <inheritdoc/>
        public bool fnValidar()
        {
            var msg = string.Empty;
            msg += string.IsNullOrEmpty(this.txtReferencia.Text) ? "*Campo referencia vacio \n" : string.Empty;
            msg += string.IsNullOrEmpty(this.cbProveedor.Text) ? "*Seleccione un proveedor vacio \n" : string.Empty;
            msg += this.dpFecha.Value.Date > DateTime.Now.Date ? "*La fecha no pueder ser mayor al dia de hoy \n" : string.Empty;

            msg += string.IsNullOrEmpty(this.txtTotal.Text) ?  "*Campo total vacio \n"
                 :double.Parse(this.txtTotal.Text) <= 0 ? "*Campo total no puede ser valor cero , ingrese un producto \n" : string.Empty;
            msg += !productosSelect.Any() ? "*No tiene productos seleccionados \n" : string.Empty;

            if (!string.IsNullOrEmpty(msg))
            {
                MsgAlert.Error(msg);
                return false;
            }
            return true;
        }

        private void StyleGrid()
        {
            this.grMovDet.Columns["costo"].DefaultCellStyle.Format = $"N{Constantes.CANT_DECIMALES}";
            this.grMovDet.Columns["precio"].DefaultCellStyle.Format = $"N{Constantes.CANT_DECIMALES}";
            this.grMovDet.Columns["cantidad"].DefaultCellStyle.Format = $"N{Constantes.CANT_SIN_DECIMALES}";
            this.grMovDet.Columns["productoId"].HeaderText = "ID";
            this.grMovDet.Columns["producto"].Width = 160;
            this.grMovDet.Columns["marca"].Width = 150;
            this.grMovDet.Columns["modelo"].Width = 130;
            this.grMovDet.Columns["existencia"].Visible = false;
            this.grMovDet.Columns["precio"].Visible = false;
            if (this.KeyForm != 0)
            {
                this.grMovDet.Columns["borrar"].Visible = false;
            }

            this.grMovDet.Columns["marca"].Visible = false;
            this.grMovDet.Columns["modelo"].Visible = false;
        }

        /// <summary>
        /// Generar los montos totales
        /// </summary>
        private void GenerarMontos()
        {
            var txtDescuento = string.IsNullOrEmpty(this.txtDescuento.Text) || this.txtDescuento.Text == "." ? "0"
                               : this.txtDescuento.Text;
            var txtImp = string.IsNullOrEmpty(this.txtImp.Text) || this.txtImp.Text == "." ? "0"
                   : this.txtImp.Text;

            var subtotal = this.productosSelect.Sum(t => t.cantidad * t.costo);
            var decuento = (subtotal * (double.Parse(txtDescuento) / 100));
            var impuesto = (subtotal * (double.Parse(txtImp) / 100));

            this.txtSubtotal.Text = subtotal.ToString("#,##0.00"); 
            this.txtTotal.Text = ((subtotal - decuento) + impuesto).ToString("#,##0.00");
        }

        private void txtDescuento_KeyDown(object sender, KeyEventArgs e)
        {
            (sender as TextBox).KeyDownDecimal(e);
        }

        private void txtDescuento_KeyPress(object sender, KeyPressEventArgs e)
        {
            (sender as TextBox).KeyPressDecimal(e);
        }

        private void txtImp_KeyDown(object sender, KeyEventArgs e)
        {
            (sender as TextBox).KeyDownDecimal(e);
        }

        private void txtImp_KeyPress(object sender, KeyPressEventArgs e)
        {
            (sender as TextBox).KeyPressDecimal(e);
        }

        private void txtDescuento_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty((sender as TextBox).Text))
            {
                var valor =  (sender as TextBox).Text == "." ? "0"
                            : (sender as TextBox).Text;
                if (double.Parse(valor) > 100)
                {
                    MsgAlert.Warning($"Descuento no puede ser mayor del {Constantes.MAYOR_DESCUENTO} %");
                    (sender as TextBox).Text = string.Format("{0:#,##0.00}","0");
                }
            }
            GenerarMontos();
        }

        private void txtImp_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty((sender as TextBox).Text))
            {
                var valor = (sender as TextBox).Text == "." ? "0"
                          : (sender as TextBox).Text;
                if (double.Parse(valor) > Constantes.IMPUESTO)
                {
                    MsgAlert.Warning($"Descuento no puede ser mayor del {Constantes.IMPUESTO} %");
                    (sender as TextBox).Text = Constantes.IMPUESTO.ToString("#,##0.00");
                }
            }

            GenerarMontos();
        }

        private void txtDescuento_KeyUp(object sender, KeyEventArgs e)
        {
            var textbox = (sender as TextBox);
            if (string.IsNullOrEmpty(textbox.Text))
            {
                textbox.Text = "0.00";
            }
        }

        private void txtImp_KeyUp(object sender, KeyEventArgs e)
        {
            var textbox = (sender as TextBox);
            if (string.IsNullOrEmpty(textbox.Text))
            {
                textbox.Text = Constantes.IMPUESTO.ToString("#,##0.00");
            }
        }

        private void btnSelecccionar_Click(object sender, EventArgs e)
        {
            var frmFindProd = new frmListFindProductos(tipoMov, this);
            App.OpenForm(frmFindProd);
        }

        private void grMovDet_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (this.KeyForm != 0) return;

                var row = this.grMovDet.CurrentRow;
                if (row.Cells["borrar"].ColumnIndex == e.ColumnIndex)
                {
                    var cellProd = row.Cells["productoId"];
                    var cellmMvDetId = row.Cells["movDetId"];

                    if (cellProd.Value != null)
                    {
                        var prod = productosSelect.FirstOrDefault(i => i.productoId == int.Parse(cellProd.Value.ToString()));
                        if (cellmMvDetId != null)
                        {

                        }
                        if (prod != null) productosSelect.Remove(prod);
                    }
                    ReloadDataProd();
                }
            }
            catch (Exception ex)
            {
                 MsgAlert.Error(ex);
            }
        }
    }
}
