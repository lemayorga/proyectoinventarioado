﻿using BussinesLogic.Security;
using Entity.Security;
using InventarioForms.Code;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static BussinesLogic.Security.Users;

namespace InventarioForms.formularios
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
            this.MaximumSize = new Size(this.Size.Width, this.Size.Height);
            this.lblAppName.Text = Constantes.APP_NAME;
        }

        /// <summary>
        /// Obtener las teclas presionadas , para llamar al iniciar session
        /// </summary>
        /// <param name="e"></param>
        private void KeyPress(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) IniciarSesion();
        }

        /// <summary>
        /// Metodo de validacion de usuario y contrase;a
        /// </summary>
        private void IniciarSesion()
        {
            try
            {
                MgsLogin resultado;
                string msg = string.Empty;

                AppUsers login = new Users().IniciarSesion(this.txtUser.Text, this.txtPass.Text, out resultado);
                switch (resultado)
                {
                    case MgsLogin.usuarioNoExiste:
                        msg = "Usuario y/o contraseña incorreccto(s)";
                        break;
                    case MgsLogin.usuarioBloqueado:
                        msg = "Usuario se encuentra bloquedo";
                        break;
                    case MgsLogin.exitoso:
                        Constantes.USUARIO_ACTUAL = login;
                        Constantes.USER_NAME = login.appUserName;
                        Constantes.ROL_ACTUAL  = new Roles().GetFirstRolUser(login);
                        Constantes.ROLES = (Constantes.ROL_ACTUAL == null ? string.Empty : Constantes.ROL_ACTUAL.appRolName);

                        var conexion = new MyConection();
                        conexion.GetData();
                        Constantes.MY_SERVIDOR = conexion.myServer;
                        Constantes.MY_BASE_DATOS = conexion.myBaseDate;

                        break;
                }
                if (!string.IsNullOrEmpty(msg))
                {
                    MsgAlert.Error(msg);
                    return;
                }

                Constantes.APP.SetFormMaster(this);
            }
            catch (Exception ex)
            {
                MsgAlert.Error(ex);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            IniciarSesion();
        }

        private void txtUser_KeyDown(object sender, KeyEventArgs e)
        {
            KeyPress(e);
        }

        private void txtPass_KeyDown(object sender, KeyEventArgs e)
        {
            KeyPress(e);
        }
    }
}
