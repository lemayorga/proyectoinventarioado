﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InventarioForms;
using BussinesLogic;
using Entity;
using InventarioForms.Code;
using InventarioForms.Code.Interface;
using InventarioForms.Code.Extensions;
using BussinesLogic.Security;

namespace InventarioForms.formularios
{
    public partial class frmListUsuarios : Form , IFormListar
    {
        #region Variables
        /// <summary>
        /// Repositorio a utilizar
        /// </summary>
        private Users repoPrincipal;
        /// <summary>
        /// Formulario de Add,Update
        /// </summary>
        private IFormAdd frmAdd;
        /// <summary>
        /// Formulario tab contenedor
        /// </summary>       
        private FormParentTab formParentTab;

        #endregion

        public frmListUsuarios()
        {
            InitializeComponent();
            repoPrincipal = new Users();
            formParentTab = new FormParentTab();
            MyControls.UsuarioRecursos(this);
        }

        /// <inheritdoc/>
        public void fnListData()
        {
            try
            {
                var data = repoPrincipal.Get_DT();
                this.gridViewData.DataSource = new BindingSource(data, null);  
                this.gridViewData.FormaStyle();

                this.gridViewData.Columns["appUserId"].HeaderText = "ID";
                this.gridViewData.Columns["appUserName"].HeaderText = "Usuario";
                this.gridViewData.Columns["firstName"].HeaderText = "Nombre(s)";
                this.gridViewData.Columns["lastName"].HeaderText = "Apellido(s)";
                this.gridViewData.Columns["bloqueado"].HeaderText = "Bloqueado";
                this.gridViewData.Columns["appUserName"].Width = 130;
                this.gridViewData.Columns["firstName"].Width = 130;
                this.gridViewData.Columns["lastName"].Width = 180;
                this.gridViewData.Columns["bloqueado"].Width = 90; 
                this.gridViewData.Columns["password"].Visible = false;
            }
            catch (Exception ex)
            {
                 MsgAlert.Error(ex);
            }
        }

        public void fnShowChildren(ref TabControl tab_Control)
        {
            formParentTab.fnShowAsChildrenTabPage(this, ref tab_Control);
        }

        /// <inheritdoc/>
        private void fnClose()
        {
            formParentTab.fnCloseAsChildrenTabPage();
        }

        /// <inheritdoc/>
        public void fnOpenUpdate(object key)
        {
            frmAdd = new frmAddUsuario();
            frmAdd.fnSetInitVariablesShow(this, int.Parse(key.ToString()));
        }

        /// <inheritdoc/>
        public void fnOpenNew()
        {
            frmAdd = new frmAddUsuario();
            frmAdd.fnSetInitVariablesShow(this);
        }

        private void DeleteItemGrid(object key, DataGridViewCellCollection columns)
        {
            try
            {
              if(MsgAlert.Question($"Registro con Id:{key.ToString()} \n\nUsuario: *{columns["appUserName"].Value}*\n¿Seguro desea borrar el registro?", msgButtons:  MessageBoxButtons.YesNo) == DialogResult.Yes)
              {
                    //if (repoPrincipal.TieneMovimientos(int.Parse(key.ToString())))
                    //{
                    //    MsgAlert.Information($"Registro ** {columns["nombres"].Value} {columns["apellidos"].Value}** cuenta con movimientos. \n\n No se puede realizar la eliminación.");
                    //    return;
                    //}
                    //if (repoPrincipal.Delete(int.Parse(key.ToString()), ClasficacionesRepository.DataClasif.clientes))
                    //{
                    //    this.fnListData();
                    //    MsgAlert.Information("Registro borrado sastifactoriamente");
                    //}
              }
            }
            catch (Exception ex)
            {
                 MsgAlert.Error(ex);
            }
        } 

        private void frmListData_Load(object sender, EventArgs e)
        {
            fnListData();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            fnClose();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            fnOpenNew();
        }

        private void gridViewData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if(this.gridViewData.CurrentRow == null)
            {
                MsgAlert.Information("Seleccione un registro del listado");
                return;
            }

            var cell = this.gridViewData.CurrentRow.Cells[repoPrincipal.GetKeyName()];
            if (cell != null) DeleteItemGrid(cell.Value, this.gridViewData.CurrentRow.Cells);
            else MsgAlert.Information("Seleccione un registro del listado");
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            gridViewData.FilterOneValue((sender as TextBox).Text, new string[] { "appUserName", "firstName ", "lastName"});
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            var cell = this.gridViewData.CurrentRow.Cells[repoPrincipal.GetKeyName()];
            if (cell != null) fnOpenUpdate(cell.Value);
        }
    }
}
    