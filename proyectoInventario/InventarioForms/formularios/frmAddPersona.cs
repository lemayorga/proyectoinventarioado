﻿using BussinesLogic;
using Entity;
using InventarioForms.Code.Extensions;
using InventarioForms.Code.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InventarioForms.Code;
namespace InventarioForms.formularios
{
    public partial class frmAddPersona : Form, IFormAdd
    {
        #region Variables

        /// <summary>
        /// Valor de la llave primaria del formulario
        /// </summary>
        public int KeyForm = 0;
        /// <summary>
        /// Modelo a usar
        /// </summary>
        private Personas model;
        /// <summary>
        /// Repositorio del modelo
        /// </summary>
        private PersonasRepository repo;
        /// <summary>
        /// Formulario de recarga lista de datos
        /// </summary>
        public IFormListar formList;

        #endregion

        public frmAddPersona()
        {
            InitializeComponent();
            this.MaximumSize = new Size(this.Size.Width, this.Size.Height);
            this.Text = string.Empty;
            this.txtNombre.MaxLength = this.txtApellido.MaxLength = 50;
            this.txtTelef.MaxLength = 100;
            this.txtDni.MaxLength = 10;
            this.txtEmail.MaxLength = 100;
            MyControls.UsuarioRecursos(this);
        }

        private void frmAddPersona_Load(object sender, EventArgs e)
        {
            repo = new PersonasRepository();
            this.lblKey.Visible = KeyForm == 0 ? false : true;
            this.lblKey.Text = $"Id: {KeyForm.ToString()}";
            fnLoadData();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            fnGuardar();
        }

        /// <inheritdoc/>
        public void fnSetInitVariablesShow(IFormListar formList, int key = 0, bool showForm = true)
        {
            this.KeyForm = key;
            this.formList = formList;
            if (showForm) App.OpenForm(this);
        }

        /// <inheritdoc/>
        public void fnLoadData()
        {
            try
            {
                this.txtNombre.OnlyUpper();
                this.txtApellido.OnlyUpper();

                if (KeyForm != 0)
                {
                    var dataModel = repo.GetById(this.KeyForm);
                    if (dataModel != null)
                    {
                        this.txtNombre.Text = dataModel.nombres;
                        this.txtApellido.Text = dataModel.apellidos;
                        this.txtDni.Text = dataModel.ndi;
                        this.txtTelef.Text = dataModel.telefonos;
                        this.txtDomicilio.Text = dataModel.domicilio;
                        this.txtEmail.Text = dataModel.correos;
                    }
                }
            }
            catch (Exception ex)
            {
                 MsgAlert.Error(ex);
            }
        }

        /// <inheritdoc/>
        public void fnGuardar()
        {
            try
            {
                if (!fnValidar()) return;

                model = new Personas()
                {
                    personaId = this.KeyForm,
                    nombres = this.txtNombre.Text.Trim(),
                    apellidos = this.txtApellido.Text.Trim(),
                    ndi = this.txtDni.Text.Trim(),
                    domicilio = this.txtDomicilio.Text.Trim(),
                    correos = this.txtEmail.Text.Trim(),
                    telefonos = this.txtTelef.Text.Trim(),
                };

                if (this.KeyForm != 0)
                    repo.Update(model);
                else
                {
                    repo.Insert(model);
                    repo.AddClasificacion(model, ClasficacionesRepository.DataClasif.clientes);
                }
                  
                formList.fnListData();
                this.Dispose();
             
            }
            catch (Exception ex)
            {
                 MsgAlert.Error(ex);
            }
        }

        /// <inheritdoc/>
        public bool fnValidar()
        {
            var msg = string.Empty;
            msg += string.IsNullOrEmpty(this.txtNombre.Text) ? "*Campo nombre vacio \n" : string.Empty;
            msg += string.IsNullOrEmpty(this.txtApellido.Text) ? "*Campo apellido vacio \n" : string.Empty;

            if (!string.IsNullOrEmpty(msg))
            {
                MsgAlert.Error(msg);
                return false;
            }

            return true;
        }
    }
}
