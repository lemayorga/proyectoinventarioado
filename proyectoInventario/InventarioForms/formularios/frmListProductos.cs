﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InventarioForms;
using BussinesLogic;
using Entity;
using InventarioForms.Code;
using InventarioForms.Code.Interface;
using InventarioForms.Code.Extensions;

//http://viblend.com/blog/?tag=/datagridview
namespace InventarioForms.formularios
{
    public partial class frmListProductos : Form , IFormListar
    {
        #region Variables
        /// <summary>
        /// Repositorio a utilizar
        /// </summary>
        private ProductosRepository repoPrincipal;
        /// <summary>
        /// Formulario de Add,Update
        /// </summary>
        private IFormAdd frmAdd;
        /// <summary>
        /// Formulario tab contenedor
        /// </summary>  
        private FormParentTab formParentTab;

        #endregion

        public frmListProductos()
        {
            InitializeComponent();
            repoPrincipal = new ProductosRepository();
            formParentTab = new FormParentTab();
            MyControls.UsuarioRecursos(this);
        }

        /// <inheritdoc/>
        public void fnListData()
        {
            try
            {
                var data = repoPrincipal.GetAll_DT();
                this.gridViewData.DataSource = new BindingSource(data, null);
                this.gridViewData.FormaStyle();

                this.gridViewData.Columns["costo"].DefaultCellStyle.Format = $"N{Constantes.CANT_DECIMALES}";
                this.gridViewData.Columns["precio"].DefaultCellStyle.Format = $"N{Constantes.CANT_DECIMALES}";
                this.gridViewData.Columns["productoId"].HeaderText = "ID";
                this.gridViewData.Columns["marcaId"].Visible = false;
                this.gridViewData.Columns["marca"].Visible = false;
                this.gridViewData.Columns["modelo"].Visible = false;
                this.gridViewData.Columns["producto"].Width = 160;
                this.gridViewData.Columns["marca"].Width = 150;
                this.gridViewData.Columns["modelo"].Width = 130;
            }
            catch (Exception ex)
            {
                 MsgAlert.Error(ex);
            }
        }

        /// <inheritdoc/>
        public void fnShowChildren(ref TabControl tab_Control)
        {
            formParentTab.fnShowAsChildrenTabPage(this, ref tab_Control);
        }

        /// <inheritdoc/>
        private void fnClose()
        {
            formParentTab.fnCloseAsChildrenTabPage();
        }

        /// <inheritdoc/>
        public void fnOpenUpdate(object key)
        {
            frmAdd = new frmAddProductos();
            frmAdd.fnSetInitVariablesShow(this, int.Parse(key.ToString()));
        }

        /// <inheritdoc/>
        public void fnOpenNew()
        {
            frmAdd = new frmAddProductos();
            frmAdd.fnSetInitVariablesShow(this);
        }

        private void DeleteItemGrid(object key, DataGridViewCellCollection columns)
        {
            try
            {
              if(MsgAlert.Question($"Registro con Id:{key.ToString()} \n\n** {columns["producto"].Value} **\n¿Seguro desea borrar el registro?", msgButtons:  MessageBoxButtons.YesNo) == DialogResult.Yes)
              {
                    if (repoPrincipal.TieneMovimientos(int.Parse(key.ToString())))
                    {
                        MsgAlert.Information($"Registro ** {columns["producto"].Value}** cuenta con movimientos. \n\n No se puede realizar la eliminación.");
                        return;
                    }

                    if (repoPrincipal.Delete(int.Parse(key.ToString())))
                    {
                        this.fnListData();
                        MsgAlert.Information("Registro borrado sastifactoriamente");
                    }
              }
            }
            catch (Exception ex)
            {
                 MsgAlert.Error(ex);
            }
        } 

        private void frmListData_Load(object sender, EventArgs e)
        {
            fnListData();
        }


        private void btnCancelar_Click(object sender, EventArgs e)
        {
            fnClose();
        }


        private void btnNuevo_Click(object sender, EventArgs e)
        {
            fnOpenNew();
        }

        private void gridViewData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.gridViewData.CurrentRow == null)
            {
                MsgAlert.Information("Seleccione un registro del listado");
                return;
            }

            var cell = this.gridViewData.CurrentRow.Cells[repoPrincipal.GetKeyName()];
            if (cell != null) DeleteItemGrid(cell.Value, this.gridViewData.CurrentRow.Cells);
            else MsgAlert.Information("Seleccione un registro del listado");
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            gridViewData.FilterOneValue((sender as TextBox).Text, new string[] { "producto", "modelo", "marca" });
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            var cell = this.gridViewData.CurrentRow.Cells[repoPrincipal.GetKeyName()];
            if (cell != null) fnOpenUpdate(cell.Value);
        }
    }
}
    