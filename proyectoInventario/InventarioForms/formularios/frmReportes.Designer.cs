﻿namespace InventarioForms.formularios
{
    partial class frmReportes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rbRptMovDet = new System.Windows.Forms.RadioButton();
            this.rbRptMov = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rbRptTipoMovAll = new System.Windows.Forms.RadioButton();
            this.rbRptMovCompra = new System.Windows.Forms.RadioButton();
            this.rbRptMovVenta = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dpFecha1 = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dpFecha2 = new System.Windows.Forms.DateTimePicker();
            this.txtNombre = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblTitleForm = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGenerar = new System.Windows.Forms.Button();
            this.btnRptExistencias = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtNombre1 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.txtNombre.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel3);
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dpFecha1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dpFecha2);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 66);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(729, 217);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtro del Reporte de Movimientos";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Controls.Add(this.rbRptMovDet);
            this.panel3.Controls.Add(this.rbRptMov);
            this.panel3.Location = new System.Drawing.Point(208, 23);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(412, 49);
            this.panel3.TabIndex = 9;
            // 
            // rbRptMovDet
            // 
            this.rbRptMovDet.AutoSize = true;
            this.rbRptMovDet.Location = new System.Drawing.Point(146, 15);
            this.rbRptMovDet.Name = "rbRptMovDet";
            this.rbRptMovDet.Size = new System.Drawing.Size(183, 22);
            this.rbRptMovDet.TabIndex = 18;
            this.rbRptMovDet.Text = "Detalle de Movimientos";
            this.rbRptMovDet.UseVisualStyleBackColor = true;
            // 
            // rbRptMov
            // 
            this.rbRptMov.AutoSize = true;
            this.rbRptMov.Checked = true;
            this.rbRptMov.Location = new System.Drawing.Point(5, 15);
            this.rbRptMov.Name = "rbRptMov";
            this.rbRptMov.Size = new System.Drawing.Size(114, 22);
            this.rbRptMov.TabIndex = 15;
            this.rbRptMov.TabStop = true;
            this.rbRptMov.Text = "Movimientos";
            this.rbRptMov.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.rbRptTipoMovAll);
            this.panel2.Controls.Add(this.rbRptMovCompra);
            this.panel2.Controls.Add(this.rbRptMovVenta);
            this.panel2.Location = new System.Drawing.Point(208, 81);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(412, 43);
            this.panel2.TabIndex = 10;
            // 
            // rbRptTipoMovAll
            // 
            this.rbRptTipoMovAll.AutoSize = true;
            this.rbRptTipoMovAll.Checked = true;
            this.rbRptTipoMovAll.Location = new System.Drawing.Point(5, 12);
            this.rbRptTipoMovAll.Name = "rbRptTipoMovAll";
            this.rbRptTipoMovAll.Size = new System.Drawing.Size(185, 22);
            this.rbRptTipoMovAll.TabIndex = 30;
            this.rbRptTipoMovAll.TabStop = true;
            this.rbRptTipoMovAll.Text = "Todos los movimientos";
            this.rbRptTipoMovAll.UseVisualStyleBackColor = true;
            // 
            // rbRptMovCompra
            // 
            this.rbRptMovCompra.AutoSize = true;
            this.rbRptMovCompra.Location = new System.Drawing.Point(304, 12);
            this.rbRptMovCompra.Name = "rbRptMovCompra";
            this.rbRptMovCompra.Size = new System.Drawing.Size(91, 22);
            this.rbRptMovCompra.TabIndex = 25;
            this.rbRptMovCompra.Text = "Compras";
            this.rbRptMovCompra.UseVisualStyleBackColor = true;
            // 
            // rbRptMovVenta
            // 
            this.rbRptMovVenta.AutoSize = true;
            this.rbRptMovVenta.Location = new System.Drawing.Point(207, 12);
            this.rbRptMovVenta.Name = "rbRptMovVenta";
            this.rbRptMovVenta.Size = new System.Drawing.Size(74, 22);
            this.rbRptMovVenta.TabIndex = 26;
            this.rbRptMovVenta.Text = "Ventas";
            this.rbRptMovVenta.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(170, 18);
            this.label4.TabIndex = 27;
            this.label4.Text = "Tipo de Movimientos:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(205, 148);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 18);
            this.label1.TabIndex = 3;
            this.label1.Text = "Fecha Inicio:";
            // 
            // dpFecha1
            // 
            this.dpFecha1.CustomFormat = "dd/MM/yyyy ";
            this.dpFecha1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpFecha1.Location = new System.Drawing.Point(309, 148);
            this.dpFecha1.Name = "dpFecha1";
            this.dpFecha1.Size = new System.Drawing.Size(150, 24);
            this.dpFecha1.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(31, 147);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 18);
            this.label3.TabIndex = 24;
            this.label3.Text = "Rango de Fechas:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(46, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(134, 18);
            this.label5.TabIndex = 23;
            this.label5.Text = "Tipo de Reporte:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(465, 148);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 18);
            this.label2.TabIndex = 16;
            this.label2.Text = "Fecha fin:";
            // 
            // dpFecha2
            // 
            this.dpFecha2.CustomFormat = "dd/MM/yyyy ";
            this.dpFecha2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpFecha2.Location = new System.Drawing.Point(552, 147);
            this.dpFecha2.Name = "dpFecha2";
            this.dpFecha2.Size = new System.Drawing.Size(150, 24);
            this.dpFecha2.TabIndex = 17;
            // 
            // txtNombre
            // 
            this.txtNombre.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNombre.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.txtNombre.Controls.Add(this.pictureBox1);
            this.txtNombre.Controls.Add(this.lblTitleForm);
            this.txtNombre.Location = new System.Drawing.Point(-3, -1);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(1439, 48);
            this.txtNombre.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::InventarioForms.Properties.Resources.icons8_presentation_48;
            this.pictureBox1.Location = new System.Drawing.Point(3, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(50, 47);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // lblTitleForm
            // 
            this.lblTitleForm.AutoSize = true;
            this.lblTitleForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitleForm.ForeColor = System.Drawing.Color.White;
            this.lblTitleForm.Location = new System.Drawing.Point(68, 10);
            this.lblTitleForm.Name = "lblTitleForm";
            this.lblTitleForm.Size = new System.Drawing.Size(85, 20);
            this.lblTitleForm.TabIndex = 4;
            this.lblTitleForm.Text = "Reportes";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::InventarioForms.Properties.Resources.reportes_excursiones;
            this.pictureBox2.Location = new System.Drawing.Point(788, 66);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(237, 230);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 9;
            this.pictureBox2.TabStop = false;
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackgroundImage = global::InventarioForms.Properties.Resources.icons8_unavailable_96;
            this.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnCancelar.Location = new System.Drawing.Point(384, 298);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(88, 84);
            this.btnCancelar.TabIndex = 8;
            this.btnCancelar.Text = " ";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnGenerar
            // 
            this.btnGenerar.BackgroundImage = global::InventarioForms.Properties.Resources.icons8_ok_96;
            this.btnGenerar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnGenerar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGenerar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerar.ForeColor = System.Drawing.Color.DarkGreen;
            this.btnGenerar.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnGenerar.Location = new System.Drawing.Point(291, 298);
            this.btnGenerar.Name = "btnGenerar";
            this.btnGenerar.Size = new System.Drawing.Size(87, 84);
            this.btnGenerar.TabIndex = 6;
            this.btnGenerar.Text = " ";
            this.btnGenerar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnGenerar.UseVisualStyleBackColor = true;
            this.btnGenerar.Click += new System.EventHandler(this.btnGenerar_Click);
            // 
            // btnRptExistencias
            // 
            this.btnRptExistencias.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRptExistencias.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRptExistencias.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRptExistencias.ForeColor = System.Drawing.Color.DarkGreen;
            this.btnRptExistencias.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnRptExistencias.Location = new System.Drawing.Point(199, 112);
            this.btnRptExistencias.Name = "btnRptExistencias";
            this.btnRptExistencias.Size = new System.Drawing.Size(325, 59);
            this.btnRptExistencias.TabIndex = 10;
            this.btnRptExistencias.Text = " Generar reporte de Existencias";
            this.btnRptExistencias.UseVisualStyleBackColor = true;
            this.btnRptExistencias.Click += new System.EventHandler(this.btnRptExistencias_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtNombre1);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.btnRptExistencias);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(21, 423);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(720, 196);
            this.groupBox2.TabIndex = 28;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Filtro del Reporte de Existencias";
            // 
            // txtNombre1
            // 
            this.txtNombre1.Location = new System.Drawing.Point(9, 63);
            this.txtNombre1.Name = "txtNombre1";
            this.txtNombre1.Size = new System.Drawing.Size(684, 24);
            this.txtNombre1.TabIndex = 24;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(6, 42);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(267, 18);
            this.label9.TabIndex = 23;
            this.label9.Text = "Escriba un nombre si desea filtrar:";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::InventarioForms.Properties.Resources._3pl_control_de_inventarios;
            this.pictureBox3.Location = new System.Drawing.Point(788, 338);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(237, 230);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 29;
            this.pictureBox3.TabStop = false;
            // 
            // frmReportes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1433, 654);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGenerar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmReportes";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reportes";
            this.Load += new System.EventHandler(this.frmListData_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.txtNombre.ResumeLayout(false);
            this.txtNombre.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel txtNombre;
        private System.Windows.Forms.Label lblTitleForm;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGenerar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton rbRptMovVenta;
        private System.Windows.Forms.RadioButton rbRptMovCompra;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton rbRptMovDet;
        private System.Windows.Forms.DateTimePicker dpFecha2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbRptMov;
        private System.Windows.Forms.DateTimePicker dpFecha1;
        private System.Windows.Forms.RadioButton rbRptTipoMovAll;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnRptExistencias;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtNombre1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}