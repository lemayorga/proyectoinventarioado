﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InventarioForms;
using BussinesLogic;
using Entity;
using InventarioForms.Code;
using InventarioForms.Code.Interface;
using InventarioForms.Code.Extensions;
using static BussinesLogic.TiposMovimientosRepository;

//http://viblend.com/blog/?tag=/datagridview
namespace InventarioForms.formularios
{
    public partial class frmListFindProductos : Form 
    {
        #region Variables
        /// <summary>
        /// Repositorio a utilizar
        /// </summary>
        private ProductosRepository repoPrincipal;
        /// <summary>
        /// Formulario que manda a llamar este formulario
        /// </summary>
        private IFormCallFindProd frmAdd;
        /// <summary>
        /// Saber el tipo de Movimiento
        /// </summary>
        private DataTiposMov tipoMov;
        /// <summary>
        /// Saber si hay errores
        /// </summary>
        private string Errores;
        #endregion

        public frmListFindProductos(DataTiposMov tipoMov_, IFormCallFindProd frmAdd_)
        {
            InitializeComponent();
            tipoMov = tipoMov_;
            frmAdd = frmAdd_;
            frmAdd.productosSelect = frmAdd.productosSelect ?? new List<ProductosSelect>();
            repoPrincipal = new ProductosRepository();
        }

        /// <summary>
        /// Listar los datos del grid
        /// </summary>
        public void fnListData()
        {
            try
            {
                var data = repoPrincipal.GetProductosSelect_DT(tipoMov);
                this.gridViewData.DataSource = new BindingSource(data, null);
                this.gridViewData.FormaStyle(DataGridViewEditMode.EditOnKeystrokeOrF2);

                this.gridViewData.Columns["costo"].DefaultCellStyle.Format = $"N{Constantes.CANT_DECIMALES}";
                this.gridViewData.Columns["precio"].DefaultCellStyle.Format = $"N{Constantes.CANT_DECIMALES}";
                this.gridViewData.Columns["cantidad"].DefaultCellStyle.Format = $"N{Constantes.CANT_SIN_DECIMALES}";
                this.gridViewData.Columns["productoId"].HeaderText = "ID";
                this.gridViewData.Columns["producto"].Width = 160;
                this.gridViewData.Columns["marca"].Width = 150;
                this.gridViewData.Columns["modelo"].Width = 130;
                this.gridViewData.Columns["cantidad"].ReadOnly = false;

                switch (tipoMov)
                {
                    case DataTiposMov.compras:

                        this.gridViewData.Columns["existencia"].Visible = false;
                        this.gridViewData.Columns["precio"].Visible = false;
                        break;
                    case DataTiposMov.ventas:

                        this.gridViewData.Columns["costo"].Visible = false;

                        break;
                }
            }
            catch (Exception ex)
            {
                 MsgAlert.Error(ex);
            }
        }


        private void frmListData_Load(object sender, EventArgs e)
        {
            fnListData();
        }


        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            gridViewData.FilterOneValue((sender as TextBox).Text, new string[] { "producto", "modelo", "marca" });
        }

        private void gridViewData_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress -= new KeyPressEventHandler(TextBoxNumberExtensions.Column_KeyPressNumber);
            TextBox tb = e.Control as TextBox;
            if (tb == null) return;

            if (this.gridViewData.CurrentRow.Cells["cantidad"].ColumnIndex == this.gridViewData.CurrentCell.ColumnIndex)
                tb.KeyPress += new KeyPressEventHandler(TextBoxNumberExtensions.Column_KeyPressNumber);
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            OKProductosSelecct();
        }

        private void OKProductosSelecct()
        {
            try
            {
                //Buscar los productos con cantidades distintas de cero
                var rowSelecct = this.gridViewData.Rows.Cast<DataGridViewRow>().Where(r => int.Parse(r.Cells["cantidad"].Value.ToString()) != 0);
                if (!rowSelecct.Any())
                {
                    MsgAlert.Warning("No tiene productos con cantidades mayor a cero para procesar los datos");
                    return;
                }
                var sms = GenerarMensaje();
                if (!string.IsNullOrEmpty(sms))
                {
                    MsgAlert.Error(sms);
                    return;
                }


                if (!frmAdd.productosSelect.Any()) /// Rregistros nuevos
                {
                    foreach (DataGridViewRow row in rowSelecct)
                        AddNewRegisterRow(row);
                }
                else
                {
                    foreach (DataGridViewRow row in rowSelecct)
                    {
                        var prodExiste = frmAdd.productosSelect.FirstOrDefault(x => ((ProductosSelect)x).productoId == int.Parse(row.Cells["productoId"].Value.ToString()));
                        if (prodExiste == null)
                            AddNewRegisterRow(row);
                        else
                            ((ProductosSelect)prodExiste).cantidad = int.Parse(row.Cells["cantidad"].Value.ToString());
                    }
                }

                ///Recargar el grid con los nuevos datos
                if (frmAdd is IFormAdd)
                {
                    frmAdd.ReloadDataProd();
                    this.Dispose();
                }
            }
            catch (Exception ex)
            {
                 MsgAlert.Error(ex);
            }
        }

        /// <summary>
        /// Agregar registro a la variable de productos del formulario que abrio este formulario
        /// </summary>
        /// <param name="row"></param>
        private void AddNewRegisterRow(DataGridViewRow row)
        {
            frmAdd.productosSelect.Add(new ProductosSelect()
            {
                productoId = int.Parse(row.Cells["productoId"].Value.ToString()),
                producto = row.Cells["producto"].Value.ToString(),
                marca = row.Cells["marca"].Value.ToString(),
                modelo = row.Cells["modelo"].Value.ToString(),
                costo = double.Parse(row.Cells["costo"].Value.ToString()),
                precio = double.Parse(row.Cells["precio"].Value.ToString()),
                cantidad = int.Parse(row.Cells["cantidad"].Value.ToString()),
            });
        }

        private void gridViewData_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (gridViewData.Rows[e.RowIndex].IsNewRow) { return; }

            var row = this.gridViewData.CurrentRow;

            try
            {
                if (e.ColumnIndex == gridViewData.Columns["cantidad"].Index)
                {
                    var sms = GenerarMensaje(e.RowIndex);
                    if (!string.IsNullOrEmpty(sms))
                    {
                        gridViewData.Rows[e.RowIndex].ErrorText = Errores;
                        MsgAlert.Error(sms);
                    }

                    else
                    {
                        gridViewData.Rows[e.RowIndex].ErrorText = string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                 MsgAlert.Error(ex);
            }
        }

        /// <summary>
        /// Generar mensaje , recorriendo loas filas del grid
        /// </summary>
        /// <param name="indice"></param>
        /// <returns></returns>
        private string GenerarMensaje(int indice = -1)
        {
            string sms = string.Empty;


            switch (tipoMov)
            {
                case DataTiposMov.compras:
                    break;
                case DataTiposMov.ventas:

                    int cantidad, existencia;
                    string producto, marca, modelo;
                    if (indice != -1)
                    {
                        cantidad = int.Parse(gridViewData.Rows[indice].Cells["cantidad"].Value.ToString());
                        existencia = int.Parse(gridViewData.Rows[indice].Cells["existencia"].Value.ToString());
                        producto = gridViewData.Rows[indice].Cells["producto"].Value.ToString();
                        marca = gridViewData.Rows[indice].Cells["marca"].Value.ToString();
                        modelo = gridViewData.Rows[indice].Cells["modelo"].Value.ToString();

                        if (cantidad > existencia)
                            sms = $"La cantidad ingresada para el producto \n **{producto} - {marca} {modelo}** no puede ser mayor a la existencia del producto";
                    }
                    else
                    {
                        for (int i = 0; i < gridViewData.Rows.Count; i++)
                        {
                            cantidad = int.Parse(gridViewData.Rows[i].Cells["cantidad"].Value.ToString());
                            existencia = int.Parse(gridViewData.Rows[i].Cells["existencia"].Value.ToString());
                            producto = gridViewData.Rows[i].Cells["producto"].Value.ToString();
                            marca = gridViewData.Rows[i].Cells["marca"].Value.ToString();
                            modelo = gridViewData.Rows[i].Cells["modelo"].Value.ToString();

                            if (cantidad > existencia)
                                sms += $"*Cantidad para el producto **{producto} - {marca} {modelo}** no puede ser mayor a la existencia. \n\n";
                        }
                    }

                    break;
            }


            return sms;
        }
    }
}
    