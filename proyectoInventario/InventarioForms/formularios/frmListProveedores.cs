﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InventarioForms;
using BussinesLogic;
using Entity;
using InventarioForms.Code;
using InventarioForms.Code.Interface;
using InventarioForms.Code.Extensions;

//http://viblend.com/blog/?tag=/datagridview
namespace InventarioForms.formularios
{
    public partial class frmListProveedores : Form , IFormListar
    {
        #region Variables
        /// <summary>
        /// Repositorio a utilizar
        /// </summary>
        private PersonasRepository repoPrincipal;
        /// <summary>
        /// Formulario de Add,Update
        /// </summary>
        private IFormAdd frmAdd;
        /// <summary>
        /// Formulario tab contenedor
        /// </summary>       
        private FormParentTab formParentTab;

        #endregion

        public frmListProveedores()
        {
            InitializeComponent();
            repoPrincipal = new PersonasRepository();
            formParentTab = new FormParentTab();
            MyControls.UsuarioRecursos(this);
        }

        /// <inheritdoc/>
        public void fnListData()
        {
            try
            {
                var data = repoPrincipal.GetAllClasificados_DT(ClasficacionesRepository.DataClasif.proveedores);
                this.gridViewData.DataSource = new BindingSource(data, null);
                this.gridViewData.FormaStyle();

                this.gridViewData.Columns["personaId"].HeaderText = "ID";
                this.gridViewData.Columns["nombres"].HeaderText = "RAZON SOCIAL";
                this.gridViewData.Columns["apellidos"].HeaderText = "SIGLAS";
                this.gridViewData.Columns["ndi"].HeaderText = "RUC";
                this.gridViewData.Columns["nombres"].Width = 390;
                this.gridViewData.Columns["apellidos"].Width = 130;
                this.gridViewData.Columns["domicilio"].Width = 180;
                this.gridViewData.Columns["telefonos"].Width = 130;
                this.gridViewData.Columns["correos"].Width = 130;
            }
            catch (Exception ex)
            {
                 MsgAlert.Error(ex);
            }
        }

        /// <inheritdoc/>
        public void fnShowChildren(ref TabControl tab_Control)
        {
            formParentTab.fnShowAsChildrenTabPage(this, ref tab_Control);
        }

        /// <inheritdoc/>
        private void fnClose()
        {
            formParentTab.fnCloseAsChildrenTabPage();
        }

        /// <inheritdoc/>
        public void fnOpenUpdate(object key)
        {
            frmAdd = new frmAddProveedor();
            frmAdd.fnSetInitVariablesShow(this, int.Parse(key.ToString()));
        }

        /// <inheritdoc/>
        public void fnOpenNew()
        {
            frmAdd = new frmAddProveedor();
            frmAdd.fnSetInitVariablesShow(this);
        }

        private void DeleteItemGrid(object key, DataGridViewCellCollection columns)
        {
            try
            {
              if(MsgAlert.Question($"Registro con Id:{key.ToString()} \n\n** {columns["nombres"].Value} {columns["apellidos"].Value}**\n¿Seguro desea borrar el registro?", msgButtons:  MessageBoxButtons.YesNo) == DialogResult.Yes)
              {
                    if (repoPrincipal.TieneMovimientos(int.Parse(key.ToString())))
                    {
                        MsgAlert.Information($"Registro ** {columns["nombres"].Value} {columns["apellidos"].Value}** cuenta con movimientos. \n\n No se puede realizar la eliminación.");
                        return;
                    }

                    if (repoPrincipal.Delete(int.Parse(key.ToString()), ClasficacionesRepository.DataClasif.proveedores))
                    {
                        this.fnListData();
                        MsgAlert.Information("Registro borrado sastifactoriamente");
                    }
              }
            }
            catch (Exception ex)
            {
                 MsgAlert.Error(ex);
            }
        } 

        private void frmListData_Load(object sender, EventArgs e)
        {
            fnListData();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            fnClose();
        }

        private void btnClearFilter_Click(object sender, EventArgs e)
        {
            fnListData();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            fnOpenNew();
        }

        private void gridViewData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.gridViewData.CurrentRow == null)
            {
                MsgAlert.Information("Seleccione un registro del listado");
                return;
            }

            var cell = this.gridViewData.CurrentRow.Cells[repoPrincipal.GetKeyName()];
            if (cell != null) DeleteItemGrid(cell.Value, this.gridViewData.CurrentRow.Cells);
            else MsgAlert.Information("Seleccione un registro del listado");
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            gridViewData.FilterOneValue((sender as TextBox).Text, new string[] { "nombres", "apellidos ", "domicilio", "correos", "ndi", "telefonos" });
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            var cell = this.gridViewData.CurrentRow.Cells[repoPrincipal.GetKeyName()];
            if (cell != null) fnOpenUpdate(cell.Value);
        }
    }
}
    