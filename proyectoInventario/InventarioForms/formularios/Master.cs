﻿using InventarioForms.Code;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InventarioForms.formularios
{
    public partial class Master : Form
    {
        public Master()
        {
            InitializeComponent();
            GetTime();
            this.lblUsuario.Text += Constantes.USER_NAME;
            this.lblRol.Text += Constantes.ROLES;
            this.lblServidor.Text += Constantes.MY_SERVIDOR;
            this.lblBaseDatos.Text += Constantes.MY_BASE_DATOS;
            this.lblAppName.Text = Constantes.APP_NAME;
            MyControls.UsuarioRecursos(this);
        }

        /// <summary>
        /// Establecer Timpo para la hora del formulario
        /// </summary>
        private void GetTime()
        {
            this.lblTime.Text = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.GetTime();
        }

        private void btnClientes_Click(object sender, EventArgs e)
        {
            new frmListData().fnShowChildren(ref this.tabControl1);
        }

        private void btnProductos_Click(object sender, EventArgs e)
        {
            new frmListProductos().fnShowChildren(ref this.tabControl1);
        }

        private void btnProveedores_Click(object sender, EventArgs e)
        {
            new frmListProveedores().fnShowChildren(ref this.tabControl1);
        }

        private void btnCompras_Click(object sender, EventArgs e)
        {
            new frmListCompras().fnShowChildren(ref this.tabControl1);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Constantes.APP.SetFormLogin(this);
        }
        private void Master_FormClosing(object sender, FormClosingEventArgs e)
        {
            Constantes.APP.SetFormLogin(this);
        }

        private void btnVentas_Click(object sender, EventArgs e)
        {
            new frmListVentas().fnShowChildren(ref this.tabControl1);
        }

        private void btnReportes_Click(object sender, EventArgs e)
        {
            new frmReportes().fnShowChildren(ref this.tabControl1);
        }

        private void btnUsuarios_Click(object sender, EventArgs e)
        {
            new frmListUsuarios().fnShowChildren(ref this.tabControl1);
        }
    }
}
