﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InventarioForms;
using BussinesLogic;
using Entity;
using InventarioForms.Code;
using InventarioForms.Code.Interface;
using InventarioForms.Code.Extensions;
using static BussinesLogic.TiposMovimientosRepository;
using static BussinesLogic.EstadosRepository;

//http://viblend.com/blog/?tag=/datagridview
namespace InventarioForms.formularios
{
    public partial class frmListVentas : Form , IFormListar
    {
        #region Variables
        /// <summary>
        /// Repositorio a utilizar
        /// </summary>
        private MovimientosRepository repoPrincipal;
        /// <summary>
        /// Formulario de Add,Update
        /// </summary>
        private IFormAdd frmAdd;
        /// <summary>
        /// Formulario tab contenedor
        /// </summary>  
        private FormParentTab formParentTab;
        /// <summary>
        /// Estado respositorio
        /// </summary   
        private EstadosRepository estadoRepo = new EstadosRepository();

        #endregion

        public frmListVentas()
        {
            InitializeComponent();
            repoPrincipal = new MovimientosRepository();
            formParentTab = new FormParentTab();
            MyControls.UsuarioRecursos(this);
        }

        /// <inheritdoc/>
        public void fnListData()
        {
            try
            {
                var data = repoPrincipal.GetMovimientosAllData_DT(DataTiposMov.ventas);
                this.gridViewData.DataSource = new BindingSource(data, null);
                this.gridViewData.FormaStyle();

                this.gridViewData.Columns["movimientoId"].HeaderText = "MOV. ID";
                this.gridViewData.Columns["descuento"].HeaderText = "DESCUENTO %";
                this.gridViewData.Columns["impuesto"].HeaderText = "IMPUESTO %";
                this.gridViewData.Columns["persona"].HeaderText = "CLIENTE";
                this.gridViewData.Columns["numero"].HeaderText = "REFERENCIA";
                this.gridViewData.Columns["movimientoId"].Width = 80;
                this.gridViewData.Columns["descuento"].Width = 120;
                this.gridViewData.Columns["impuesto"].Width = 120;
                this.gridViewData.Columns["persona"].Width = 320;
                this.gridViewData.Columns["fecha"].Width = 80;
                this.gridViewData.Columns["observaciones"].Width = 300;
                this.gridViewData.Columns["subtotal"].DefaultCellStyle.Format = $"N{Constantes.CANT_DECIMALES}";
                this.gridViewData.Columns["descuento"].DefaultCellStyle.Format = $"N{Constantes.CANT_DECIMALES}";
                this.gridViewData.Columns["impuesto"].DefaultCellStyle.Format = $"N{Constantes.CANT_DECIMALES}";
                this.gridViewData.Columns["total"].DefaultCellStyle.Format = $"N{Constantes.CANT_DECIMALES}";
                this.gridViewData.Columns["fecha"].DefaultCellStyle.Format = Constantes.FORMAT_DATE;
                this.gridViewData.Columns["perNombre"].Visible = false;
                this.gridViewData.Columns["perApellido"].Visible = false;
                this.gridViewData.Columns["personaId"].Visible = false;
                this.gridViewData.Columns["tipoId"].Visible = false;
                this.gridViewData.Columns["tipoMov"].Visible = false;
                this.gridViewData.Columns["estadoId"].Visible = false;
                this.gridViewData.Columns["estCodigo"].Visible = false;
            }
            catch (Exception ex)
            {
                 MsgAlert.Error(ex);
            }
        }

        public void fnShowChildren(ref TabControl tab_Control)
        {
            formParentTab.fnShowAsChildrenTabPage(this, ref tab_Control);
        }

        /// <inheritdoc/>
        private void fnClose()
        {
            formParentTab.fnCloseAsChildrenTabPage();
        }

        /// <inheritdoc/>
        public void fnOpenUpdate(object key)
        {
            frmAdd = new frmAddVenta();
            frmAdd.fnSetInitVariablesShow(this, int.Parse(key.ToString()));
        }

        /// <inheritdoc/>
        public void fnOpenNew()
        {
            frmAdd = new frmAddVenta();
            frmAdd.fnSetInitVariablesShow(this);
        }

        private void DeleteItemGrid(object key, DataGridViewCellCollection columns)
        { 
            try
            {
                if (columns["estCodigo"].Value.ToString() == DataEstados.anulado.DescriptionAttr())
                {

                    MsgAlert.Information($"Registro con Id:{key.ToString()} \n\n** {columns["movimientoId"].Value} ** \n Referncia: {columns["numero"].Value} \n ya se encuentra anulado");
                    return;
                }

              if(MsgAlert.Question($"Registro con Id:{key.ToString()} \n\n** {columns["movimientoId"].Value} ** \n Referncia: {columns["numero"].Value} \n¿Seguro desea anular el registro?", msgButtons:  MessageBoxButtons.YesNo) == DialogResult.Yes)
              {
                    if (repoPrincipal.AnularMovimiento(int.Parse(key.ToString()),Constantes.USER_NAME, DateTime.Now))
                    {
                        this.fnListData();
                        MsgAlert.Information("Registro anulado sastifactoriamente");
                    }
              }
            }
            catch (Exception ex)
            {
                 MsgAlert.Error(ex);
            }
        } 

        private void frmListData_Load(object sender, EventArgs e)
        {
            fnListData();
        }


        private void btnCancelar_Click(object sender, EventArgs e)
        {
            fnClose();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            fnOpenNew();
        }

        private void gridViewData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.gridViewData.CurrentRow == null)
            {
                MsgAlert.Information("Seleccione un registro del listado");
                return;
            }

            var cell = this.gridViewData.CurrentRow.Cells[repoPrincipal.GetKeyName()];
            if (cell != null) DeleteItemGrid(cell.Value, this.gridViewData.CurrentRow.Cells);
            else MsgAlert.Information("Seleccione un registro del listado");
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            gridViewData.FilterOneValue((sender as TextBox).Text, new string[] { "numero", "persona " });
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            var cell = this.gridViewData.CurrentRow.Cells[repoPrincipal.GetKeyName()];
            if (cell != null) fnOpenUpdate(cell.Value);
        }
    }
}
    