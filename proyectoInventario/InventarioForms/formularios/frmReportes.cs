﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InventarioForms;
using BussinesLogic;
using Entity;
using InventarioForms.Code;
using InventarioForms.Code.Interface;
using InventarioForms.Code.Extensions;
using BussinesLogic.Reports;
using BussinesLogic.Extensions;
using InventarioForms.reportes.MovimientosDetalle;
using InventarioForms.reportes.Movimientos;
using InventarioForms.reportes;
using InventarioForms.reportes.Existencias;

//http://viblend.com/blog/?tag=/datagridview
namespace InventarioForms.formularios
{
    public partial class frmReportes : Form 
    {
        #region Variables
        /// <summary>
        /// Formulario tab contenedor
        /// </summary>       
        private FormParentTab formParentTab;
        /// <summary>
        /// Clasificacion de los tipos de reportes
        /// </summary>
        private enum TipoReporte
        {
            movimientos,
            movimientosDetalle,
        }

        #endregion

        public frmReportes()
        {
            InitializeComponent();
            formParentTab = new FormParentTab();
        }

        public void fnShowChildren(ref TabControl tab_Control)
        {
            formParentTab.fnShowAsChildrenTabPage(this, ref tab_Control);
        }
        

        private void frmListData_Load(object sender, EventArgs e)
        {
     
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            formParentTab.fnCloseAsChildrenTabPage();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
           
        }

        private void btnGenerar_Click(object sender, EventArgs e)
        {
            GenerarReporte();
        }

        /// <summary>
        /// Generar el llamado al reporte
        /// </summary>
        private void GenerarReporte()
        {
            try
            {
                IReport report = null;
                var tipoReporte = rbRptMov.Checked ? TipoReporte.movimientos
                                 : rbRptMovDet.Checked ? TipoReporte.movimientosDetalle : 0;

                var tipoMovimiento = rbRptTipoMovAll.Checked ? TiposMovimientosRepository.DataTiposMov.todos
                                 : rbRptMovVenta.Checked ? TiposMovimientosRepository.DataTiposMov.ventas : TiposMovimientosRepository.DataTiposMov.compras;

                switch (tipoReporte)
                {
                    case TipoReporte.movimientos:
                        report = new MovimientosBase(new MovimientosParameter() { codigo = tipoMovimiento, fechaInicial = dpFecha1.Value.Date, fechaFinal = dpFecha2.Value.Date });
                        break;

                    case TipoReporte.movimientosDetalle:
                        report = new rptDataMovimientosDetale(new MovimientosDetalleParameter() { codigo = tipoMovimiento, fechaInicial = dpFecha1.Value.Date, fechaFinal = dpFecha2.Value.Date });
                        break;
                }
                if (report == null)  return;

                var formReport = new frmReportViwer();
                formReport.GenerateReport(report);
            }
            catch (Exception ex)
            {
                 MsgAlert.Error(ex);
            }
        }

        private void btnRptExistencias_Click(object sender, EventArgs e)
        {
            try
            {
                var formReport = new frmReportViwer();
                IReport report = new ExistenciasBase(new ExistenciasParameter() { nombre = txtNombre1.Text });
                formReport.GenerateReport(report);
            }
            catch (Exception ex)
            {
                MsgAlert.Error(ex);
            }
        }
    }
}
    