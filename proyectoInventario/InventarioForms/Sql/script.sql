use master
go

IF EXISTS (SELECT * FROM sysdatabases WHERE (name = 'inventario')) 
BEGIN
	drop database inventario
END


create database inventario

go

USE [inventario]
GO
/****** Object:  Table [dbo].[AppControls]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppControls](
	[appControlId] [int] IDENTITY(1,1) NOT NULL,
	[controlName] [varchar](50) NOT NULL,
	[appFormId] [int] NOT NULL,
	[alias] [varchar](50) NOT NULL,
	[visible] [bit] NOT NULL DEFAULT ((1)),
 CONSTRAINT [PK_AppControls] PRIMARY KEY CLUSTERED 
(
	[appControlId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AppForms]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppForms](
	[appFormId] [int] IDENTITY(1,1) NOT NULL,
	[formName] [varchar](50) NOT NULL,
	[formFatherId] [int] NULL,
	[alias] [varchar](50) NOT NULL,
	[visible] [bit] NOT NULL DEFAULT ((1)),
 CONSTRAINT [PK_AppForms] PRIMARY KEY CLUSTERED 
(
	[appFormId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AppRoles]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppRoles](
	[appRolId] [int] IDENTITY(1,1) NOT NULL,
	[appRolName] [varchar](50) NOT NULL,
	[descripcion] [nvarchar](500) NULL,
	[masterInic] [nvarchar](100) NULL,
 CONSTRAINT [PK_AppRoles] PRIMARY KEY CLUSTERED 
(
	[appRolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AppRolesControls]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppRolesControls](
	[appRolesControlId] [int] IDENTITY(1,1) NOT NULL,
	[appRolId] [int] NOT NULL,
	[appControlId] [int] NOT NULL,
 CONSTRAINT [PK_AppRolesControls] PRIMARY KEY CLUSTERED 
(
	[appRolesControlId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AppRolesUser]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppRolesUser](
	[appRolUserId] [int] IDENTITY(1,1) NOT NULL,
	[appRolId] [int] NOT NULL,
	[appUserId] [int] NOT NULL,
 CONSTRAINT [PK_AppRolesUser] PRIMARY KEY CLUSTERED 
(
	[appRolUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AppUsers]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppUsers](
	[appUserId] [int] IDENTITY(1,1) NOT NULL,
	[appUserName] [varchar](50) NOT NULL,
	[firstName] [varchar](50) NOT NULL,
	[lastName] [varchar](50) NOT NULL,
	[bloqueado] [bit] NOT NULL CONSTRAINT [DF_AppUsers_disabled]  DEFAULT ((0)),
	[password] [varchar](150) NOT NULL,
 CONSTRAINT [PK_AppUsers] PRIMARY KEY CLUSTERED 
(
	[appUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Clasificaciones]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clasificaciones](
	[clasificacionId] [int] IDENTITY(1,1) NOT NULL,
	[clasificacion] [nvarchar](50) NOT NULL,
	[codigo] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_Clasficacion] PRIMARY KEY CLUSTERED 
(
	[clasificacionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ClasifPersonas]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClasifPersonas](
	[clasifPersId] [int] IDENTITY(1,1) NOT NULL,
	[personaId] [int] NOT NULL,
	[clasificacionId] [int] NOT NULL,
 CONSTRAINT [PK_ClasifPersonas] PRIMARY KEY CLUSTERED 
(
	[clasifPersId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Estados]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Estados](
	[estadoId] [int] IDENTITY(1,1) NOT NULL,
	[estado] [nvarchar](50) NOT NULL,
	[codigo] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Estados] PRIMARY KEY CLUSTERED 
(
	[estadoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Marcas]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Marcas](
	[marcaId] [int] IDENTITY(1,1) NOT NULL,
	[marca] [nvarchar](150) NOT NULL,
 CONSTRAINT [PK_Marcas] PRIMARY KEY CLUSTERED 
(
	[marcaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MovEstados]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MovEstados](
	[movEstadoId] [int] IDENTITY(1,1) NOT NULL,
	[estadoId] [int] NOT NULL,
	[movimientoId] [int] NOT NULL,
	[usuario] [nvarchar](50) NOT NULL,
	[fecha] [datetime] NOT NULL,
 CONSTRAINT [PK_MovEstados] PRIMARY KEY CLUSTERED 
(
	[movEstadoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MovimientoDet]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MovimientoDet](
	[movDetId] [int] IDENTITY(1,1) NOT NULL,
	[productoId] [int] NOT NULL,
	[movimientoId] [int] NOT NULL,
	[valor] [float] NOT NULL,
	[cantidad] [int] NOT NULL,
 CONSTRAINT [PK_MovimientoDet] PRIMARY KEY CLUSTERED 
(
	[movDetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Movimientos]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Movimientos](
	[movimientoId] [int] IDENTITY(1,1) NOT NULL,
	[numero] [nvarchar](8) NOT NULL,
	[fecha] [datetime] NOT NULL,
	[personaId] [int] NOT NULL,
	[tipoId] [int] NOT NULL,
	[subtotal] [float] NOT NULL,
	[descuento] [float] NOT NULL,
	[impuesto] [float] NOT NULL,
	[total] [float] NOT NULL,
	[observaciones] [nvarchar](max) NULL,
 CONSTRAINT [PK_Movimientos] PRIMARY KEY CLUSTERED 
(
	[movimientoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Personas]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Personas](
	[personaId] [int] IDENTITY(1,1) NOT NULL,
	[nombres] [nvarchar](50) NOT NULL,
	[apellidos] [nvarchar](50) NOT NULL,
	[ndi] [nchar](10) NULL,
	[domicilio] [nvarchar](max) NULL,
	[telefonos] [nvarchar](100) NULL,
	[correos] [nvarchar](350) NULL,
 CONSTRAINT [PK_Personas] PRIMARY KEY CLUSTERED 
(
	[personaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Productos]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Productos](
	[productoId] [int] IDENTITY(1,1) NOT NULL,
	[producto] [nvarchar](50) NOT NULL,
	[costo] [float] NOT NULL,
	[precio] [float] NOT NULL,
	[marcaId] [int] NULL,
	[modelo] [nvarchar](50) NULL,
 CONSTRAINT [PK_Producto] PRIMARY KEY CLUSTERED 
(
	[productoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TiposMovimientos]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TiposMovimientos](
	[tipoId] [int] IDENTITY(1,1) NOT NULL,
	[tipo] [nvarchar](50) NOT NULL,
	[codigo] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_TiposMovimientos] PRIMARY KEY CLUSTERED 
(
	[tipoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[vwMovimientosActivos]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwMovimientosActivos]
AS
SELECT me.movimientoId, me.movEstadoId, e.estado
FROM     dbo.MovEstados AS me INNER JOIN
                  dbo.Estados AS e ON me.estadoId = e.estadoId AND e.codigo = 'ACTIVO' INNER JOIN
                      (SELECT MAX(movEstadoId) AS movEstadoId, movimientoId
                       FROM      dbo.MovEstados AS me
                       GROUP BY movimientoId) AS mme ON mme.movEstadoId = me.movEstadoId

GO
/****** Object:  View [dbo].[vwExistenciasProducto]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwExistenciasProducto]
as

select p.productoId, ISNULL(md.existencia,0) as existencia
from Productos p 
left join(
Select md.productoId,SUM(md.cantidad) existencia
from    dbo.MovimientoDet md 
		inner join dbo.vwMovimientosActivos AS ma ON ma.movimientoId = md.movimientoId
		group by productoId
) md on md.productoId = p.productoId

GO
/****** Object:  View [dbo].[vwMovimientosAll]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwMovimientosAll]
AS
SELECT m.movimientoId, m.numero, m.fecha, m.personaId, p.nombres AS perNombre, p.apellidos AS perApellido, p.nombres + ' ' + p.apellidos AS persona, tm.tipo AS tipoMov, e.estadoId, e.estado, e.codigo AS estCodigo, m.tipoId, m.subtotal, 
                  m.descuento, m.impuesto, m.total, m.observaciones
FROM     dbo.Movimientos AS m INNER JOIN
                  dbo.TiposMovimientos AS tm ON m.tipoId = tm.tipoId INNER JOIN
                  dbo.Personas AS p ON p.personaId = m.personaId INNER JOIN
                  dbo.MovEstados AS me ON m.movimientoId = me.movimientoId INNER JOIN
                  dbo.Estados AS e ON e.estadoId = me.estadoId INNER JOIN
                      (SELECT MAX(movEstadoId) AS movEstadoId, movimientoId
                       FROM      dbo.MovEstados AS me
                       GROUP BY movimientoId) AS mme ON mme.movEstadoId = me.movEstadoId

GO
/****** Object:  View [dbo].[vwPersonas]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vwPersonas]
as

select c.codigo tipoPersona, p.nombres + ' ' + p.apellidos persona, p.* 
from Personas p inner join ClasifPersonas cp on cp.personaId = p.personaId
inner join Clasificaciones c on cp.clasificacionId = c.clasificacionId
GO
/****** Object:  View [dbo].[vwProductos]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwProductos]
AS
SELECT p.productoId, p.producto, m.marca, p.modelo, p.costo, p.precio, p.marcaId
FROM     dbo.Productos AS p LEFT OUTER JOIN
                  dbo.Marcas AS m ON m.marcaId = p.marcaId

GO
SET IDENTITY_INSERT [dbo].[AppControls] ON 

GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (1, N'btnClientes', 1, N'clientes', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (2, N'btnProveedores', 1, N'proveedores', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (3, N'btnProductos', 1, N'productos', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (4, N'btnVentas', 1, N'ventas', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (5, N'btnCompras', 1, N'compras', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (6, N'btnReportes', 1, N'reportes', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (7, N'btnUsuarios', 1, N'usuarios', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (8, N'btnNuevo', 2, N'nuevo', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (9, N'btnEditar', 2, N'editar', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (10, N'btnNuevo', 3, N'nuevo', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (11, N'btnEditar', 3, N'editar', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (12, N'btnDelete', 3, N'delete', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (13, N'btnNuevo', 4, N'nuevo', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (14, N'btnEditar', 4, N'editar', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (15, N'btnDelete', 4, N'delete', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (16, N'btnNuevo', 5, N'nuevo', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (17, N'btnEditar', 5, N'editar', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (18, N'btnDelete', 5, N'delete', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (19, N'btnNuevo', 6, N'nuevo', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (20, N'btnEditar', 6, N'editar', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (21, N'btnNuevo', 7, N'nuevo', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (22, N'btnEditar', 7, N'editar', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (23, N'btnDelete', 7, N'delete', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (24, N'btnSelecccionar', 8, N'selecccionar', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (25, N'btnGuardar', 8, N'guardar', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (26, N'btnGuardar', 9, N'guardar', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (27, N'btnGuardar', 10, N'guardar', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (28, N'btnGuardar', 11, N'guardar', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (29, N'btnGuardar', 12, N'guardar', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (30, N'btnSelecccionar', 13, N'selecccionar', 1)
GO
INSERT [dbo].[AppControls] ([appControlId], [controlName], [appFormId], [alias], [visible]) VALUES (31, N'btnGuardar', 13, N'guardar', 1)
GO
SET IDENTITY_INSERT [dbo].[AppControls] OFF
GO
SET IDENTITY_INSERT [dbo].[AppForms] ON 

GO
INSERT [dbo].[AppForms] ([appFormId], [formName], [formFatherId], [alias], [visible]) VALUES (1, N'Master', NULL, N'Formulario Principal', 1)
GO
INSERT [dbo].[AppForms] ([appFormId], [formName], [formFatherId], [alias], [visible]) VALUES (2, N'frmListCompras', 1, N'Listado de Compras', 1)
GO
INSERT [dbo].[AppForms] ([appFormId], [formName], [formFatherId], [alias], [visible]) VALUES (3, N'frmListData', 1, N'Listado de Clientes', 1)
GO
INSERT [dbo].[AppForms] ([appFormId], [formName], [formFatherId], [alias], [visible]) VALUES (4, N'frmListProductos', 1, N'Listado de Productos', 1)
GO
INSERT [dbo].[AppForms] ([appFormId], [formName], [formFatherId], [alias], [visible]) VALUES (5, N'frmListProveedores', 1, N'Listado de Proveedores', 1)
GO
INSERT [dbo].[AppForms] ([appFormId], [formName], [formFatherId], [alias], [visible]) VALUES (6, N'frmListUsuarios', 1, N'Listado de Usuarios', 1)
GO
INSERT [dbo].[AppForms] ([appFormId], [formName], [formFatherId], [alias], [visible]) VALUES (7, N'frmListVentas', 1, N'Listado de Ventas', 1)
GO
INSERT [dbo].[AppForms] ([appFormId], [formName], [formFatherId], [alias], [visible]) VALUES (8, N'frmAddCompra', 2, N'Agregar-Editar Compra', 1)
GO
INSERT [dbo].[AppForms] ([appFormId], [formName], [formFatherId], [alias], [visible]) VALUES (9, N'frmAddPersona', 3, N'Agregar-Editar Cliente', 1)
GO
INSERT [dbo].[AppForms] ([appFormId], [formName], [formFatherId], [alias], [visible]) VALUES (10, N'frmAddProductos', 4, N'Agregar-Editar Productos', 1)
GO
INSERT [dbo].[AppForms] ([appFormId], [formName], [formFatherId], [alias], [visible]) VALUES (11, N'frmAddProveedor', 5, N'Agregar-Editar Proveedor', 1)
GO
INSERT [dbo].[AppForms] ([appFormId], [formName], [formFatherId], [alias], [visible]) VALUES (12, N'frmAddUsuario', 6, N'Agregar-Editar Usuarios', 1)
GO
INSERT [dbo].[AppForms] ([appFormId], [formName], [formFatherId], [alias], [visible]) VALUES (13, N'frmAddVenta', 7, N'Agregar-Editar Venta', 1)
GO
SET IDENTITY_INSERT [dbo].[AppForms] OFF
GO
SET IDENTITY_INSERT [dbo].[AppRoles] ON 

GO
INSERT [dbo].[AppRoles] ([appRolId], [appRolName], [descripcion], [masterInic]) VALUES (2, N'sadministrador', NULL, NULL)
GO
INSERT [dbo].[AppRoles] ([appRolId], [appRolName], [descripcion], [masterInic]) VALUES (3, N'Administrador', N'Rol de administrador', NULL)
GO
INSERT [dbo].[AppRoles] ([appRolId], [appRolName], [descripcion], [masterInic]) VALUES (4, N'Gerente', N'Rol de usuario administracion de gerencia, para el control del inventario', NULL)
GO
INSERT [dbo].[AppRoles] ([appRolId], [appRolName], [descripcion], [masterInic]) VALUES (5, N'Bodega', N'Rol para acceso a los usuarios de bodega, registros de inventario', NULL)
GO
INSERT [dbo].[AppRoles] ([appRolId], [appRolName], [descripcion], [masterInic]) VALUES (6, N'Caja', N'Rol para acceso a los usuarios de caja,registros de inventario', NULL)
GO
SET IDENTITY_INSERT [dbo].[AppRoles] OFF
GO
SET IDENTITY_INSERT [dbo].[AppRolesControls] ON 

GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (1, 4, 1)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (2, 4, 2)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (3, 4, 3)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (4, 4, 6)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (5, 4, 10)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (6, 4, 11)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (7, 4, 12)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (8, 4, 13)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (9, 4, 14)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (10, 4, 15)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (11, 4, 16)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (12, 4, 17)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (13, 4, 18)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (14, 4, 26)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (15, 4, 27)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (16, 4, 28)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (17, 3, 1)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (18, 3, 2)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (19, 3, 3)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (20, 3, 4)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (21, 3, 5)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (22, 3, 6)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (23, 3, 7)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (24, 3, 8)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (25, 3, 9)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (26, 3, 10)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (27, 3, 11)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (28, 3, 12)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (29, 3, 13)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (30, 3, 14)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (31, 3, 15)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (32, 3, 16)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (33, 3, 17)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (34, 3, 18)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (35, 3, 19)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (36, 3, 20)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (37, 3, 21)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (38, 3, 22)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (39, 3, 23)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (40, 3, 24)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (41, 3, 25)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (42, 3, 26)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (43, 3, 27)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (44, 3, 28)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (45, 3, 29)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (46, 3, 30)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (47, 3, 31)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (48, 6, 4)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (49, 6, 21)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (50, 6, 22)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (51, 6, 23)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (52, 6, 30)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (53, 6, 31)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (54, 5, 5)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (55, 5, 8)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (56, 5, 9)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (57, 5, 24)
GO
INSERT [dbo].[AppRolesControls] ([appRolesControlId], [appRolId], [appControlId]) VALUES (58, 5, 25)
GO
SET IDENTITY_INSERT [dbo].[AppRolesControls] OFF
GO
SET IDENTITY_INSERT [dbo].[AppRolesUser] ON 

GO
INSERT [dbo].[AppRolesUser] ([appRolUserId], [appRolId], [appUserId]) VALUES (1, 2, 2)
GO
INSERT [dbo].[AppRolesUser] ([appRolUserId], [appRolId], [appUserId]) VALUES (17, 3, 6)
GO
SET IDENTITY_INSERT [dbo].[AppRolesUser] OFF
GO
SET IDENTITY_INSERT [dbo].[AppUsers] ON 

GO
INSERT [dbo].[AppUsers] ([appUserId], [appUserName], [firstName], [lastName], [bloqueado], [password]) VALUES (2, N'sadministrador', N'sadministrador', N'sadministrador', 0, N'managua0102')
GO
INSERT [dbo].[AppUsers] ([appUserId], [appUserName], [firstName], [lastName], [bloqueado], [password]) VALUES (6, N'admin', N'ADMIN', N'ADMIN', 0, N'123456')
GO
SET IDENTITY_INSERT [dbo].[AppUsers] OFF
GO
SET IDENTITY_INSERT [dbo].[Clasificaciones] ON 

GO
INSERT [dbo].[Clasificaciones] ([clasificacionId], [clasificacion], [codigo]) VALUES (1, N'clientes', N'CLIENTE')
GO
INSERT [dbo].[Clasificaciones] ([clasificacionId], [clasificacion], [codigo]) VALUES (2, N'proveedores', N'PROVEEDOR')
GO
SET IDENTITY_INSERT [dbo].[Clasificaciones] OFF
GO
SET IDENTITY_INSERT [dbo].[Estados] ON 

GO
INSERT [dbo].[Estados] ([estadoId], [estado], [codigo]) VALUES (1, N'activo', N'ACTIVO')
GO
INSERT [dbo].[Estados] ([estadoId], [estado], [codigo]) VALUES (2, N'anulado', N'ANULADO')
GO
SET IDENTITY_INSERT [dbo].[Estados] OFF
GO
SET IDENTITY_INSERT [dbo].[Marcas] ON 

GO
INSERT [dbo].[Marcas] ([marcaId], [marca]) VALUES (1, N'YAMAHA')
GO
INSERT [dbo].[Marcas] ([marcaId], [marca]) VALUES (2, N'DAYUN')
GO
INSERT [dbo].[Marcas] ([marcaId], [marca]) VALUES (3, N'SUZUKI')
GO
INSERT [dbo].[Marcas] ([marcaId], [marca]) VALUES (4, N'SERPENTO')
GO
INSERT [dbo].[Marcas] ([marcaId], [marca]) VALUES (5, N'HONDA')
GO
INSERT [dbo].[Marcas] ([marcaId], [marca]) VALUES (6, N'PULSAR')
GO
INSERT [dbo].[Marcas] ([marcaId], [marca]) VALUES (7, N'RAYBAR')
GO
INSERT [dbo].[Marcas] ([marcaId], [marca]) VALUES (8, N'GENESIS')
GO
INSERT [dbo].[Marcas] ([marcaId], [marca]) VALUES (9, N'HERO')
GO
INSERT [dbo].[Marcas] ([marcaId], [marca]) VALUES (10, N'KAWASAKI')
GO
INSERT [dbo].[Marcas] ([marcaId], [marca]) VALUES (11, N'BAJAJ')
GO
SET IDENTITY_INSERT [dbo].[Marcas] OFF
GO
SET IDENTITY_INSERT [dbo].[TiposMovimientos] ON 

GO
INSERT [dbo].[TiposMovimientos] ([tipoId], [tipo], [codigo]) VALUES (1, N'compras', N'COMPRAS')
GO
INSERT [dbo].[TiposMovimientos] ([tipoId], [tipo], [codigo]) VALUES (2, N'ventas', N'VENTAS')
GO
SET IDENTITY_INSERT [dbo].[TiposMovimientos] OFF
GO
ALTER TABLE [dbo].[MovEstados] ADD  CONSTRAINT [DF_MovEstados_fecha]  DEFAULT (getdate()) FOR [fecha]
GO
ALTER TABLE [dbo].[Productos] ADD  CONSTRAINT [DF_Productos_costo]  DEFAULT ((0)) FOR [costo]
GO
ALTER TABLE [dbo].[Productos] ADD  CONSTRAINT [DF_Productos_precio]  DEFAULT ((0)) FOR [precio]
GO
ALTER TABLE [dbo].[AppControls]  WITH CHECK ADD  CONSTRAINT [FK_AppControls_AppForms] FOREIGN KEY([appFormId])
REFERENCES [dbo].[AppForms] ([appFormId])
GO
ALTER TABLE [dbo].[AppControls] CHECK CONSTRAINT [FK_AppControls_AppForms]
GO
ALTER TABLE [dbo].[AppRolesControls]  WITH CHECK ADD  CONSTRAINT [FK_AppRolesControls_AppControls] FOREIGN KEY([appControlId])
REFERENCES [dbo].[AppControls] ([appControlId])
GO
ALTER TABLE [dbo].[AppRolesControls] CHECK CONSTRAINT [FK_AppRolesControls_AppControls]
GO
ALTER TABLE [dbo].[AppRolesControls]  WITH CHECK ADD  CONSTRAINT [FK_AppRolesControls_AppRoles] FOREIGN KEY([appRolId])
REFERENCES [dbo].[AppRoles] ([appRolId])
GO
ALTER TABLE [dbo].[AppRolesControls] CHECK CONSTRAINT [FK_AppRolesControls_AppRoles]
GO
ALTER TABLE [dbo].[AppRolesUser]  WITH CHECK ADD  CONSTRAINT [FK_AppRolesUser_AppRoles] FOREIGN KEY([appRolId])
REFERENCES [dbo].[AppRoles] ([appRolId])
GO
ALTER TABLE [dbo].[AppRolesUser] CHECK CONSTRAINT [FK_AppRolesUser_AppRoles]
GO
ALTER TABLE [dbo].[AppRolesUser]  WITH CHECK ADD  CONSTRAINT [FK_AppRolesUser_AppUsers] FOREIGN KEY([appUserId])
REFERENCES [dbo].[AppUsers] ([appUserId])
GO
ALTER TABLE [dbo].[AppRolesUser] CHECK CONSTRAINT [FK_AppRolesUser_AppUsers]
GO
ALTER TABLE [dbo].[ClasifPersonas]  WITH CHECK ADD  CONSTRAINT [FK_ClasifPersonas_Clasficacion] FOREIGN KEY([clasificacionId])
REFERENCES [dbo].[Clasificaciones] ([clasificacionId])
GO
ALTER TABLE [dbo].[ClasifPersonas] CHECK CONSTRAINT [FK_ClasifPersonas_Clasficacion]
GO
ALTER TABLE [dbo].[ClasifPersonas]  WITH CHECK ADD  CONSTRAINT [FK_ClasifPersonas_Personas1] FOREIGN KEY([personaId])
REFERENCES [dbo].[Personas] ([personaId])
GO
ALTER TABLE [dbo].[ClasifPersonas] CHECK CONSTRAINT [FK_ClasifPersonas_Personas1]
GO
ALTER TABLE [dbo].[MovEstados]  WITH CHECK ADD  CONSTRAINT [FK_MovEstados_Estados] FOREIGN KEY([estadoId])
REFERENCES [dbo].[Estados] ([estadoId])
GO
ALTER TABLE [dbo].[MovEstados] CHECK CONSTRAINT [FK_MovEstados_Estados]
GO
ALTER TABLE [dbo].[MovEstados]  WITH CHECK ADD  CONSTRAINT [FK_MovEstados_Movimientos] FOREIGN KEY([movimientoId])
REFERENCES [dbo].[Movimientos] ([movimientoId])
GO
ALTER TABLE [dbo].[MovEstados] CHECK CONSTRAINT [FK_MovEstados_Movimientos]
GO
ALTER TABLE [dbo].[MovimientoDet]  WITH CHECK ADD  CONSTRAINT [FK_MovimientoDet_Movimientos] FOREIGN KEY([movimientoId])
REFERENCES [dbo].[Movimientos] ([movimientoId])
GO
ALTER TABLE [dbo].[MovimientoDet] CHECK CONSTRAINT [FK_MovimientoDet_Movimientos]
GO
ALTER TABLE [dbo].[MovimientoDet]  WITH CHECK ADD  CONSTRAINT [FK_MovimientoDet_Productos] FOREIGN KEY([productoId])
REFERENCES [dbo].[Productos] ([productoId])
GO
ALTER TABLE [dbo].[MovimientoDet] CHECK CONSTRAINT [FK_MovimientoDet_Productos]
GO
ALTER TABLE [dbo].[Movimientos]  WITH CHECK ADD  CONSTRAINT [FK_Movimientos_Personas] FOREIGN KEY([personaId])
REFERENCES [dbo].[Personas] ([personaId])
GO
ALTER TABLE [dbo].[Movimientos] CHECK CONSTRAINT [FK_Movimientos_Personas]
GO
ALTER TABLE [dbo].[Movimientos]  WITH CHECK ADD  CONSTRAINT [FK_Movimientos_TiposMovimientos] FOREIGN KEY([tipoId])
REFERENCES [dbo].[TiposMovimientos] ([tipoId])
GO
ALTER TABLE [dbo].[Movimientos] CHECK CONSTRAINT [FK_Movimientos_TiposMovimientos]
GO
ALTER TABLE [dbo].[Productos]  WITH CHECK ADD  CONSTRAINT [FK_Productos_Marcas] FOREIGN KEY([marcaId])
REFERENCES [dbo].[Marcas] ([marcaId])
GO
ALTER TABLE [dbo].[Productos] CHECK CONSTRAINT [FK_Productos_Marcas]
GO
/****** Object:  StoredProcedure [dbo].[GetEstadoMov]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[GetEstadoMov]
	@movId int 
as


select e.*
FROM     dbo.Movimientos AS m INNER JOIN
                  dbo.MovEstados AS me ON m.movimientoId = me.movimientoId INNER JOIN
                  dbo.Estados AS e ON e.estadoId = me.estadoId INNER JOIN
                      (SELECT MAX(movEstadoId) AS movEstadoId, movimientoId
                       FROM      dbo.MovEstados AS me
                       GROUP BY movimientoId) AS mme ON mme.movEstadoId = me.movEstadoId
					   where m.movimientoId = @movId
GO
/****** Object:  StoredProcedure [dbo].[ListControlsUser]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[ListControlsUser]
	@rolId int
as

select f.appFormId, f.formName, f.formFatherId, f.visible as fVisible,
	c.appControlId, c.controlName, c.visible as cVisible
from AppRolesUser ru
inner join AppRolesControls rc on ru.appRolId = rc.appRolId
inner join	AppControls c on rc.appControlId = c.appControlId
inner join AppForms f on  c.appFormId = f.appFormId	
where ru.appRolId = @rolId
GO
/****** Object:  StoredProcedure [dbo].[ListProductosSelect]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ListProductosSelect]
	@tipoMov int 
AS

IF  @tipoMov = 1  /*Compras*/
BEGIN 

	SELECT p.productoId, p.producto, p.marca, p.modelo, p.costo, p.precio, 0 AS existencia, 0 AS cantidad
		FROM   vwProductos p
	order by marca,modelo
END
ELSE IF  @tipoMov = 2  /*Ventas */
BEGIN 

	SELECT p.productoId, p.producto, p.marca, p.modelo, p.costo, p.precio, md.existencia, 0 AS cantidad
	FROM   vwProductos p
	inner join vwExistenciasProducto md on p.productoId = md.productoId 
	where md.existencia > 0

	order by marca,modelo
END




GO
/****** Object:  StoredProcedure [dbo].[ListProductosSelectMovDet]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---
---Listar los movimientos detalle seleccionados al momento de guardar el movimiento
---
CREATE procedure [dbo].[ListProductosSelectMovDet]
	@idMov int 
AS


SELECT md.movimientoId, md.movDetId, md.productoId, p.producto, m.marca, p.modelo, md.valor AS costo, md.valor AS precio, 0 AS existencia, ABS(md.cantidad) AS cantidad
FROM     dbo.MovimientoDet AS md INNER JOIN
                  dbo.Productos AS p ON md.productoId = p.productoId left JOIN
                  dbo.Marcas AS m ON p.marcaId = m.marcaId 
where md.movimientoId  = @idMov
    




GO
/****** Object:  StoredProcedure [dbo].[ReseteoTables]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[ReseteoTables]
as


delete from MovimientoDet
delete from MovEstados
delete from Movimientos
delete from ClasifPersonas
delete from Personas
delete from Productos

declare @userId table ( id int ) 
insert into @userId (id) 
select appUserId from AppUsers where appUserName in ('sadministrador','admin')

delete AppRolesUser where appUserId NOT IN (select id from  @userId)
delete AppUsers where appUserId NOT IN (select id from  @userId)

DBCC CHECKIDENT ('dbo.MovimientoDet', RESEED,0);  
DBCC CHECKIDENT ('dbo.MovEstados', RESEED,0);  
DBCC CHECKIDENT ('dbo.Movimientos', RESEED,0);  
DBCC CHECKIDENT ('dbo.ClasifPersonas', RESEED,0);  
DBCC CHECKIDENT ('dbo.Personas', RESEED,0);  
DBCC CHECKIDENT ('dbo.Productos', RESEED,0);  
GO
/****** Object:  StoredProcedure [dbo].[rptExistencias]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[rptExistencias]
	@nombre nvarchar(max) 
as


set @nombre =  ISNULL(@nombre , '')
select p.* , e.existencia
from Productos p
inner join vwExistenciasProducto e on p.productoId   = e.productoId
where (@nombre = '' and p.producto = p.producto) or (@nombre <> '' and p.producto like '%'+ @nombre +'%') 
GO
/****** Object:  StoredProcedure [dbo].[rptMovimientos]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[rptMovimientos]
	@tipoMov nvarchar(50),
	@fecha1 nvarchar(50),
	@fecha2  nvarchar(50)
as

	SELECT m.movimientoId, tm.codigo as tipo, m.numero referencia, m.fecha, per.tipoPersona, per.persona,
	m.subtotal, m.descuento, m.impuesto, m.total  
	FROM     Movimientos as m 
	inner join TiposMovimientos as tm on m.tipoId = tm.tipoId
	inner join dbo.vwPersonas as per on per.personaId = m.personaId
	where (m.fecha between  CONVERT(datetime, @fecha1, 103) and  CONVERT(datetime, @fecha2, 103))
		 and (tm.codigo = case when @tipoMov =  '' then tm.codigo else @tipoMov end)
	order by m.fecha, m.movimientoId



GO
/****** Object:  StoredProcedure [dbo].[rptMovimientosDetalle]    Script Date: 27/3/2019 11:26:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[rptMovimientosDetalle]
	@tipoMov nvarchar(50),
	@fecha1 nvarchar(50),
	@fecha2  nvarchar(50)
as

	SELECT m.movimientoId, tm.codigo as tipo, m.numero referencia, m.fecha, per.tipoPersona, per.persona,
			p.productoId, p.producto, p.marca, p.modelo, 
	       CASE WHEN tm.codigo = 'COMPRAS' THEN p.costo ELSE 0 END as costo, 
	       CASE WHEN tm.codigo = 'VENTAS' THEN p.precio ELSE 0 END as precio, 
		   --pe.existencia, 
		   ABS(md.cantidad) as cantidad
	FROM   vwProductos as p
	--inner join vwExistenciasProducto as pe on p.productoId = pe.productoId 
	inner join  dbo.MovimientoDet AS md  on md.productoId = p.productoId
	inner join	 Movimientos as m on md.movimientoId = m.movimientoId
	inner join TiposMovimientos as tm on m.tipoId = tm.tipoId
	inner join dbo.vwPersonas as per on per.personaId = m.personaId
	where (m.fecha between  CONVERT(datetime, @fecha1, 103) and  CONVERT(datetime, @fecha2, 103))
		 and (tm.codigo = case when @tipoMov =  '' then tm.codigo else @tipoMov end)
	order by m.fecha, m.movimientoId



GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "me"
            Begin Extent = 
               Top = 7
               Left = 48
               Bottom = 170
               Right = 242
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "e"
            Begin Extent = 
               Top = 175
               Left = 48
               Bottom = 316
               Right = 242
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "mme"
            Begin Extent = 
               Top = 7
               Left = 290
               Bottom = 126
               Right = 484
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1200
         Width = 1968
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1176
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1356
         SortOrder = 1416
         GroupBy = 1350
         Filter = 1356
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwMovimientosActivos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwMovimientosActivos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[32] 4[5] 2[38] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = -1832
      End
      Begin Tables = 
         Begin Table = "m"
            Begin Extent = 
               Top = 7
               Left = 48
               Bottom = 170
               Right = 242
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tm"
            Begin Extent = 
               Top = 175
               Left = 48
               Bottom = 316
               Right = 242
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "p"
            Begin Extent = 
               Top = 322
               Left = 48
               Bottom = 485
               Right = 242
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "me"
            Begin Extent = 
               Top = 7
               Left = 290
               Bottom = 170
               Right = 484
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "e"
            Begin Extent = 
               Top = 7
               Left = 532
               Bottom = 148
               Right = 726
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "mme"
            Begin Extent = 
               Top = 7
               Left = 774
               Bottom = 126
               Right = 968
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 18
         Width = 284
         Width = 1200
         Width = 1200
         Width = 1200
         Width ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwMovimientosAll'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'= 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1176
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1356
         SortOrder = 1416
         GroupBy = 1350
         Filter = 1356
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwMovimientosAll'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwMovimientosAll'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "p"
            Begin Extent = 
               Top = 7
               Left = 48
               Bottom = 170
               Right = 242
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "m"
            Begin Extent = 
               Top = 7
               Left = 290
               Bottom = 126
               Right = 484
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1176
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1356
         SortOrder = 1416
         GroupBy = 1350
         Filter = 1356
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwProductos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwProductos'
GO
