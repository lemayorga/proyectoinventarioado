﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Security
{
    public class AppRolesControls
    {
        public int appRolesControlId { get; set; }
        public int appRolId { get; set; }
        public int appControlId { get; set; }
    }
}
