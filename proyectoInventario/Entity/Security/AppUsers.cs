﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Security
{
    public class AppUsers
    {
        public int appUserId { get; set; }
        public string appUserName { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public bool bloqueado { get; set; }
        public string password { get; set; }
    }
}
