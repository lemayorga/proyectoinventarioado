﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Security
{
    public class AppRoles
    {
        public int appRolId { get; set; }
        public string appRolName { get; set; }
        public string descripcion { get; set; }
        public string masterInic { get; set; }
    }
}
