﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Security
{
    public class AppForms
    {
        public int appFormId { get; set; }
        public string formName { get; set; }
        public int? formFatherId { get; set; }
        public string alias { get; set; }
        public bool visible { get; set; }
    }
}
