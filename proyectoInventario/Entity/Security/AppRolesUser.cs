﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Security
{
   public  class AppRolesUser
    {
        public int appRolUserId { get; set; }
        public int appRolId { get; set; }
        public int appUserId { get; set; }
    }
}
