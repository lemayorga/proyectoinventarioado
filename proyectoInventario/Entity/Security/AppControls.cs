﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Security
{
    public class AppControls
    {
        public int appControlId { get; set; }
        public string controlName { get; set; }
        public int appFormId { get; set; }
        public string alias { get; set; }
        public bool visible { get; set; }
    }
}
