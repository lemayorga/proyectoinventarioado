﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class ClasifPersonas
    {
        public int clasifPersId { get; set; }
        public int personaId { get; set; }
        public int clasificacionId { get; set; }
    }
}
