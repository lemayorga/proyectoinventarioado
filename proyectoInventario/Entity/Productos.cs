﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Productos
    {
        public int productoId { get; set; }
        public string producto { get; set; }
        public double costo { get; set; }
        public double precio { get; set; }
        public int? marcaId { get; set; }
        public string modelo { get; set; }
    }
}
