﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Personas
    {
        public int personaId { get; set; }
        public string nombres { get; set; }
        public string apellidos { get; set; }
        public string ndi { get; set; }
        public string domicilio { get; set; }
        public string telefonos { get; set; }
        public string correos { get; set; }
    }
}
