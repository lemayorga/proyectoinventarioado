﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Movimientos
    {
        public int movimientoId { get; set; }
        public string numero { get; set; }
        public DateTime fecha { get; set; }
        public int personaId { get; set; }
        public int tipoId { get; set; }
        public double subtotal { get; set; }
        public double descuento { get; set; }
        public double impuesto { get; set; }
        public double total { get; set; }
        public string observaciones { get; set; }
    }
}
