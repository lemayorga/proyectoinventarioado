﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class MovimientoDet
    {
        public int movDetId { get; set; }
        public int productoId { get; set; }
        public int movimientoId { get; set; }
        public double valor { get; set; }
        public int cantidad { get; set; }
    }
}
