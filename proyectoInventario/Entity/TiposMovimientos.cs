﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class TiposMovimientos
    {
        public int tipoId { get; set; }
        public string tipo { get; set; }
        public string codigo { get; set; }
    }
}
