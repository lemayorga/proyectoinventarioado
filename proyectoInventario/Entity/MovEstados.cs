﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class MovEstados
    {
        public int movEstadoId { get; set; }
        public int estadoId { get; set; }
        public int movimientoId { get; set; }
        public string usuario { get; set; }
        public DateTime fecha { get; set; }
    }
}
