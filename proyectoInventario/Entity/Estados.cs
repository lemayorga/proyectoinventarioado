﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Estados
    {
        public int estadoId { get; set; }
        public string estado { get; set; }
        public string codigo { get; set; }
    }
}
